var data = [
 {
   "STT": 1,
   "Name": "Bệnh viện đa khoa tỉnh Khánh Hòa",
   "address": "Khu B, Quang Trung, Lộc Thọ, Thành phố Nha Trang, Khánh Hòa 650000, Việt Nam",
   "Longtitude": 12.248668,
   "Latitude": 109.191923,
   "Rating": 3.98,
   "Number_of_beds": 600,
   "area": "Tỉnh"
 },
 {
   "STT": 2,
   "Name": "Bệnh viện Lao và Bệnh phổi Khánh Hòa",
   "address": "Vĩnh Hải, Thành phố Nha Trang, Khánh Hòa, Việt Nam",
   "Longtitude": 12.277695,
   "Latitude": 109.187753,
   "Rating": 3.98,
   "Number_of_beds": 600,
   "area": "Tỉnh"
 },
 {
   "STT": 3,
   "Name": "Bệnh viện da liễu Khánh Hòa",
   "address": "229 Đường Nguyễn Khuyến Vĩnh Hải, Thành phố Nha Trang",
   "Longtitude": 12.278416,
   "Latitude": 109.183956,
   "Rating": 3.98,
   "Number_of_beds": 600,
   "area": "Tỉnh"
 },
 {
   "STT": 4,
   "Name": "Bệnh Viện Y Học Cổ Truyền và Phục Hồi Chức Năng Khánh Hòa",
   "address": "Đường Phạm Văn Đồng, Vĩnh Phước, Thành phố Nha Trang, Khánh Hòa, Việt Nam",
   "Longtitude": 12.272481,
   "Latitude": 109.202804,
   "Rating": 3.98,
   "Number_of_beds": 600,
   "area": "Tỉnh"
 },
 {
   "STT": 5,
   "Name": "Bệnh Viện Tâm Thần Khánh Hòa",
   "address": "Diên Phước, Diên Khánh, Khánh Hòa, Việt Nam",
   "Longtitude": 12.259240,
   "Latitude": 109.042137,
   "Rating": 3.98,
   "Number_of_beds": 600,
   "area": "Tỉnh"
 },
 {
   "STT": 6,
   "Name": "Bệnh viện Đa khoa huyện Cam Lâm",
   "address": "Cam Đức, Cam Lâm, Khánh Hòa, Việt Nam",
   "Longtitude": 12.061722,
   "Latitude": 109.163706,
   "Rating": 3.98,
   "Number_of_beds": 600,
   "area": "Tỉnh"
 },
 {
   "STT": 7,
   "Name": "Bệnh viện Đa khoa Thị xã Ninh Hòa",
   "address": "Mười Sáu Tháng Bảy, TT. Ninh Hòa, Ninh Hòa, Khánh Hòa, Việt Nam",
   "Longtitude": 12.510608,
   "Latitude": 109.137046,
   "Rating": 3.98,
   "Number_of_beds": 600,
   "area": "Tỉnh"
 },
 // {
 //   "STT": 8,
 //   "Name": "Trung tâm Y tế Long Điền",
 //   "address": "Xã Tam Phước, Huyện Long Điền, Tỉnh Khánh Hoà",
 //   "Longtitude": 10.464606,
 //   "Latitude": 107.215427,
 //   "Rating": 3.98,
 //   "Number_of_beds": 600,
 //   "area": "Tỉnh"
 // },
 // {
 //   "STT": 9,
 //   "Name": "Trung tâm Y tế Xuyên Mộc",
 //   "address": "Thị Trấn Phước Bửu, Huyện Xuyên Mộc, Tỉnh Khánh Hoà",
 //   "Longtitude": 10.5390052,
 //   "Latitude": 107.4108108,
 //   "Rating": 3.98,
 //   "Number_of_beds": 600,
 //   "area": "Tỉnh"
 // },
 // {
 //   "STT": 10,
 //   "Name": "Trung tâm Y tế Tân Thành",
 //   "address": "Thị Trấn Mỹ Xuân, Huyện Tân Thành, Tỉnh Khánh Hoà",
 //   "Longtitude": 10.596877,
 //   "Latitude": 107.058933,
 //   "Rating": 3.98,
 //   "Number_of_beds": 600,
 //   "area": "Tỉnh"
 // },
 // {
 //   "STT": 11,
 //   "Name": "Trung tâm Y tế huyện Châu Đức",
 //   "address": "Thị Trấn Ngãi Giao, Huyện Châu Đức, Tỉnh Khánh Hoà",
 //   "Longtitude": 10.647814,
 //   "Latitude": 107.240635,
 //   "Rating": 3.98,
 //   "Number_of_beds": 600,
 //   "area": "Tỉnh"
 // },
 // {
 //   "STT": 12,
 //   "Name": "Trung tâm Y tế Quân dân y Côn Đảo",
 //   "address": "Lê Hồng Phong, Huyện Côn Đảo, Tỉnh Khánh Hoà",
 //   "Longtitude": 8.7120617,
 //   "Latitude": 106.6011811,
 //   "Rating": 3.98,
 //   "Number_of_beds": 600,
 //   "area": "Tỉnh"
 // },
 // {
 //   "STT": 13,
 //   "Name": "Trung tâm Y tế thành phố Bà Rịa",
 //   "address": "Phường Phước Hiệp,Thành phố Bà Rịa, Tỉnh Khánh Hoà",
 //   "Longtitude": 10.498684,
 //   "Latitude": 107.172207,
 //   "Rating": 3.98,
 //   "Number_of_beds": 600,
 //   "area": "Tỉnh"
 // },
 // {
 //   "STT": 14,
 //   "Name": "Trung tâm Y tế huyện Đất Đỏ",
 //   "address": "Tỉnh Lộ 44, Phước Hội, Đất Đỏ, KHÁNH HOÀ, Việt Nam",
 //   "Longtitude": 10.4648746,
 //   "Latitude": 107.2729932,
 //   "Rating": 3.98,
 //   "Number_of_beds": 600,
 //   "area": "Tỉnh"
 // }
];