var datanhathuoc = [
 {
   "STT": 1,
   "ten_co_so": "Quầy thuốc Số 253",
   "address": "Khu phố Thị Vải, phường Mỹ Xuân, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5945606,
   "Latitude": 107.0307371
 },
 {
   "STT": 2,
   "ten_co_so": "Quầy thuốc Bình An",
   "address": "Tổ 18, đường HL8, xã Long Phước, Bà Rịa, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5242681,
   "Latitude": 107.2009949
 },
 {
   "STT": 3,
   "ten_co_so": "Quầy thuốc Hoài An",
   "address": "Tổ 4, thôn Láng Cát, xã Tân Hải, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.504039,
   "Latitude": 107.0923331
 },
 {
   "STT": 4,
   "ten_co_so": "Quầy thuốc Ngọc Lan",
   "address": "Tổ 24, thôn Tân Phú, xã Bàu Chinh, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6760869,
   "Latitude": 107.2481312
 },
 {
   "STT": 5,
   "ten_co_so": "Nhà thuốc Ngọc Linh",
   "address": "217 Nguyễn Hữu Cảnh, phường Thắng Nhất, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.385807,
   "Latitude": 107.1084605
 },
 {
   "STT": 7,
   "ten_co_so": "Quầy thuốc Quốc Bảo HD",
   "address": "Tổ 13, ấp 2, xã Hắc Dịch, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6466114,
   "Latitude": 107.113007
 },
 {
   "STT": 8,
   "ten_co_so": "Nhà thuốc Thiên Đăng",
   "address": "220 Cách Mạng Tháng 8, phường Phước Piệp, Bà Rịa, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4875674,
   "Latitude": 107.1917859
 },
 {
   "STT": 9,
   "ten_co_so": "Quầy thuốc Trí An",
   "address": "Tổ 6, ấp Phước Hưng, xã Mỹ Xuân, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6166667,
   "Latitude": 107.0666667
 },
 {
   "STT": 10,
   "ten_co_so": "Quầy thuốc Triều An",
   "address": "Thôn 1, xã Long Sơn, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4488577,
   "Latitude": 107.1216613
 },
 {
   "STT": 11,
   "ten_co_so": "Nhà thuốc Hoàng Hiệp",
   "address": "29A Phước Thắng, Phường 12, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4135461,
   "Latitude": 107.1612798
 },
 {
   "STT": 12,
   "ten_co_so": "Quầy thuốc Anh Khang",
   "address": "Số 4, Mạc Thanh Đạm , thị trấn Long Điền, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4852984,
   "Latitude": 107.2118914
 },
 {
   "STT": 13,
   "ten_co_so": "Nhà thuốc Bảo Châu",
   "address": "231A, Nguyễn Hữu Cảnh, P. Thắng Nhất, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3860279,
   "Latitude": 107.1086975
 },
 {
   "STT": 15,
   "ten_co_so": "Nhà thuốc Hoàng Anh",
   "address": "79 quốc lộ 51, Kim Hải, phường Kim Dinh, Bà Rịa, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5008657,
   "Latitude": 107.1298981
 },
 {
   "STT": 16,
   "ten_co_so": "Quầy thuốc Kim Anh",
   "address": "Tổ 5, ấp Hội Mỹ, xã Phước Hội, Đất Đỏ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4575,
   "Latitude": 107.2961111
 },
 {
   "STT": 17,
   "ten_co_so": "Nhà thuốc Kim Oanh",
   "address": "780/11 đường Bình Giã, phường 11, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3939221,
   "Latitude": 107.1196845
 },
 {
   "STT": 18,
   "ten_co_so": "Nhà thuốc Mai Anh",
   "address": "1492/7, đường 30/4, phường 12, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4100558,
   "Latitude": 107.1276599
 },
 {
   "STT": 19,
   "ten_co_so": "Nhà thuốc Nguyễn Lê",
   "address": "216 Nguyễn Thanh Đằng, phường Phước Hiệp, Bà Rịa, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4996791,
   "Latitude": 107.1731245
 },
 {
   "STT": 20,
   "ten_co_so": "Quầy thuốc Nhật Anh",
   "address": "Tổ 24/8 Ô 3, khu phố Hải Điền, thị trấn Long Hải, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3976551,
   "Latitude": 107.2374214
 },
 {
   "STT": 21,
   "ten_co_so": "Quầy thuốc Tân Anh",
   "address": "Ấp 3, xã Sông Xoài,, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.666111,
   "Latitude": 107.15
 },
 {
   "STT": 22,
   "ten_co_so": "Quầy thuốc Tiến Doan",
   "address": "Chợ Láng Lớn, xã Láng Lớn, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6627894,
   "Latitude": 107.1696035
 },
 {
   "STT": 24,
   "ten_co_so": "Nhà thuốc Vũng Tàu",
   "address": "408C Lê Hồng Phong, phường Thắng Tam, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3513386,
   "Latitude": 107.0888618
 },
 {
   "STT": 26,
   "ten_co_so": "Quầy thuốc Bình An",
   "address": "Số 52/2, đường số 2, ấp Tân An, xã Phước Tân, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5669449,
   "Latitude": 107.3730563
 },
 {
   "STT": 27,
   "ten_co_so": "Quầy thuốc Bình An",
   "address": "Tổ 3, thôn Tân Ninh, xã Châu Pha, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5737817,
   "Latitude": 107.1543601
 },
 {
   "STT": 28,
   "ten_co_so": "Quầy thuốc Quốc Bảo",
   "address": "104 Lê Hồng Phong, thị trấn Ngãi Giao, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6461055,
   "Latitude": 107.2486531
 },
 {
   "STT": 29,
   "ten_co_so": "Quầy thuốc Số 153",
   "address": "Thôn Sơn Thuận, xã  Xuân Sơn, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.638508,
   "Latitude": 107.321815
 },
 {
   "STT": 30,
   "ten_co_so": "Quầy thuốc Tường Vy",
   "address": "Kios 1, số 5, tổ 13, ấp Phước Lộc, xã Phước Hưng, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4077885,
   "Latitude": 107.2207964
 },
 {
   "STT": 31,
   "ten_co_so": "Quầy thuốc Thuý Hằng",
   "address": "Số 284, tổ 11, ấp Phú Sơn, xã Hoà Hiệp, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6787414,
   "Latitude": 107.5031811
 },
 {
   "STT": 32,
   "ten_co_so": "Quầy thuốc Thanh Hảo",
   "address": "Số 5, lô F, khu phố Phước Thới, thị trấn Đất Đỏ, Đất Đỏ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4900131,
   "Latitude": 107.2712946
 },
 {
   "STT": 33,
   "ten_co_so": "Nhà thuốc Hồng Phúc",
   "address": "189 Nam Kỳ Khởi Nghĩa, phường 3, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.350753,
   "Latitude": 107.0863527
 },
 {
   "STT": 34,
   "ten_co_so": "Quầy thuốc Thiên Phúc",
   "address": "Tổ 10, ấp Phước Lộc, xã Tân Phước, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5639613,
   "Latitude": 107.0744052
 },
 {
   "STT": 35,
   "ten_co_so": "Quầy thuốc Nhật Tân",
   "address": "Kios B3 chợ Hải Sơn, Khu phố Hải Sơn, thị trấn Long Hải, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3918408,
   "Latitude": 107.2325626
 },
 {
   "STT": 36,
   "ten_co_so": "Quầy thuốc Số 136",
   "address": "E2-1/9 Trung tâm đô thị Chí Linh, phường 10, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3787996,
   "Latitude": 107.1118176
 },
 {
   "STT": 37,
   "ten_co_so": "TTTYT Xã Bình Giã",
   "address": "Ấp Gia Hòa Yên, xã Bình Giã, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6437421,
   "Latitude": 107.2607289
 },
 {
   "STT": 38,
   "ten_co_so": "Nhà thuốc Đào Văn Bê",
   "address": "115 Hoàng Văn Thụ, phường 7, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3642068,
   "Latitude": 107.0871323
 },
 {
   "STT": 39,
   "ten_co_so": "Quầy thuốc Ngọc Bích",
   "address": "Ấp 2, xã Hắc Dịch, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6466114,
   "Latitude": 107.113007
 },
 {
   "STT": 40,
   "ten_co_so": "Quầy thuốc Ngọc Bích",
   "address": "Ấp Liên Sơn, xã Xà Bang, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.7258533,
   "Latitude": 107.2408177
 },
 {
   "STT": 41,
   "ten_co_so": "Quầy thuốc Thuận Hòa",
   "address": "Ấp Tân Long, xã Châu Pha, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5749611,
   "Latitude": 107.15554
 },
 {
   "STT": 42,
   "ten_co_so": "Quầy thuốc Hoàng Lan",
   "address": "Tổ 1-ấp Mỹ Thạnh, thị trấn Mỹ Xuân, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6327258,
   "Latitude": 107.0548264
 },
 {
   "STT": 43,
   "ten_co_so": "Nhà thuốc Đức Minh",
   "address": "432 Trần Phú, phường 5, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3830261,
   "Latitude": 107.0653156
 },
 {
   "STT": 44,
   "ten_co_so": "Nhà thuốc Bích Hằng",
   "address": "159H Đô Lương, phường 12, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4036369,
   "Latitude": 107.144006
 },
 {
   "STT": 45,
   "ten_co_so": "Quầy thuốc Lê Cúc",
   "address": "Số 120, ấp An Trung, xã An Nhứt, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4901197,
   "Latitude": 107.2459527
 },
 {
   "STT": 46,
   "ten_co_so": "Quầy thuốc Phòng khám đa khoa Vạn Thành Sài Gòn",
   "address": "304 Độc Lập, xã Quảng Phú, thị trấn Phú Mỹ, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5905556,
   "Latitude": 107.0480556
 },
 {
   "STT": 47,
   "ten_co_so": "Quầy thuốc Số 220",
   "address": "Số 30 Huỳnh Minh Thạnh, thị trấn Phước Bửu, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5341012,
   "Latitude": 107.3995711
 },
 {
   "STT": 48,
   "ten_co_so": "Quầy thuốc Thanh Bình",
   "address": "Tồ 2 ấp Phú Lâm, xã Hòa Hiệp, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6787414,
   "Latitude": 107.5031811
 },
 {
   "STT": 49,
   "ten_co_so": "Nhà thuốc Thành Châu",
   "address": "473 Trương Công Định, phường 7, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3587014,
   "Latitude": 107.0866923
 },
 {
   "STT": 50,
   "ten_co_so": "Quầy thuốc TTYT Long Điền",
   "address": "Trung tâm Y tế huyện Long Điền, ấp An Thạnh, xã An Ngãi, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4646105,
   "Latitude": 107.2154253
 },
 {
   "STT": 51,
   "ten_co_so": "Quầy thuốc TTYT. Long Điền",
   "address": "Trung tâm Y tế huyện Long Điền, ấp An Thạnh, xã An Ngãi, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4646105,
   "Latitude": 107.2154253
 },
 {
   "STT": 52,
   "ten_co_so": "TTTYT Xã Suối Rao",
   "address": "Thôn 4, xã Suối Rao, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.585865,
   "Latitude": 107.322845
 },
 {
   "STT": 53,
   "ten_co_so": "TTTYT Xã Tam Phước",
   "address": "Trạm Y tế xã Tam Phước, ấp Phước Hưng, xã Tam Phước, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4739818,
   "Latitude": 107.236441
 },
 {
   "STT": 54,
   "ten_co_so": "Quầy thuốc Út Cải",
   "address": "Tổ 28/01, khu phố Hải Vân, thị trấn Long Hải, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.395902,
   "Latitude": 107.2370874
 },
 {
   "STT": 56,
   "ten_co_so": "Quầy thuốc Gia Phát",
   "address": "T44, tổ 04, ấp Tân Phước, xã Phước Tỉnh, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.402897,
   "Latitude": 107.186093
 },
 {
   "STT": 57,
   "ten_co_so": "Quầy thuốc Quốc Trọng",
   "address": " Ô6, ấp Bắc, xã Hòa Long, Bà Rịa, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5466302,
   "Latitude": 107.2056869
 },
 {
   "STT": 58,
   "ten_co_so": "Nhà thuốc Việt Hoa",
   "address": "53 Hoàng Hoa Thám, phường Thắng Tam, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3401583,
   "Latitude": 107.0817636
 },
 {
   "STT": 59,
   "ten_co_so": "Quầy thuốc Thúy Ngọc",
   "address": "Ấp Hồ Tràm, xã Phước Thuận, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4932137,
   "Latitude": 107.3982236
 },
 {
   "STT": 60,
   "ten_co_so": "Quầy thuốc Hưng Cảnh",
   "address": "Số 1, tổ 1, ấp Hải Lâm, xã Phước Hưng, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4124996,
   "Latitude": 107.2058125
 },
 {
   "STT": 61,
   "ten_co_so": "Nhà thuốc Mai Anh",
   "address": "515 Nguyễn An Ninh, phường Nguyễn An Ninh, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3653461,
   "Latitude": 107.0904055
 },
 {
   "STT": 62,
   "ten_co_so": "Quầy thuốc Kim Oanh",
   "address": "Tổ 09, khu phố Hải Bình, thị trấn Long Hải, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3842286,
   "Latitude": 107.2366329
 },
 {
   "STT": 63,
   "ten_co_so": "Quầy thuốc Bảo Châu",
   "address": "Số 153 Võ Thị Sáu, khu phố Long Nguyên, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4838087,
   "Latitude": 107.2049765
 },
 {
   "STT": 64,
   "ten_co_so": "Quầy thuốc Bảo Châu",
   "address": "Tổ 13, ấp Tân Phú, thị trấn Phú Mỹ, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.620929,
   "Latitude": 107.055887
 },
 {
   "STT": 65,
   "ten_co_so": "Quầy thuốc Châu Thành",
   "address": "Ấp Phước Thọ, xã Phước Hưng , Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4124996,
   "Latitude": 107.2058125
 },
 {
   "STT": 66,
   "ten_co_so": "Quầy thuốc Minh Đức",
   "address": "Số 3/8 Bùi Công Minh, xã An Ngãi, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4649237,
   "Latitude": 107.2107374
 },
 {
   "STT": 67,
   "ten_co_so": "Quầy thuốc Mỹ Châu",
   "address": "Tổ 39, thôn Đồng Tiến, xã Cù Bị, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.7294922,
   "Latitude": 107.1839024
 },
 {
   "STT": 68,
   "ten_co_so": "Quầy thuốc Ngọc Châu",
   "address": "Tổ 3, KP. Phước Trung, TT. Phước Hải, Đất Đỏ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4857647,
   "Latitude": 107.1779936
 },
 {
   "STT": 69,
   "ten_co_so": "Quầy thuốc Phương Nhi",
   "address": "Tổ 7/27, KP. Hải An, TT. Long Hải, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.385901,
   "Latitude": 107.239737
 },
 {
   "STT": 70,
   "ten_co_so": "Nhà thuốc Quỳnh Như",
   "address": "45 Bình Giã, phường 8, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3535228,
   "Latitude": 107.0892461
 },
 {
   "STT": 71,
   "ten_co_so": "Nhà thuốc Trang Đức",
   "address": "414 đường Nguyễn Hữu Cảnh, phường 10, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3721838,
   "Latitude": 107.1205512
 },
 {
   "STT": 72,
   "ten_co_so": "Quầy thuốc Anh Tú",
   "address": "9/12 ấp Thạnh Sơn 2B, xã Phước Tân, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5669449,
   "Latitude": 107.3730563
 },
 {
   "STT": 73,
   "ten_co_so": "Nhà thuốc Vạn Tâm",
   "address": "Quốc lộ 51, khu phố Hải Sơn, phường Phước Hòa, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5235078,
   "Latitude": 107.0821861
 },
 {
   "STT": 74,
   "ten_co_so": "TTTYT Xã Bình Ba",
   "address": "Ấp Bình Đức, xã Bình Ba, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6183333,
   "Latitude": 107.2348877
 },
 {
   "STT": 75,
   "ten_co_so": "Quầy thuốc Số 208",
   "address": "Tổ 10/2 Ô2, khu phố Hải Hà, thị trấn Long Hải, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3851624,
   "Latitude": 107.2401989
 },
 {
   "STT": 76,
   "ten_co_so": "Quầy thuốc Thu Hiền",
   "address": "Tổ 30, ấp Xà Bang 1, xã Xà Bang, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.726022,
   "Latitude": 107.2370411
 },
 {
   "STT": 77,
   "ten_co_so": "Nhà thuốc Mai Hân",
   "address": "258 Nguyễn An Ninh, phường 7, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3684515,
   "Latitude": 107.0834354
 },
 {
   "STT": 78,
   "ten_co_so": "Nhà thuốc Hải Vân",
   "address": "953A Bình Giã, phường 10, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3938535,
   "Latitude": 107.1178579
 },
 {
   "STT": 79,
   "ten_co_so": "Nhà thuốc Thúy Ngọc",
   "address": "1216/31 đường 30/4, phường 12, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4100558,
   "Latitude": 107.1276599
 },
 {
   "STT": 80,
   "ten_co_so": "Nhà thuốc Thọ",
   "address": "LK 4 - 03 Võ Văn Kiệt, phường Long Tâm, Bà Rịa, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5026231,
   "Latitude": 107.1930086
 },
 {
   "STT": 81,
   "ten_co_so": "Quầy thuốc Thái Tú",
   "address": "QL 52, ấp Bắc 2, xã Hòa Long, Bà Rịa, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5466302,
   "Latitude": 107.2056869
 },
 {
   "STT": 82,
   "ten_co_so": "Nhà thuốc Tuyết Thiên",
   "address": "90 Huỳnh Tịnh Của, phường Phước Trung, Bà Rịa, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4917058,
   "Latitude": 107.172678
 },
 {
   "STT": 83,
   "ten_co_so": "Quầy thuốc Bích Cúc",
   "address": "Số 04, tổ 28, ấp Phước Lâm, xã Phước Hưng, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4155606,
   "Latitude": 107.2102525
 },
 {
   "STT": 84,
   "ten_co_so": "Nhà thuốc Kim Cúc",
   "address": "418 Trương Công Định, P. 8, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3602518,
   "Latitude": 107.0875836
 },
 {
   "STT": 85,
   "ten_co_so": "Nhà thuốc Kim Hà",
   "address": "15 Lê Lợi, phường 1, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3522616,
   "Latitude": 107.0736911
 },
 {
   "STT": 86,
   "ten_co_so": "Quầy thuốc Lê Huyền",
   "address": "62/3 Tổ 3, ấp Bến Lội, xã Bình châu, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.57921,
   "Latitude": 107.5386809
 },
 {
   "STT": 87,
   "ten_co_so": "Quầy thuốc Số 207",
   "address": "Số 269/4  khu phố Thạnh Sơn, thị trấn Phước Bửu, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5353285,
   "Latitude": 107.4055814
 },
 {
   "STT": 88,
   "ten_co_so": "Quầy thuốc Tâm Phúc ",
   "address": "Ấp 5, xã Hắc Dịch, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.644244,
   "Latitude": 107.111359
 },
 {
   "STT": 89,
   "ten_co_so": "Nhà thuốc Tú Quyên",
   "address": "1001/13 Bình Giã, phường Rạch Dừa, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3975786,
   "Latitude": 107.1167046
 },
 {
   "STT": 90,
   "ten_co_so": "Nhà thuốc Hoàng Sơn",
   "address": "Tổ 4, khu phố Nam Dinh, phường Kim Dinh, Bà Rịa, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5056072,
   "Latitude": 107.1337162
 },
 {
   "STT": 91,
   "ten_co_so": "Quầy thuốc Số 08",
   "address": "62 Lê Quý Đôn, phường Phước Trung, Bà Rịa, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4922934,
   "Latitude": 107.1714687
 },
 {
   "STT": 92,
   "ten_co_so": "Nhà thuốc Thanh Trúc",
   "address": "724 đường 30/4, phường 11, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4100558,
   "Latitude": 107.1276599
 },
 {
   "STT": 93,
   "ten_co_so": "Quầy thuốc Thanh Dần",
   "address": "Tổ 14, khu phố Phước Sơn, phường Phước Hòa, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5139583,
   "Latitude": 107.0598491
 },
 {
   "STT": 94,
   "ten_co_so": "Quầy thuốc An Bình",
   "address": "Tổ 6, ấp Hội Mỹ, xã Phước Hội, Đất Đỏ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4575552,
   "Latitude": 107.2961953
 },
 {
   "STT": 95,
   "ten_co_so": "Quầy thuốc Anh Đào",
   "address": "Tổ 4 ấp Nhân Tiến, xã Xuyên Mộc, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5657342,
   "Latitude": 107.4231952
 },
 {
   "STT": 96,
   "ten_co_so": "Quầy thuốc Kim Thư",
   "address": "Tổ 14, ấp Láng Cát, xã Tân Hải, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.504039,
   "Latitude": 107.0923331
 },
 {
   "STT": 97,
   "ten_co_so": "Quầy thuốc Long Bình",
   "address": "Số 122, Bùi Công Minh, khu phố Long Bình, thị trấn Long Điền, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4786597,
   "Latitude": 107.2117455
 },
 {
   "STT": 98,
   "ten_co_so": "TTTYT Phòng khám khu vực Hòa Bình",
   "address": "Ấp 3, xã Hòa Bình, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6130313,
   "Latitude": 107.3975372
 },
 {
   "STT": 99,
   "ten_co_so": "Quầy thuốc Trinh Pharmacy",
   "address": "Số 146A Huỳnh Minh Thạnh, khu phố Phước An, thị trấn Phước Bửu, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5262371,
   "Latitude": 107.4020627
 },
 {
   "STT": 100,
   "ten_co_so": "Quầy thuốc Số 04",
   "address": "111 Nguyễn Thanh Đằng, phường Phước Hiệp, Bà Rịa, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5000609,
   "Latitude": 107.1731233
 },
 {
   "STT": 101,
   "ten_co_so": "Quầy thuốc Anh Thư",
   "address": "Tổ 3/16, Ô 1, khu phố Hải Lộc, thị trấn Long Hải, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.395902,
   "Latitude": 107.2370874
 },
 {
   "STT": 102,
   "ten_co_so": "Quầy thuốc Diễm Quỳnh",
   "address": "Tổ 5, thôn Xuân Tân, xã Xuân Sơn, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6404024,
   "Latitude": 107.3198424
 },
 {
   "STT": 103,
   "ten_co_so": "Quầy thuốc Hồng Diễm",
   "address": "D44/1, ấp Phước Thái, xã Phước Tỉnh, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4141066,
   "Latitude": 107.2013665
 },
 {
   "STT": 104,
   "ten_co_so": "Quầy thuốc Ngọc Diễm",
   "address": "Tổ 2, khu phố Lộc An, thị trấn Phước Hải, Đất Đỏ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4054168,
   "Latitude": 107.2607289
 },
 {
   "STT": 105,
   "ten_co_so": "Nhà thuốc Phương Yến",
   "address": "1124 đường 30/4, phường11, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4100558,
   "Latitude": 107.1276599
 },
 {
   "STT": 106,
   "ten_co_so": "Quầy thuốc Thúy Diễm",
   "address": "Số 91, đường số 6, thôn Gia An, xã Suối Nghệ, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5935525,
   "Latitude": 107.180232
 },
 {
   "STT": 107,
   "ten_co_so": "Quầy thuốc Số 02",
   "address": "Số 45 Lý Thường Kiệt, phường Phước Trung , Bà Rịa, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5086736,
   "Latitude": 107.195195
 },
 {
   "STT": 108,
   "ten_co_so": "Nhà thuốc Thúy Điệp",
   "address": "Kiosque 41F, Ba Cu, phường 4, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.349249,
   "Latitude": 107.075824
 },
 {
   "STT": 109,
   "ten_co_so": "Nhà thuốc Bảo Khang",
   "address": "Số 304 đường Phan Văn Trị, tổ 7, khu phố Kim Sơn, phường Kim Dinh, Bà Rịa, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.510181,
   "Latitude": 107.1921393
 },
 {
   "STT": 110,
   "ten_co_so": "Nhà thuốc Quỳnh Anh",
   "address": "Số 118 Nguyễn Văn Hưởng, phường Long Tâm, Bà Rịa, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5083743,
   "Latitude": 107.1884733
 },
 {
   "STT": 111,
   "ten_co_so": "Quầy thuốc Thái An",
   "address": "576 tổ 1, khu phố Tường Thành, thị trấn Đất Đỏ, Đất Đỏ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4913475,
   "Latitude": 107.2725505
 },
 {
   "STT": 112,
   "ten_co_so": "Nhà thuốc Thủy Tùng",
   "address": "26 Hoàng Hoa Thám, phường 2, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3405759,
   "Latitude": 107.0793507
 },
 {
   "STT": 113,
   "ten_co_so": "Nhà thuốc Minh Đạt",
   "address": "Số 10 Phạm Ngọc Thạch, phường Phước Hưng, Bà Rịa, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5085218,
   "Latitude": 107.1757235
 },
 {
   "STT": 114,
   "ten_co_so": "Nhà thuốc Thu Nga",
   "address": "134 Nam Kỳ Khởi Nghĩa, phường Thắng Tam, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3473343,
   "Latitude": 107.0848165
 },
 {
   "STT": 115,
   "ten_co_so": "Quầy thuốc Phương Đông",
   "address": "Ấp 4, xã Hắc Dịch, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6469372,
   "Latitude": 107.1125203
 },
 {
   "STT": 116,
   "ten_co_so": "Nhà thuốc Minh An",
   "address": "129 Hoàng Hoa Thám, phường Thắng Tam, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3380178,
   "Latitude": 107.0854415
 },
 {
   "STT": 117,
   "ten_co_so": "Quầy thuốc Hoàng Long",
   "address": "Số 92, khu phố Long Tân, thị trấn Long Điền, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4844827,
   "Latitude": 107.214944
 },
 {
   "STT": 118,
   "ten_co_so": "Nhà thuốc Pasteur Vũng Tàu",
   "address": "03 Nguyễn Thái Học, phường 7, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3617253,
   "Latitude": 107.0807192
 },
 {
   "STT": 119,
   "ten_co_so": "Quầy thuốc số 230",
   "address": "Chợ xã Xuyên Mộc, xã Xuyên Mộc, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5637807,
   "Latitude": 107.423045
 },
 {
   "STT": 120,
   "ten_co_so": "Quầy thuốc Thanh Cúc",
   "address": "1 Ô 2/9, khu phố Phước An, thị trấn Phước Hải, Đất Đỏ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4902864,
   "Latitude": 107.2711131
 },
 {
   "STT": 121,
   "ten_co_so": "Nhà thuốc Đình Khoa",
   "address": "62 Nguyễn Tri Phương, phường 7, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3617823,
   "Latitude": 107.0860835
 },
 {
   "STT": 122,
   "ten_co_so": "Quầy thuốc Hoàng Dung",
   "address": "98 Huỳnh Minh Thạnh, thị trấn Phước Bửu, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5303014,
   "Latitude": 107.4006037
 },
 {
   "STT": 123,
   "ten_co_so": "Nhà thuốc Kim Châu",
   "address": "Số 167, đường 27/4, phường Phước Hưng, Bà Rịa, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5153943,
   "Latitude": 107.180948
 },
 {
   "STT": 124,
   "ten_co_so": "Quầy thuốc Kim Dung",
   "address": "Tổ 2, ấp Phú Lâm, xã Hòa Hiệp, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6787414,
   "Latitude": 107.5031811
 },
 {
   "STT": 125,
   "ten_co_so": "Quầy thuốc Kim Dung",
   "address": "Tổ 6, ấp Quảng Phú, thị trấn Phú Mỹ, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5905345,
   "Latitude": 107.0480378
 },
 {
   "STT": 126,
   "ten_co_so": "Nhà thuốc Mai Dung",
   "address": "04 Nam Kỳ Khởi Nghĩa, phường Thắng Tam, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3437633,
   "Latitude": 107.0828549
 },
 {
   "STT": 127,
   "ten_co_so": "Nhà thuốc Minh Huy",
   "address": "146, đường Hoàng Hoa Thám, phường 2, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3345655,
   "Latitude": 107.0885631
 },
 {
   "STT": 128,
   "ten_co_so": "Quầy thuốc Mỹ Dung",
   "address": "Ấp 2 Đông, xã Bầu Lâm, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6970348,
   "Latitude": 107.3730563
 },
 {
   "STT": 129,
   "ten_co_so": "Quầy thuốc Mỹ Dung",
   "address": "Số 308, QL55, khu phố Thạnh Sơn, thị trấn Phước Bửu, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5386462,
   "Latitude": 107.4109322
 },
 {
   "STT": 130,
   "ten_co_so": "Quầy thuốc Mỹ Dung",
   "address": "Tổ 2, ấp Song Vĩnh, xã Tân Phước, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.565034,
   "Latitude": 107.05809
 },
 {
   "STT": 131,
   "ten_co_so": "Quầy thuốc Phú Mỹ",
   "address": "tổ 1, KP. Quãng Phú, TT. Phú Mỹ, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5900675,
   "Latitude": 107.0485002
 },
 {
   "STT": 132,
   "ten_co_so": "Quầy thuốc Phương Dung",
   "address": "Thôn Tân Hạnh, thị trấn Phú Mỹ, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6013546,
   "Latitude": 107.0490856
 },
 {
   "STT": 133,
   "ten_co_so": "Quầy thuốc số 159",
   "address": "15 Bùi Công Minh, KP. Long Tân, TT. Long Điền, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.484225,
   "Latitude": 107.2118801
 },
 {
   "STT": 134,
   "ten_co_so": "Quầy thuốc  Số 177",
   "address": "Ấp Tân Hòa, xã Long Tân, Đất Đỏ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5505926,
   "Latitude": 107.2784615
 },
 {
   "STT": 135,
   "ten_co_so": "Quầy thuốc số 224",
   "address": "Khu phố Phước Lộc, thị trấn Phước Bửu, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5354501,
   "Latitude": 107.403416
 },
 {
   "STT": 136,
   "ten_co_so": "Nhà thuốc Thanh Đức Phát 1",
   "address": "28 Đội Cấn, phường 8, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3627406,
   "Latitude": 107.0922486
 },
 {
   "STT": 137,
   "ten_co_so": "Quầy thuốc Thùy Dung II",
   "address": "Ấp Phước Tấn, xã Tân Hòa, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5193791,
   "Latitude": 107.0947444
 },
 {
   "STT": 138,
   "ten_co_so": "Quầy thuốc Trọng Nghĩa",
   "address": "Tổ 16, ấp Chu Hải, xã Tân Hải, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5030618,
   "Latitude": 107.1100534
 },
 {
   "STT": 139,
   "ten_co_so": "Quầy thuốc Việt Mỹ",
   "address": "ấp Mỹ Tân, xã Mỹ Xuân, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6327258,
   "Latitude": 107.0548264
 },
 {
   "STT": 140,
   "ten_co_so": "TTTYT Xã Đá Bạc",
   "address": "Thôn Phú Sơn, xã Đá Bạc, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.594167,
   "Latitude": 107.278333
 },
 {
   "STT": 141,
   "ten_co_so": "Nhà thuốc An Thảo",
   "address": "1782, đường 30/4, Phường 12, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4100558,
   "Latitude": 107.1276599
 },
 {
   "STT": 142,
   "ten_co_so": "Quầy thuốc Hoàng Thuỷ",
   "address": "Tổ 16, ấp Phước Hưng, xã Mỹ Xuân, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.620929,
   "Latitude": 107.055887
 },
 {
   "STT": 143,
   "ten_co_so": "Quầy thuốc Trúc Nhân",
   "address": "Tổ 1, ấp Hải Sơn, xã Phước Hưng, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4283333,
   "Latitude": 107.2369444
 },
 {
   "STT": 144,
   "ten_co_so": "Quầy thuốc Ngọc Thạch",
   "address": "Khu phố Quảng Phú, thị trấn Phú Mỹ, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5872811,
   "Latitude": 107.0622145
 },
 {
   "STT": 145,
   "ten_co_so": "Nhà thuốc Phương Linh",
   "address": "Số 105, Trần Hưng Đạo, phường Phước Nguyên, Bà Rịa, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.500874,
   "Latitude": 107.1806403
 },
 {
   "STT": 146,
   "ten_co_so": "Quầy thuốc Thanh Thảo",
   "address": "Đường 37, tổ 28, thôn Quảng Thành 1, xã Nghĩa Thành, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5718057,
   "Latitude": 107.1898113
 },
 {
   "STT": 147,
   "ten_co_so": "Nhà thuốc Duy Đức",
   "address": "1578A đường 30/4, phường 12, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4100558,
   "Latitude": 107.1276599
 },
 {
   "STT": 148,
   "ten_co_so": "Quầy thuốc Phước Sơn",
   "address": "Số 19, tổ 1, ấp Hải Sơn, xã Phước Hưng, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4163017,
   "Latitude": 107.2104935
 },
 {
   "STT": 149,
   "ten_co_so": "Quầy thuốc Khang Phúc",
   "address": "Số 10, tổ 13, ấp Phước Thọ, xã Phước Hưng, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4163582,
   "Latitude": 107.2156779
 },
 {
   "STT": 150,
   "ten_co_so": "Quầy thuốc Lộc An",
   "address": "Tổ 9, thôn Tân Phước, xã Tân Hòa, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5573341,
   "Latitude": 107.0598491
 },
 {
   "STT": 151,
   "ten_co_so": "Quầy thuốc Lộc Duyên",
   "address": "Tổ 12, ấp Hải Sơn, xã Phước Hòa, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5367551,
   "Latitude": 107.0765948
 },
 {
   "STT": 152,
   "ten_co_so": "Quầy thuốc Lộc Ngân II",
   "address": "44/9 ấp Tân Hòa, xã Long Tân, Đất Đỏ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5505926,
   "Latitude": 107.2784615
 },
 {
   "STT": 153,
   "ten_co_so": "Quầy thuốc Phúc An",
   "address": "E2, ấp Phước thiện, xã Phước Tỉnh, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.412436,
   "Latitude": 107.200427
 },
 {
   "STT": 154,
   "ten_co_so": "Quầy thuốc Số 18",
   "address": "115 Trần Hưng Đạo, phường Phước Nguyên, Bà Rịa, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5010751,
   "Latitude": 107.1815705
 },
 {
   "STT": 155,
   "ten_co_so": "Quầy thuốc Tâm Đức",
   "address": "36 Lê Lợi, thị trấn Ngãi Giao, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.649983,
   "Latitude": 107.240484
 },
 {
   "STT": 156,
   "ten_co_so": "Quầy thuốc Duyên Linh",
   "address": "3/11 ấp Phước Lâm, xã Phước Hưng, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4146365,
   "Latitude": 107.2046113
 },
 {
   "STT": 157,
   "ten_co_so": "Quầy thuốc Đăng Khoa",
   "address": "Tổ 5, ấp Phước Hưng, xã Mỹ Xuân, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6544024,
   "Latitude": 107.0395051
 },
 {
   "STT": 158,
   "ten_co_so": "Nhà thuốc Hồng Châu",
   "address": "175 đường Nam Kỳ Khởi Nghĩa, phường 3, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3504947,
   "Latitude": 107.0861596
 },
 {
   "STT": 159,
   "ten_co_so": "Nhà thuốc Hương Tùng",
   "address": "160 Trương Công Định, phường 3, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3510309,
   "Latitude": 107.0829873
 },
 {
   "STT": 160,
   "ten_co_so": "Quầy thuốc Số 251",
   "address": "Ô 1, khu phố Hải Bình, thị trấn Long Hải, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4049205,
   "Latitude": 107.2222109
 },
 {
   "STT": 161,
   "ten_co_so": "Quầy thuốc Thành Giang",
   "address": "Số 6, Phan Bội Châu, thôn Ngọc Hà, thị trấn Phú Mỹ, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6052902,
   "Latitude": 107.0513143
 },
 {
   "STT": 162,
   "ten_co_so": "Quầy thuốc Trường Giang",
   "address": "Ấp Sông Cầu, xã Nghĩa Thành, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5673977,
   "Latitude": 107.2113457
 },
 {
   "STT": 163,
   "ten_co_so": "Quầy thuốc Long Điền",
   "address": "65J, khu phố Long An, thị trấn Long Điền, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4933173,
   "Latitude": 107.2168114
 },
 {
   "STT": 164,
   "ten_co_so": "Quầy thuốc Hà Trâm",
   "address": "Tổ 8, ấp Nam, xã Hòa Long, Bà Rịa, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5219235,
   "Latitude": 107.2078572
 },
 {
   "STT": 165,
   "ten_co_so": "Nhà thuốc Huy Hà",
   "address": "171 Nguyễn An Ninh, phường Thắng Nhì, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3699281,
   "Latitude": 107.080818
 },
 {
   "STT": 166,
   "ten_co_so": "Quầy thuốc Khánh Hà",
   "address": "Tổ 6, thôn Tân Lễ A, xã Châu Pha, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5633892,
   "Latitude": 107.1706079
 },
 {
   "STT": 167,
   "ten_co_so": "Nhà thuốc Ngọc Hà",
   "address": "119 Lê Lai, phường 1, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3498299,
   "Latitude": 107.0781567
 },
 {
   "STT": 168,
   "ten_co_so": "Quầy thuốc Quỳnh Anh 10",
   "address": "Tổ 2 Khu phố Phước Lập - Phường Mỹ Xuân, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6396214,
   "Latitude": 107.0521974
 },
 {
   "STT": 169,
   "ten_co_so": "Quầy thuốc Tâm Hà",
   "address": "Số 6, Lê Hồng Phong, thị trấn Ngãi Giao, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6480291,
   "Latitude": 107.2418905
 },
 {
   "STT": 170,
   "ten_co_so": "Quầy thuốc Tân Thành",
   "address": "Độc Lập, KP. Vạn Hạnh, TT. Phú Mỹ, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5996859,
   "Latitude": 107.0520258
 },
 {
   "STT": 171,
   "ten_co_so": "Nhà thuốc Thảo Hòa",
   "address": "448 Trần Phú, Phường 5, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3824136,
   "Latitude": 107.0657541
 },
 {
   "STT": 172,
   "ten_co_so": "Quầy thuốc Thảo My",
   "address": "89 ấp An Trung, xã An Nhứt, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4901197,
   "Latitude": 107.2459527
 },
 {
   "STT": 173,
   "ten_co_so": "TTTYT Thị trấn Long Hải",
   "address": "Tủ thuốc trạm Y tế  thị trấn Long Hải, Thị trấn Long Hải, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3967066,
   "Latitude": 107.2374559
 },
 {
   "STT": 174,
   "ten_co_so": "Nhà thuốc Trường Giang",
   "address": "57 Tú Xương,  phường 4, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3560327,
   "Latitude": 107.0805551
 },
 {
   "STT": 175,
   "ten_co_so": "Quầy thuốc Việt Tâm ",
   "address": "ấp Phước Thạnh, xã Mỹ Xuân, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.616667,
   "Latitude": 107.066667
 },
 {
   "STT": 176,
   "ten_co_so": "TTTYT Xã Quảng Thành",
   "address": "Ấp Công Thành, xã Qủang Thành, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6974711,
   "Latitude": 107.2773613
 },
 {
   "STT": 177,
   "ten_co_so": "TTTYT Xã Tân Hải",
   "address": "Xã Tân Hải, thôn Tân Hải, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5005153,
   "Latitude": 107.1071
 },
 {
   "STT": 178,
   "ten_co_so": "Nhà thuốc Kim Liên",
   "address": "99 Hàn Thuyên, phường Rạch Dừa, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3929233,
   "Latitude": 107.1134624
 },
 {
   "STT": 179,
   "ten_co_so": "Quầy thuốc Thanh Trúc",
   "address": "Tổ 01, ấp Phước Tân, xã Phước Tỉnh, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.402897,
   "Latitude": 107.186093
 },
 {
   "STT": 180,
   "ten_co_so": "Quầy thuốc Hải Dương",
   "address": "Thôn Phước Hải, xã Tân Hải, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.499121,
   "Latitude": 107.108784
 },
 {
   "STT": 181,
   "ten_co_so": "Nhà thuốc Pharmacity số 169",
   "address": "Số 290/10B Nguyễn Hữu Cảnh, phường Nguyễn An Ninh, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3817604,
   "Latitude": 107.1088215
 },
 {
   "STT": 182,
   "ten_co_so": "Nhà thuốc Phương Vy",
   "address": "Số 2777B, khu phố 3, Phường Phước Nguyên, Bà Rịa, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.501265,
   "Latitude": 107.1793596
 },
 {
   "STT": 183,
   "ten_co_so": "Nhà thuốc Số 50 ",
   "address": "3F8, tầng 3, Lotte Mart, góc đường 3/2, Thi Sách, phường 8, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.35015,
   "Latitude": 107.0939865
 },
 {
   "STT": 184,
   "ten_co_so": "Quầy thuốc Số 6",
   "address": "Tổ 16, ô 2, ấp Bắc 1, xã Hòa Long, Bà Rịa, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5277905,
   "Latitude": 107.2016295
 },
 {
   "STT": 185,
   "ten_co_so": "Quầy thuốc Thiên Phúc",
   "address": "Tổ 2, thôn Quảng Giao, xã Xuân Sơn, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6404024,
   "Latitude": 107.3198424
 },
 {
   "STT": 186,
   "ten_co_so": "Quầy thuốc Minh Nhật",
   "address": "175/7 ấp 3, xã Hòa Hội, , Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6279317,
   "Latitude": 107.4440254
 },
 {
   "STT": 187,
   "ten_co_so": "Quầy thuốc Ngọc Hân",
   "address": "Thôn 2, xã Suối Rao, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5912828,
   "Latitude": 107.3257545
 },
 {
   "STT": 188,
   "ten_co_so": "Quầy thuốc Số 204",
   "address": "Thôn Xuân Tân, xã Xuân Sơn, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6522573,
   "Latitude": 107.3265123
 },
 {
   "STT": 189,
   "ten_co_so": "Quầy thuốc Gia Hân",
   "address": "Ấp 3, xã Hòa Hưng, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6573892,
   "Latitude": 107.4026244
 },
 {
   "STT": 190,
   "ten_co_so": "Quầy thuốc Hải Âu",
   "address": "Số 190, tổ 7, ấp 3, xã Hòa Hội, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6279317,
   "Latitude": 107.4440254
 },
 {
   "STT": 191,
   "ten_co_so": "Nhà thuốc Mai An",
   "address": "13 Nguyễn Văn Trỗi, phường 4, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3548195,
   "Latitude": 107.0781244
 },
 {
   "STT": 192,
   "ten_co_so": "Quầy thuốc Minh Hằng",
   "address": "Tổ 7, ấp Thị Vải, xã Mỹ Xuân, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.620929,
   "Latitude": 107.055887
 },
 {
   "STT": 193,
   "ten_co_so": "Nhà thuốc Nhi Đồng",
   "address": "326 Nguyễn Hữu Cảnh, phường Thắng Nhất, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.381293,
   "Latitude": 107.1101728
 },
 {
   "STT": 194,
   "ten_co_so": "Quầy thuốc Số 245",
   "address": "2189 đường Độc Lập, KP Tân Phú, phường Phú Mỹ, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6067584,
   "Latitude": 107.054414
 },
 {
   "STT": 195,
   "ten_co_so": "Quầy thuốc Tấn Tài",
   "address": "Khu phố Phước Tiến, thị trấn Phước Bữu, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5347843,
   "Latitude": 107.3989927
 },
 {
   "STT": 196,
   "ten_co_so": "Quầy thuốc Thanh Hằng",
   "address": "Tổ 31, thôn 4, xã Bình Trung, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6424077,
   "Latitude": 107.2843726
 },
 {
   "STT": 197,
   "ten_co_so": "Quầy thuốc Thiên Thanh",
   "address": "903 tổ 16, khu phố Tường Thành, thị trấn Đất Đỏ, Đất Đỏ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4913475,
   "Latitude": 107.2725505
 },
 {
   "STT": 198,
   "ten_co_so": "Quầy thuốc Tuệ Tâm",
   "address": "Tổ 3, ấp Ông Trịnh, xã Tân Phước, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5624307,
   "Latitude": 107.0612418
 },
 {
   "STT": 199,
   "ten_co_so": "TTTYT Xã Bình Trung",
   "address": "Thôn 6, xã Bình Trung, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6379169,
   "Latitude": 107.2861962
 },
 {
   "STT": 200,
   "ten_co_so": "TTTYT Xã Tân Lâm",
   "address": "Ấp 4B, xã Tân Lâm, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.7377268,
   "Latitude": 107.4203669
 },
 {
   "STT": 201,
   "ten_co_so": "Quầy thuốc Thu Hành",
   "address": "Ô 2, ấp Bắc I, xã Hòa Long, Bà Rịa, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5466302,
   "Latitude": 107.2056869
 },
 {
   "STT": 202,
   "ten_co_so": "Quầy thuốc Hồng Hạnh",
   "address": "1 Ô 2/21, khu phố Phước Trung, Thị trấn Phước Hải, Đất Đỏ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4902864,
   "Latitude": 107.2711131
 },
 {
   "STT": 203,
   "ten_co_so": "Nhà thuốc Hồng Ngân",
   "address": "901C Bình Giã, Phường 10, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3890376,
   "Latitude": 107.1188219
 },
 {
   "STT": 204,
   "ten_co_so": "Nhà thuốc Mỹ Châu",
   "address": "972 đường 30/4, phường 11, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4100558,
   "Latitude": 107.1276599
 },
 {
   "STT": 205,
   "ten_co_so": "Quầy thuốc Mỹ Hạnh",
   "address": "Tổ 2, ấp Trang Nghiêm, xã Bông Trang, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5355319,
   "Latitude": 107.4499404
 },
 {
   "STT": 206,
   "ten_co_so": "Quầy thuốc Ngọc Quỳnh",
   "address": "Tổ 9, khu phố Hải Hà 2, thị trấn Long Hải, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3936953,
   "Latitude": 107.2390746
 },
 {
   "STT": 207,
   "ten_co_so": "Quầy thuốc Nhân Tâm",
   "address": "38/32 Ô3, KP Hải Hòa, thị trấn Long Hải, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.395902,
   "Latitude": 107.2370874
 },
 {
   "STT": 208,
   "ten_co_so": "Quầy thuốc Nhật Khoa",
   "address": "Tổ 8, Ô 2, ấp Bắc, xã Hòa Long, Bà Rịa, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5277905,
   "Latitude": 107.2016295
 },
 {
   "STT": 210,
   "ten_co_so": "Quầy thuốc Số 10",
   "address": "261 Cách Mạng Tháng Tám, phường Phước Hiệp, Bà Rịa, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4953801,
   "Latitude": 107.1725385
 },
 {
   "STT": 211,
   "ten_co_so": "Quầy thuốc Số 144",
   "address": "298 Nguyễn Văn Cừ, phường Long Toàn, Bà Rịa, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4978949,
   "Latitude": 107.198215
 },
 {
   "STT": 212,
   "ten_co_so": "Quầy thuốc Tâm An",
   "address": "114 Hùng Vương, thị trấn Ngãi Giao, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.643258,
   "Latitude": 107.2449091
 },
 {
   "STT": 213,
   "ten_co_so": "Quầy thuốc Hoàng Lực",
   "address": "Tổ 45, ấp Tân Thành, xã Quảng Thành, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5640726,
   "Latitude": 107.0595233
 },
 {
   "STT": 214,
   "ten_co_so": "Nhà thuốc Ngọc Châu III",
   "address": "2031 Đường Độc Lập, khu phố Tân Ngọc, thị trấn Phú Mỹ, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6038324,
   "Latitude": 107.0547739
 },
 {
   "STT": 216,
   "ten_co_so": "Quầy thuốc Thúy Hoa",
   "address": "Ấp Song Vĩnh, xã Tân Phước, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.571325,
   "Latitude": 107.0554865
 },
 {
   "STT": 217,
   "ten_co_so": "Quầy thuốc Minh Nhật",
   "address": "Tổ 6, ấp Phú Quý, xã Hòa Hiệp, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.533333,
   "Latitude": 108.95
 },
 {
   "STT": 218,
   "ten_co_so": "Quầy thuốc Gia Huy",
   "address": "Tổ 8, ấp 5, xã Hòa Bình, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6155553,
   "Latitude": 107.3772141
 },
 {
   "STT": 219,
   "ten_co_so": "Nhà thuốc Nhân Hậu",
   "address": "1001/20 Bình Giã, phường Rạch Dừa, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3875434,
   "Latitude": 107.1170292
 },
 {
   "STT": 220,
   "ten_co_so": "Quầy thuốc Nhân Tâm",
   "address": "Ấp Phú Hà, xã Mỹ Xuân, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.647062,
   "Latitude": 107.044289
 },
 {
   "STT": 221,
   "ten_co_so": "Quầy thuốc Minh Thu",
   "address": "Tổ 1, ấp Đông, xã Long Phước, Bà Rịa, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5275353,
   "Latitude": 107.2282686
 },
 {
   "STT": 222,
   "ten_co_so": "Nhà thuốc Thiện Tâm",
   "address": "720 Trần Phú, phường Thắng Nhì, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3713103,
   "Latitude": 107.0735048
 },
 {
   "STT": 223,
   "ten_co_so": "Quầy thuốc An Nhứt",
   "address": "74 ấp An Lạc, xã An Nhứt, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4901197,
   "Latitude": 107.2459527
 },
 {
   "STT": 224,
   "ten_co_so": "Quầy thuốc Chính Quy",
   "address": "Tổ 1, ấp 2, xã Tóc Tiên, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6070711,
   "Latitude": 107.1173423
 },
 {
   "STT": 225,
   "ten_co_so": "Nhà thuốc Đức Thiện",
   "address": "Số 57, Võ Văn Kiệt, phường Long Tâm, Bà Rịa, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5035755,
   "Latitude": 107.1931973
 },
 {
   "STT": 226,
   "ten_co_so": "Quầy thuốc Hiền ",
   "address": "4Ô 2/19, khu phố Hải An, thị trấn Phước Hải, Đất Đỏ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4141883,
   "Latitude": 107.2776811
 },
 {
   "STT": 227,
   "ten_co_so": "Nhà thuốc Hồng Yến",
   "address": "77 Lê Lợi, phường 4, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3567724,
   "Latitude": 107.074765
 },
 {
   "STT": 228,
   "ten_co_so": "Nhà thuốc Lê Hoàn",
   "address": "1362 đường 30/4, phường 12, thành 12, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4100558,
   "Latitude": 107.1276599
 },
 {
   "STT": 229,
   "ten_co_so": "Nhà thuốc Nano 3",
   "address": "1002 đường 30/4, phường 11, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4100558,
   "Latitude": 107.1276599
 },
 {
   "STT": 230,
   "ten_co_so": "Quầy thuốc Ngọc Hiền",
   "address": "1Ô5/20, khu phố Hải Lạc, thị trấn Phước Hải, Đất Đỏ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.414526,
   "Latitude": 107.278711
 },
 {
   "STT": 231,
   "ten_co_so": "Nhà thuốc Ngọc Hiền",
   "address": "Số 147 Phan Văn Trị, phường Kim Dinh, Bà Rịa, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5015168,
   "Latitude": 107.1313613
 },
 {
   "STT": 232,
   "ten_co_so": "Quầy thuốc Nhật Minh",
   "address": "Tổ 11, ấp Nhân Nghĩa, xã Xuyên Mộc, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5585237,
   "Latitude": 107.4262813
 },
 {
   "STT": 233,
   "ten_co_so": "Quầy thuốc Số 217",
   "address": "Ấp 4 Hòa Bình, xã Hòa Bình, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6263518,
   "Latitude": 107.3723267
 },
 {
   "STT": 234,
   "ten_co_so": "Quầy thuốc Thiên Nhẫn",
   "address": "4073B, tổ 3, ấp Phước Tân 3, xã Tân Hưng, Bà Rịa, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5308689,
   "Latitude": 107.1706079
 },
 {
   "STT": 235,
   "ten_co_so": "Quầy thuốc Thu Hiền",
   "address": "Ấp Suối Lê, xã Tân Lâm, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.7377268,
   "Latitude": 107.4203669
 },
 {
   "STT": 236,
   "ten_co_so": "Quầy thuốc Thu Hiền",
   "address": "Ấp Trãng Cát, xã Hắc Dịch, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6401209,
   "Latitude": 107.0889812
 },
 {
   "STT": 237,
   "ten_co_so": "Quầy thuốc Thu Hiền",
   "address": "Khu phố 3, thị trấn Ngãi Giao, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6366211,
   "Latitude": 107.2472867
 },
 {
   "STT": 238,
   "ten_co_so": "Nhà thuốc Thu Thảo 1",
   "address": "55 đường Lê Quý Đôn, phường 1, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3502671,
   "Latitude": 107.0749149
 },
 {
   "STT": 239,
   "ten_co_so": "Quầy thuốc Tiến Phong",
   "address": "Số 20, tổ 5, ấp Hải Sơn, xã Phước Hưng, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4163582,
   "Latitude": 107.2156779
 },
 {
   "STT": 240,
   "ten_co_so": "TTTYT Xã An Nhứt",
   "address": "Trạm Y tế xã An Nhứt, ấp An Lạc, xã An Nhứt, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4901197,
   "Latitude": 107.2459527
 },
 {
   "STT": 241,
   "ten_co_so": "Quầy thuốc Hoàng Hiển",
   "address": "Tổ 7, thôn Láng Cát, xã Tân Hải, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.504039,
   "Latitude": 107.0923331
 },
 {
   "STT": 242,
   "ten_co_so": "Nhà thuốc Hà Phương",
   "address": "15 Phạm Hồng Thái, phường 7, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3661274,
   "Latitude": 107.0766362
 },
 {
   "STT": 243,
   "ten_co_so": "Quầy thuốc Huỳnh",
   "address": "Thôn Tân Xuân, xã Bàu Chinh, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.678295,
   "Latitude": 107.2252675
 },
 {
   "STT": 244,
   "ten_co_so": "Quầy thuốc Ngọc Anh",
   "address": "5Ô3, tổ 18, khu phố Hải Điền, thị trấn Long Hải, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3976551,
   "Latitude": 107.2374214
 },
 {
   "STT": 245,
   "ten_co_so": "Nhà thuốc Ngọc Duyên",
   "address": "1504 đường 30/4, phường 12, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4100558,
   "Latitude": 107.1276599
 },
 {
   "STT": 246,
   "ten_co_so": "Quầy thuốc Nhật Tân 2",
   "address": "Số 123 Mạc Thanh Đạm, khu phố Long Tâm, thị trấn Long Điền, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4881495,
   "Latitude": 107.2128403
 },
 {
   "STT": 247,
   "ten_co_so": "Quầy thuốc Hiếu Trung",
   "address": "Tổ 3, ấp Trang Hùng, xã Bông Trang, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5355319,
   "Latitude": 107.4499404
 },
 {
   "STT": 248,
   "ten_co_so": "Quầy thuốc Lộc Ngân",
   "address": "Tổ 5, thôn Phú Sơn, xã Đá Bạc, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5879937,
   "Latitude": 107.2693773
 },
 {
   "STT": 249,
   "ten_co_so": "Quầy thuốc Số 105",
   "address": "Chợ mới Vũng Tàu, phường Thắng Tam, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.34594,
   "Latitude": 107.0842
 },
 {
   "STT": 250,
   "ten_co_so": "Quầy thuốc Số 249",
   "address": "Ấp Phước Long, xã Tân Hòa, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.514722,
   "Latitude": 107.082778
 },
 {
   "STT": 251,
   "ten_co_so": "Quầy thuốc Tâm Đức",
   "address": "khu 5B, ấp Hải Sơn, xã Phước Hòa, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5367551,
   "Latitude": 107.0765948
 },
 {
   "STT": 252,
   "ten_co_so": "Quầy thuốc Tâm Thiện",
   "address": "Ấp 4, tổ 11, xã Hòa Hội, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6279317,
   "Latitude": 107.4440254
 },
 {
   "STT": 253,
   "ten_co_so": "Nhà thuốc A Dũng ACP",
   "address": "Tổ 14, khu phố Hải Dinh, xã Kim Dinh, Bà Rịa, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4992113,
   "Latitude": 107.1307289
 },
 {
   "STT": 254,
   "ten_co_so": "Quầy thuốc Hạnh",
   "address": "số 387, Võ Thị Sáu, khu phố Long Phượng, thị trấn Long Điền, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.484852,
   "Latitude": 107.2159345
 },
 {
   "STT": 255,
   "ten_co_so": "Nhà thuốc Kim Hoa",
   "address": "07 Ba Cu, phường 1, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3484202,
   "Latitude": 107.0744447
 },
 {
   "STT": 256,
   "ten_co_so": "Quầy thuốc Kim Ngân",
   "address": "Tổ 19/4 khu phố Hải Hà 1, thị trấn Long Hải, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3851624,
   "Latitude": 107.2401989
 },
 {
   "STT": 257,
   "ten_co_so": "Nhà thuốc Lakeside",
   "address": "102-A2 Chung cư Lakeside, P. Nguyễn An Ninh, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.377722,
   "Latitude": 107.109842
 },
 {
   "STT": 258,
   "ten_co_so": "Quầy thuốc Mỹ Hoa",
   "address": "Ấp 2, xã Hắc Dịch, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6466114,
   "Latitude": 107.113007
 },
 {
   "STT": 259,
   "ten_co_so": "Quầy thuốc Ngọc Hân",
   "address": "68 ấp khu I, xã Bình Châu, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.57921,
   "Latitude": 107.5386809
 },
 {
   "STT": 260,
   "ten_co_so": "Nhà thuốc Ngọc Lan",
   "address": "54 đường Trương Công Định, phường 3, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3471196,
   "Latitude": 107.0803325
 },
 {
   "STT": 261,
   "ten_co_so": "Quầy thuốc Nguyễn Cường",
   "address": "Kios B26 chợ Long Điền, thị trấn Long Điền, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4933173,
   "Latitude": 107.2168114
 },
 {
   "STT": 262,
   "ten_co_so": "Nhà thuốc Nhân Đức",
   "address": "219 Cách Mạng Tháng Tám, phường Phước Hiệp, Bà Rịa, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.497491,
   "Latitude": 107.1676612
 },
 {
   "STT": 263,
   "ten_co_so": "Quầy thuốc Quỳnh Anh",
   "address": "Ấp Mỹ Thạnh, phường Mỹ Xuân , Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6231602,
   "Latitude": 107.0545273
 },
 {
   "STT": 264,
   "ten_co_so": "Quầy thuốc Số 210",
   "address": "Tổ 19, khu phố Tân Hạnh, phường Phú Mỹ, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5905345,
   "Latitude": 107.0480378
 },
 {
   "STT": 265,
   "ten_co_so": "Quầy thuốc Số 228",
   "address": "ấp 2, xã Bàu Lâm, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6970348,
   "Latitude": 107.3730563
 },
 {
   "STT": 266,
   "ten_co_so": "Quầy thuốc Thanh Hoa",
   "address": "F01, tổ 1, ấp Phước Lợi, xã Phước Tỉnh, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4118663,
   "Latitude": 107.1927658
 },
 {
   "STT": 267,
   "ten_co_so": "Quầy thuốc Tuyết Trinh",
   "address": "Số 20/10, ấp Phước Lâm, xã Phước Hưng, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4163582,
   "Latitude": 107.2156779
 },
 {
   "STT": 268,
   "ten_co_so": "Quầy thuốc Thiên Phúc",
   "address": "771 khu phố Phước Sơn, thị trấn Đất Đỏ, Đất Đỏ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4900131,
   "Latitude": 107.2712946
 },
 {
   "STT": 269,
   "ten_co_so": "Quầy thuốc Phước Thịnh",
   "address": "QL 51, ấp Hải Sơn, xã phước Hòa, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5367551,
   "Latitude": 107.0765948
 },
 {
   "STT": 270,
   "ten_co_so": "Nhà thuốc An Phước",
   "address": "51 Đồ Chiểu, phường 1, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3495956,
   "Latitude": 107.0781005
 },
 {
   "STT": 271,
   "ten_co_so": "Quầy thuốc Bảo Hòa",
   "address": "Ấp Phước Tấn, xã Tân Hòa, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5193791,
   "Latitude": 107.0947444
 },
 {
   "STT": 272,
   "ten_co_so": "Nhà thuốc Duy Anh",
   "address": "58 Nơ Trang Long, phường Rạch Dừa, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.395294,
   "Latitude": 107.1120322
 },
 {
   "STT": 273,
   "ten_co_so": "Quầy thuốc Số 05",
   "address": "477 khu phố Núi Dinh, phường Kim Dinh, Bà Rịa, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5005298,
   "Latitude": 107.1487273
 },
 {
   "STT": 274,
   "ten_co_so": "Quầy thuốc Số 237",
   "address": "21/1 Ô 3, khu phố Hải Điền, thị trấn Long Hải, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3819595,
   "Latitude": 107.2497115
 },
 {
   "STT": 275,
   "ten_co_so": "Quầy thuốc Tâm Khang",
   "address": "Tổ 1, ấp Phước Lập, xã Mỹ Xuân, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6456132,
   "Latitude": 107.0495635
 },
 {
   "STT": 276,
   "ten_co_so": "Quầy thuốc Thanh Trang",
   "address": "5 Ô 3/6, khu phố Hải Trung, thị trấn Phước Hải, Đất Đỏ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4902864,
   "Latitude": 107.2711131
 },
 {
   "STT": 277,
   "ten_co_so": "Nhà thuốc Thiện Nhân",
   "address": "245 Lê Quang Định, phường Thắng Nhất, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3770003,
   "Latitude": 107.1007122
 },
 {
   "STT": 278,
   "ten_co_so": "Quầy thuốc Xuân Giang",
   "address": "Tổ 2, ấp Bình Đức, xã Bình Ba, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6213323,
   "Latitude": 107.2384028
 },
 {
   "STT": 279,
   "ten_co_so": "Quầy thuốc Dược Nguyễn",
   "address": "Số 19/1, ấp Phú Bình, xã Hòa Hiệp, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6787414,
   "Latitude": 107.5031811
 },
 {
   "STT": 280,
   "ten_co_so": "Nhà thuốc Bình Giã",
   "address": "260 Bình Giã, phường Nguyễn An Ninh, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3594302,
   "Latitude": 107.0936362
 },
 {
   "STT": 281,
   "ten_co_so": "Quầy thuốc Hoàng Khang",
   "address": "162 tổ 5, thôn Đức Mỹ, xã Suối Nghệ, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5895466,
   "Latitude": 107.2292749
 },
 {
   "STT": 282,
   "ten_co_so": "Nhà thuốc Hồng Nhị",
   "address": "139 Nguyễn An Ninh, phường 6, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3701575,
   "Latitude": 107.0799635
 },
 {
   "STT": 283,
   "ten_co_so": "Quầy thuốc Nhi Đồng Thành Phố",
   "address": "QL 51, Ấp Phước Thạnh, xã Mỹ Xuân, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6496937,
   "Latitude": 107.0419008
 },
 {
   "STT": 284,
   "ten_co_so": "Nhà thuốc Khánh Tâm",
   "address": "558 đường 30/4, phường Rạch Dừa, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4100558,
   "Latitude": 107.1276599
 },
 {
   "STT": 285,
   "ten_co_so": "Nhà thuốc 126",
   "address": "978 đường 30/4, Phường 11, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4100558,
   "Latitude": 107.1276599
 },
 {
   "STT": 286,
   "ten_co_so": "Quầy thuốc Ánh Hồng",
   "address": "Tổ 3, ấp Thanh Bình 3, xã Bình Châu, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.57921,
   "Latitude": 107.5386809
 },
 {
   "STT": 287,
   "ten_co_so": "Quầy thuốc Bảo Thanh",
   "address": "Tổ 6, ấp Hải Sơn, xã Phước Hưng, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3918408,
   "Latitude": 107.2325626
 },
 {
   "STT": 288,
   "ten_co_so": "Quầy thuốc Hồng Phong",
   "address": "Ấp Suối Nhum, xã Hắc Dịch, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6439282,
   "Latitude": 107.10401
 },
 {
   "STT": 289,
   "ten_co_so": "Quầy thuốc Lâm Hồng",
   "address": "Tổ 27, ấp Trung Thành, xã Quảng Thành, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6974711,
   "Latitude": 107.2773613
 },
 {
   "STT": 290,
   "ten_co_so": "Nhà thuốc Lê Minh Phương",
   "address": "42 Đô Lương, phường 11, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4087429,
   "Latitude": 107.1368203
 },
 {
   "STT": 291,
   "ten_co_so": "Quầy thuốc Ngọc Hân",
   "address": "Tổ 3, nông trường Hắc Dịch, phường Hắc Dịch, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6386078,
   "Latitude": 107.1303869
 },
 {
   "STT": 292,
   "ten_co_so": "Nhà thuốc Nguyên Đức",
   "address": "212/1 Nguyễn Hữu Cảnh, phường Thắng Nhất, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.38739,
   "Latitude": 107.1061601
 },
 {
   "STT": 293,
   "ten_co_so": "Quầy thuốc Phương Nam",
   "address": "Ấp Suối Nhum, xã Hắc Dịch, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6439282,
   "Latitude": 107.10401
 },
 {
   "STT": 294,
   "ten_co_so": "Nhà thuốc Số 126",
   "address": "978 Đường 30/4, phường 11, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4100558,
   "Latitude": 107.1276599
 },
 {
   "STT": 295,
   "ten_co_so": "Quầy thuốc Số 148",
   "address": "59 Hoàng Hoa Thám, phường Long Tâm, Bà Rịa, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5096791,
   "Latitude": 107.189666
 },
 {
   "STT": 296,
   "ten_co_so": "Nhà thuốc Thanh Hà",
   "address": "174 đường Lưu Chí Hiếu, phường Thắng Nhất, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3884391,
   "Latitude": 107.1098487
 },
 {
   "STT": 297,
   "ten_co_so": "Quầy thuốc Thanh Hồng",
   "address": "Ấp Bắc, xã Long Phước, Bà Rịa, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.515556,
   "Latitude": 107.225
 },
 {
   "STT": 298,
   "ten_co_so": "Quầy thuốc Thiên Phước",
   "address": "Số 87, đường 17, thôn Gio An, xã Suối Nghệ, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.58925,
   "Latitude": 107.1706398
 },
 {
   "STT": 299,
   "ten_co_so": "Quầy thuốc Thiện Tâm",
   "address": "Q23, tổ 10, ấp Phước Hiệp, xã Phước Tỉnh, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4033645,
   "Latitude": 107.1813173
 },
 {
   "STT": 300,
   "ten_co_so": "Quầy thuốc Tuấn Hào",
   "address": "lô 23, Trung tâm thương mại Kim Long, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.7043005,
   "Latitude": 107.2445889
 },
 {
   "STT": 301,
   "ten_co_so": "Quầy thuốc Hoa Hồng",
   "address": "Số 01 trung tâm thương mại Kim Long, xã Kim Long, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.7043005,
   "Latitude": 107.2445889
 },
 {
   "STT": 302,
   "ten_co_so": "Quầy thuốc Đức Mạnh",
   "address": "Tổ 19, ấp Mỹ Xuân, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6171729,
   "Latitude": 107.0663663
 },
 {
   "STT": 303,
   "ten_co_so": "Quầy thuốc Hoàng Quân",
   "address": "Tổ 20, ấp Thị Vải, xã Mỹ Xuân, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.620929,
   "Latitude": 107.055887
 },
 {
   "STT": 304,
   "ten_co_so": "Quầy thuốc Kiều Mai",
   "address": "Ấp Bến Đình, xã Mỹ Xuân, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6170354,
   "Latitude": 107.0706189
 },
 {
   "STT": 305,
   "ten_co_so": "Quầy thuốc Lê Khánh",
   "address": "Số 226, đường Độc Lập, tổ 3, khu phố Quảng Phú, thị trấn Phú Mỹ, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5990164,
   "Latitude": 107.0544572
 },
 {
   "STT": 306,
   "ten_co_so": "Nhà thuốc Như Thủy",
   "address": "862 đường 30/4, Phường 11, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4100558,
   "Latitude": 107.1276599
 },
 {
   "STT": 307,
   "ten_co_so": "TTTYT Xã Nghĩa Thành",
   "address": "Thôn Quảng Thành 1, xã Nghĩa Thành, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5716667,
   "Latitude": 107.1897222
 },
 {
   "STT": 308,
   "ten_co_so": "Quầy thuốc Hoàng Hiếu",
   "address": "Tổ 5, ấp Phước Hưng, xã Mỹ Xuân, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6544024,
   "Latitude": 107.0395051
 },
 {
   "STT": 309,
   "ten_co_so": "Quầy thuốc Huệ Tâm",
   "address": "Số 4071, tổ 3, ấp Phước Tân 3, xã Tân Hưng, Bà Rịa, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5308689,
   "Latitude": 107.1706079
 },
 {
   "STT": 310,
   "ten_co_so": "Nhà thuốc Tâm Sáng",
   "address": "144 Xô Viết Nghệ Tĩnh, phường Thắng Tam, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.348149,
   "Latitude": 107.0867716
 },
 {
   "STT": 311,
   "ten_co_so": "Quầy thuốc Thảo Nguyên",
   "address": "Ô 4, tổ 4, khu phố Hải Tân, thị trấn Long Hải , Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3851624,
   "Latitude": 107.2401989
 },
 {
   "STT": 312,
   "ten_co_so": "Nhà thuốc TTYT Vietsovpetro",
   "address": "02 Pasteur, Phường 7, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3598759,
   "Latitude": 107.0834466
 },
 {
   "STT": 314,
   "ten_co_so": "Quầy thuốc Số 07",
   "address": "66 Nguyễn Thanh Đằng, phường Phước Trung, Bà Rịa, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4951108,
   "Latitude": 107.1706428
 },
 {
   "STT": 315,
   "ten_co_so": "Quầy thuốc Thiện Nhân",
   "address": "tổ 4, ấp Phước Tân, xã Phước Tỉnh, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.404135,
   "Latitude": 107.186664
 },
 {
   "STT": 316,
   "ten_co_so": "Nhà thuốc Thu Huyền",
   "address": "36B Bắc Sơn, phường 11, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4004846,
   "Latitude": 107.1273645
 },
 {
   "STT": 317,
   "ten_co_so": "Quầy thuốc Cẩm Hương",
   "address": "Tổ 10, ấp Phước Hưng, xã Tam Phước, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4283624,
   "Latitude": 107.2370874
 },
 {
   "STT": 318,
   "ten_co_so": "Quầy thuốc Hoàng Nhi",
   "address": "Ấp 3, xã Hòa Hưng, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6573892,
   "Latitude": 107.4026244
 },
 {
   "STT": 319,
   "ten_co_so": "Quầy thuốc Kim Hồng",
   "address": "Thôn 10, xã Long Sơn, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4529906,
   "Latitude": 107.0896654
 },
 {
   "STT": 320,
   "ten_co_so": "Quầy thuốc Kim Hương",
   "address": "03/1, tổ 1, ấp 3, xã Bưng Riềng, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.538601,
   "Latitude": 107.4913489
 },
 {
   "STT": 321,
   "ten_co_so": "Quầy thuốc Mỹ Hương",
   "address": "Tổ 9, thôn Tân Xuân, xã Bàu Chinh, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.678295,
   "Latitude": 107.2252675
 },
 {
   "STT": 322,
   "ten_co_so": "Quầy thuốc Quỳnh Giao",
   "address": "Kios K50, chợ Long Hải, thị trấn Long Hải, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4085143,
   "Latitude": 107.2213235
 },
 {
   "STT": 323,
   "ten_co_so": "Quầy thuốc Quỳnh Hương",
   "address": "tổ 13, thôn Đức Mỹ, xã Suối Nghệ, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5909619,
   "Latitude": 107.2306159
 },
 {
   "STT": 324,
   "ten_co_so": "Quầy thuốc Số 106",
   "address": "88 Bạch Đằng, phường 5, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3791502,
   "Latitude": 107.0681024
 },
 {
   "STT": 325,
   "ten_co_so": "Quầy thuốc Số 155",
   "address": "Tổ 4, ấp Phước Lâm, xã Phương Hưng, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4124996,
   "Latitude": 107.2058125
 },
 {
   "STT": 326,
   "ten_co_so": "Quầy thuốc Thanh Hương",
   "address": "Ấp 6, xã Tóc Tiên, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6030556,
   "Latitude": 107.1127778
 },
 {
   "STT": 327,
   "ten_co_so": "Quầy thuốc Thành Tâm",
   "address": "khu 1, ấp Lam Sơn, xã Phước Hòa, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5298175,
   "Latitude": 107.0802641
 },
 {
   "STT": 328,
   "ten_co_so": "Quầy thuốc Thiên Hương",
   "address": "Chợ Suối Nghệ, xã Suối Nghệ, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5905973,
   "Latitude": 107.209859
 },
 {
   "STT": 329,
   "ten_co_so": "Quầy thuốc Thiên Hương",
   "address": "Tổ 4, ấp Phú Sơn, xã Đá Bạc, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5949771,
   "Latitude": 107.2773735
 },
 {
   "STT": 330,
   "ten_co_so": "Quầy thuốc Thu Hương",
   "address": "Tổ 5, thôn 4, xã Suối Rao, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5912828,
   "Latitude": 107.3257545
 },
 {
   "STT": 331,
   "ten_co_so": "Nhà thuốc Thuận Thành",
   "address": "11 Phan Bội Châu, phường 2, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3405503,
   "Latitude": 107.0759372
 },
 {
   "STT": 332,
   "ten_co_so": "Nhà thuốc Vân Anh",
   "address": "330 Bình Giã, phường Nguyễn An N inh, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3725013,
   "Latitude": 107.09986
 },
 {
   "STT": 333,
   "ten_co_so": "Quầy thuốc Xuân Hương",
   "address": "Tổ 5, ấp Nam, xã Hòa Long, Bà Rịa, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5277905,
   "Latitude": 107.2016295
 },
 {
   "STT": 334,
   "ten_co_so": "TTTYT Hòa Hưng",
   "address": "Ấp 2, xã  Hòa Hưng, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6609454,
   "Latitude": 107.3829862
 },
 {
   "STT": 335,
   "ten_co_so": "Quầy thuốc Hoàng Gia 2",
   "address": "Tổ 1, ấp Phước Lập, xã Mỹ Xuân, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6456132,
   "Latitude": 107.0495635
 },
 {
   "STT": 336,
   "ten_co_so": "Nhà thuốc Mai Thương",
   "address": "335 đường Trần phú, phường 5, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3826106,
   "Latitude": 107.066757
 },
 {
   "STT": 337,
   "ten_co_so": "Nhà thuốc Phúc Khuê",
   "address": "1535 đường 30/4, P.12, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4198228,
   "Latitude": 107.1517611
 },
 {
   "STT": 338,
   "ten_co_so": "Quầy thuốc Tân Phú",
   "address": "Ấp Tân Phú, xã Châu Pha, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5737587,
   "Latitude": 107.153931
 },
 {
   "STT": 339,
   "ten_co_so": "Nhà thuốc Thu Hường",
   "address": "177 Nguyễn Hữu Cảnh, phường Thắng Nhất, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3872814,
   "Latitude": 107.1070314
 },
 {
   "STT": 340,
   "ten_co_so": "Quầy thuốc Thiên Ân",
   "address": "Tổ 14, ấp Phú Vinh, xã Hoà Hiệp, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6787414,
   "Latitude": 107.5031811
 },
 {
   "STT": 341,
   "ten_co_so": "Nhà thuốc Anh Huy",
   "address": "58 Bạch Đằng, phường 5, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3776588,
   "Latitude": 107.0692738
 },
 {
   "STT": 343,
   "ten_co_so": "Quầy thuốc Tam Đức",
   "address": "Tô 18, thôn Sơn Tân, xã Sơn Bình, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6462999,
   "Latitude": 107.3297846
 },
 {
   "STT": 344,
   "ten_co_so": "Quầy thuốc Thanh Huyền",
   "address": "Tổ 3, ấp 1, xã Tóc Tiên, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6070711,
   "Latitude": 107.1173423
 },
 {
   "STT": 345,
   "ten_co_so": "Nhà thuốc Thảo My",
   "address": "763 Trương Công Định, phường 9, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3670412,
   "Latitude": 107.0907165
 },
 {
   "STT": 346,
   "ten_co_so": "Quầy thuốc An Nhiên",
   "address": "Tổ 8, ấp Hoàng Giao,  thị trấn Ngãi Giao, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6284702,
   "Latitude": 107.2526106
 },
 {
   "STT": 347,
   "ten_co_so": "Quầy thuốc Huyền Anh",
   "address": "Tổ 3, thôn 1, xã Bình Trung, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6462751,
   "Latitude": 107.2815315
 },
 {
   "STT": 348,
   "ten_co_so": "Quầy thuốc Huyền Quang",
   "address": "Tổ 12, ấp Tân Hải, thị trấn Long Hải, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.41155,
   "Latitude": 107.1922471
 },
 {
   "STT": 349,
   "ten_co_so": "Nhà thuốc Khánh Huyền",
   "address": "767A, Bình Giã, phường 10, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3838749,
   "Latitude": 107.1153724
 },
 {
   "STT": 350,
   "ten_co_so": "Nhà thuốc Kim Anh",
   "address": "37 Bạch Đằng, phường Phước Nguyên, Bà Rịa, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4937968,
   "Latitude": 107.1703514
 },
 {
   "STT": 351,
   "ten_co_so": "Quầy thuốc Minh Châu",
   "address": "Kios B1, chợ Long Điền, thị trấn Long Điền, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4940076,
   "Latitude": 107.2145613
 },
 {
   "STT": 352,
   "ten_co_so": "Quầy thuốc Quốc Bảo",
   "address": "Khu 2, ấp Lam Sơn, xã Phước Hòa , Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5298175,
   "Latitude": 107.0802641
 },
 {
   "STT": 353,
   "ten_co_so": "Quầy thuốc Số 12",
   "address": "Số 10, đường Cách Mạng Tháng Tám, phường Long Hưng, Bà Rịa, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4921869,
   "Latitude": 107.1833729
 },
 {
   "STT": 354,
   "ten_co_so": "Quầy thuốc Thu Huyền",
   "address": "Tổ 11, khu phố 5, phường Hắc Dịch, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6466114,
   "Latitude": 107.113007
 },
 {
   "STT": 355,
   "ten_co_so": "Nhà thuốc Số 05",
   "address": "511 Trần Phú, phường 6, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3707356,
   "Latitude": 107.0754355
 },
 {
   "STT": 356,
   "ten_co_so": "Nhà thuốc Số 5",
   "address": "511 Trần phú, phường Thắng Nhì, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3707356,
   "Latitude": 107.0754355
 },
 {
   "STT": 357,
   "ten_co_so": "Quầy thuốc Thuận Hiếu",
   "address": "E09, tổ 03, ấp Phước Thiện, xã Phước Tỉnh, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.412436,
   "Latitude": 107.200427
 },
 {
   "STT": 358,
   "ten_co_so": "Nhà thuốc Tuấn Nguyên",
   "address": "48 Võ Văn Tần, phường Thắng Nhất, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.384156,
   "Latitude": 107.1013884
 },
 {
   "STT": 359,
   "ten_co_so": "Nhà thuốc Thanh Đức Phát",
   "address": "21D10 Lương Thế Vinh, phường 9, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3712946,
   "Latitude": 107.0865714
 },
 {
   "STT": 360,
   "ten_co_so": "Nhà thuốc Đăng Khoa",
   "address": "107 Lê Duẩn, phường Phước Nguyên, Bà Rịa, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4961408,
   "Latitude": 107.1782392
 },
 {
   "STT": 361,
   "ten_co_so": "TTTYT Tủ thuốc Trạm Y tế xã Bông Trang",
   "address": "Ấp Trang Hùng, xã Bông Trang, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5530141,
   "Latitude": 107.4561249
 },
 {
   "STT": 362,
   "ten_co_so": "Nhà thuốc Vân Khánh",
   "address": "84 Nguyễn Tri Phương, phường 7, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3621141,
   "Latitude": 107.0854837
 },
 {
   "STT": 363,
   "ten_co_so": "Quầy thuốc Văn Khánh",
   "address": "Lô 60, khu D5, chợ Lam Sơn, xã Phước Hòa, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5298175,
   "Latitude": 107.0802641
 },
 {
   "STT": 364,
   "ten_co_so": "Nhà thuốc Minh Nguyệt",
   "address": "117 đường Chu Mạnh Trinh, phường 8, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3541831,
   "Latitude": 107.0886701
 },
 {
   "STT": 365,
   "ten_co_so": "Nhà thuốc Hữu Trí",
   "address": "434 Trương Công Định, phường 8, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3606991,
   "Latitude": 107.0878066
 },
 {
   "STT": 366,
   "ten_co_so": "Nhà thuốc Nhân Tâm",
   "address": "40 Lưu Chí Hiếu, phường Thắng Nhất, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3931473,
   "Latitude": 107.1061761
 },
 {
   "STT": 367,
   "ten_co_so": "Nhà thuốc Phước Tâm",
   "address": "D13/6 Trung tâm đô thị Chí Linh, phường Nguyễn An Ninh, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3787996,
   "Latitude": 107.1118176
 },
 {
   "STT": 368,
   "ten_co_so": "Quầy thuốc Hoài Thanh",
   "address": "Tổ 01, ấp Phú Hòa, xã Hòa Hiệp, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6787414,
   "Latitude": 107.5031811
 },
 {
   "STT": 369,
   "ten_co_so": "Quầy thuốc Huệ Kiều",
   "address": "Tổ 6, ấp Phước Hưng, xã Mỹ Xuân, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6166667,
   "Latitude": 107.0666667
 },
 {
   "STT": 370,
   "ten_co_so": "Quầy thuốc Mỹ Kiều",
   "address": "Ấp Bình Hòa, xã Bình Châu, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.559203,
   "Latitude": 107.545337
 },
 {
   "STT": 371,
   "ten_co_so": "Nhà thuốc Kim",
   "address": "701A đường 30/4, phường 10, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4100484,
   "Latitude": 107.1275379
 },
 {
   "STT": 372,
   "ten_co_so": "Quầy thuốc Kim Thư",
   "address": "Thôn 10, xã Long Sơn, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4529906,
   "Latitude": 107.0896654
 },
 {
   "STT": 373,
   "ten_co_so": "Quầy thuốc Nhật Minh",
   "address": "Đường số 1, Trung Sơn, Suối Nghệ, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5865463,
   "Latitude": 107.2050624
 },
 {
   "STT": 374,
   "ten_co_so": "TTTYT Phòng khám đa khoa khu vực Hòa Hiệp",
   "address": "Ấp Phú Hòa, xã Hòa Hiệp, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6887378,
   "Latitude": 107.498116
 },
 {
   "STT": 375,
   "ten_co_so": "TTTYT Xã Bàu Lâm",
   "address": "Ấp 2 Đông, xã Bàu Lâm, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6970348,
   "Latitude": 107.3730563
 },
 {
   "STT": 376,
   "ten_co_so": "Quầy thuốc Dược Phương",
   "address": "Tổ 2, ấp Suối Nhum, xã Hắc Dịch, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6271597,
   "Latitude": 107.1005
 },
 {
   "STT": 377,
   "ten_co_so": "Quầy thuốc Hải Lâm",
   "address": "Tổ 8, ấp 5, xã Hòa Bình, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6155553,
   "Latitude": 107.3772141
 },
 {
   "STT": 378,
   "ten_co_so": "Quầy thuốc Hoàng Phương",
   "address": "Tổ 15, ấp Mỹ An, xã Long Mỹ, Đất Đỏ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4372227,
   "Latitude": 107.2725505
 },
 {
   "STT": 379,
   "ten_co_so": "Nhà thuốc Lâm Anh",
   "address": "12 Bắc Sơn, phường 11, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4015382,
   "Latitude": 107.1254641
 },
 {
   "STT": 380,
   "ten_co_so": "Nhà thuốc Nguyên Lâm",
   "address": "177 Xô Viết Nghệ Tĩnh, phường Thắng Tam, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3487552,
   "Latitude": 107.0868184
 },
 {
   "STT": 381,
   "ten_co_so": "Nhà thuốc Thu Ngọc",
   "address": "LK 2, 16 Khang Linh, Phường 10, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3920828,
   "Latitude": 107.1120319
 },
 {
   "STT": 382,
   "ten_co_so": "Quầy thuốc Bảo Khang",
   "address": "Tổ 16, khu phố Tân Hạnh, thị trấn Phú Mỹ, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5905345,
   "Latitude": 107.0480378
 },
 {
   "STT": 383,
   "ten_co_so": "Quầy thuốc Khoa Uyên",
   "address": "Quốc lộ 55, tổ 49, xã Xà Bang, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5004527,
   "Latitude": 107.2937532
 },
 {
   "STT": 384,
   "ten_co_so": "Quầy thuốc Minh Anh",
   "address": "Khu phố Vạn Hạnh, thị trấn Phú Mỹ, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5872811,
   "Latitude": 107.0622145
 },
 {
   "STT": 385,
   "ten_co_so": "Quầy thuốc Minh Chiến",
   "address": "ấp 1, xã Hòa Hội, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.615904,
   "Latitude": 107.440219
 },
 {
   "STT": 386,
   "ten_co_so": "Nhà thuốc PKĐK Thiên Nam",
   "address": "192 đường 3/2, phường 10, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3764888,
   "Latitude": 107.1141692
 },
 {
   "STT": 387,
   "ten_co_so": "Quầy thuốc Số 11",
   "address": "Tổ 06/16, Ô3, khu phố Hải Sơn, thị trấn Long Hải , Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.418488,
   "Latitude": 107.2833871
 },
 {
   "STT": 388,
   "ten_co_so": "Quầy thuốc Số 98",
   "address": "Số 153 Trần Phú, phường 5, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.384013,
   "Latitude": 107.0649205
 },
 {
   "STT": 389,
   "ten_co_so": "Nhà thuốc Thiên An",
   "address": "285A7, Bình Giã, phường 8, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3594208,
   "Latitude": 107.0933526
 },
 {
   "STT": 390,
   "ten_co_so": "Nhà thuốc Thu Trang",
   "address": "108 đường Nguyễn Tri Phương, phường 7, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3629322,
   "Latitude": 107.0845066
 },
 {
   "STT": 391,
   "ten_co_so": "Quầy thuốc Trường Thành",
   "address": "Tổ 1, khu phố Trảng Cát, phường Hắc Dịch, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6466114,
   "Latitude": 107.113007
 },
 {
   "STT": 392,
   "ten_co_so": "Nhà thuốc Nhật Lâm",
   "address": "170 Phạm Hồng Thái, phường 7, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3683672,
   "Latitude": 107.0829805
 },
 {
   "STT": 394,
   "ten_co_so": "Quầy thuốc Duy Anh",
   "address": "Kios B26, chợ Long Điền, thị trấn Long Điền, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4933173,
   "Latitude": 107.2168114
 },
 {
   "STT": 395,
   "ten_co_so": "Quầy thuốc Trinh Lê",
   "address": "Tổ 7, thôn Sơn Thuận, xã Xuân Sơn, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6404024,
   "Latitude": 107.3198424
 },
 {
   "STT": 396,
   "ten_co_so": "Quầy thuốc Trà My",
   "address": "Chợ xã Bưng Riềng, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5522225,
   "Latitude": 107.4839021
 },
 {
   "STT": 397,
   "ten_co_so": "Quầy thuốc Số 221",
   "address": "chợ Trung tâm hành chính Phước Bửu, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5352709,
   "Latitude": 107.4005413
 },
 {
   "STT": 398,
   "ten_co_so": "Quầy thuốc Thanh Bình",
   "address": "Tổ 1, ấp Phước Lộc, phường Tân Phước, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5513304,
   "Latitude": 107.0677194
 },
 {
   "STT": 399,
   "ten_co_so": "Quầy thuốc Thanh Bình",
   "address": "Tổ 1, ấp Phước Lộc, xã Tân Phước, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5639613,
   "Latitude": 107.0744052
 },
 {
   "STT": 400,
   "ten_co_so": "Quầy thuốc Thiên Thư",
   "address": "Tổ 8, ấp Phước Trung, xã Phước Long Thọ, Đất Đỏ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5059422,
   "Latitude": 107.3021069
 },
 {
   "STT": 401,
   "ten_co_so": "Quầy thuốc Phúc Bình",
   "address": "Tổ 14, khu phố Phước Lập, phường Mỹ Xuân, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6396214,
   "Latitude": 107.0521974
 },
 {
   "STT": 402,
   "ten_co_so": "Quầy thuốc Kim Lên",
   "address": "Thôn Quảng Long, xã Kim Long, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.70531,
   "Latitude": 107.231283
 },
 {
   "STT": 403,
   "ten_co_so": "TTTYT Xã Tân Phước",
   "address": "Ấp Ông Trịnh, xã Tân Phước, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5624307,
   "Latitude": 107.0612418
 },
 {
   "STT": 404,
   "ten_co_so": "thuốc từ DL Bá Hòa Đường",
   "address": "201 (B16) Cách Mạng Tháng 8, phường  Phước Hiệp, Bà Rịa, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4962567,
   "Latitude": 107.1753752
 },
 {
   "STT": 405,
   "ten_co_so": "Quầy thuốc Bích Liên",
   "address": "Tổ 27, thôn Quảng Thành 1, xã Nghĩa Thành, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5718057,
   "Latitude": 107.1898113
 },
 {
   "STT": 406,
   "ten_co_so": "Quầy thuốc Hoàng Liên",
   "address": "Tổ 1, ấp Phước Hưng, xã Mỹ Xuân, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.65272,
   "Latitude": 107.043383
 },
 {
   "STT": 408,
   "ten_co_so": "Quầy thuốc Liên Đào",
   "address": "Ấp Bình Trung, xã Bình Châu, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5603904,
   "Latitude": 107.5411257
 },
 {
   "STT": 409,
   "ten_co_so": "Quầy thuốc Nguyễn Học",
   "address": "Khu phố Phước An, thị trấn Phước Bửu, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5324543,
   "Latitude": 107.406616
 },
 {
   "STT": 410,
   "ten_co_so": "Quầy thuốc Trà My",
   "address": "Số 21/5, ấp Thạnh Sơn 2A, xã Phước Tân, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5669449,
   "Latitude": 107.3730563
 },
 {
   "STT": 411,
   "ten_co_so": "Quầy thuốc Nguyên Khang",
   "address": "Tổ 10, ấp Tây, xã Hòa Long, Bà Rịa, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5277905,
   "Latitude": 107.2016295
 },
 {
   "STT": 412,
   "ten_co_so": "Quầy thuốc Nhân Tâm",
   "address": "Đường 25, tổ 46, thôn Vinh Sơn, xã Nghĩa Thành, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5828685,
   "Latitude": 107.2061775
 },
 {
   "STT": 413,
   "ten_co_so": "Quầy thuốc Thanh Nhân",
   "address": "Tổ 1, ấp 5, xã Hắc Dịch, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6466114,
   "Latitude": 107.113007
 },
 {
   "STT": 414,
   "ten_co_so": "Quầy thuốc Đình Linh",
   "address": "Ấp 4, xã Hòa Bình, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6094444,
   "Latitude": 107.3966667
 },
 {
   "STT": 415,
   "ten_co_so": "Nhà thuốc Đức Dũng",
   "address": "113 Nguyễn Hữu Cảnh, phường Thắng Nhất, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3887057,
   "Latitude": 107.1059616
 },
 {
   "STT": 416,
   "ten_co_so": "Quầy thuốc Huyền Linh",
   "address": "01 ấp 4, xã Hòa Bình, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.633267,
   "Latitude": 107.3719168
 },
 {
   "STT": 417,
   "ten_co_so": "Nhà thuốc Khánh Linh",
   "address": "82A Hoàng Hoa Thám,  P. 2, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.34078,
   "Latitude": 107.0789056
 },
 {
   "STT": 418,
   "ten_co_so": "Nhà thuốc Kim Hồng",
   "address": "174/23 Nguyễn Thiện Thuật, phường Thắng Nhất, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3832949,
   "Latitude": 107.0987191
 },
 {
   "STT": 419,
   "ten_co_so": "Quầy thuốc Số 197",
   "address": "126 Lê Hồng Phong, thị trấn Ngãi Giao, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6471467,
   "Latitude": 107.2455896
 },
 {
   "STT": 420,
   "ten_co_so": "Quầy thuốc Thảo Linh",
   "address": "Tổ 9, khu phố Hải Tân, thị trấn Long Hải, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3851624,
   "Latitude": 107.2401989
 },
 {
   "STT": 421,
   "ten_co_so": "Quầy thuốc Tường Linh",
   "address": "Tổ 14, thôn Việt Cường, xã Cù Bị, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.7294922,
   "Latitude": 107.1839024
 },
 {
   "STT": 422,
   "ten_co_so": "TTTYT Xã Phước Hòa",
   "address": "Ấp Tân Lộc, xã Phước Hòa, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5144444,
   "Latitude": 107.0477778
 },
 {
   "STT": 423,
   "ten_co_so": "TTTYT Xã Phước Hưng",
   "address": "Ấp Lò Vôi, xã Phước Hưng, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4163582,
   "Latitude": 107.2156779
 },
 {
   "STT": 424,
   "ten_co_so": "Quầy thuốc Hồng Ngọc",
   "address": "Tổ 15/8, khu phố  Hải Vân, thị trấn Long Hải, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.395902,
   "Latitude": 107.2370874
 },
 {
   "STT": 425,
   "ten_co_so": "Quầy thuốc An Bình",
   "address": "3/9 D1, ấp An Bình, xã An Ngãi, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4513168,
   "Latitude": 107.2134482
 },
 {
   "STT": 426,
   "ten_co_so": "Quầy thuốc Dạ Thảo",
   "address": "QL56, tổ 57, thôn Tam Long, xã Kim Long, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.7174929,
   "Latitude": 107.2303927
 },
 {
   "STT": 427,
   "ten_co_so": "Quầy thuốc Đại Lộc",
   "address": "04A, tổ 3, ấp Phước Thọ, xã Phước Hưng, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4124996,
   "Latitude": 107.2058125
 },
 {
   "STT": 428,
   "ten_co_so": "Quầy thuốc Hồng Loan",
   "address": "Số 01, ấp Phước Hưng, xã Tam Phước, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4283624,
   "Latitude": 107.2370874
 },
 {
   "STT": 429,
   "ten_co_so": "Nhà thuốc Kim An",
   "address": "294 đường 30/4, phường Rạch Dừa, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3956,
   "Latitude": 107.1067
 },
 {
   "STT": 430,
   "ten_co_so": "Quầy thuốc Kim Loan",
   "address": "462 Hùng Vương, trị trấn Ngãi Giao, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6592434,
   "Latitude": 107.2480479
 },
 {
   "STT": 431,
   "ten_co_so": "Quầy thuốc Lộc An",
   "address": "Tổ 23, ấp An Điền, xã Lộc An, Đất Đỏ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4765664,
   "Latitude": 107.3434916
 },
 {
   "STT": 432,
   "ten_co_so": "Quầy thuốc Nguyên",
   "address": "27/4, 89 KP. Phước Hòa, TT. Phước Bửu, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.529915,
   "Latitude": 107.4055814
 },
 {
   "STT": 433,
   "ten_co_so": "Quầy thuốc Nguyễn Loan",
   "address": "số 01 Bùi Công Minh, TT. Long Điền, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4847632,
   "Latitude": 107.2119106
 },
 {
   "STT": 434,
   "ten_co_so": "Quầy thuốc Như Loan",
   "address": "Tổ 7, thôn 10, xã Long Sơn, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.446867,
   "Latitude": 107.087897
 },
 {
   "STT": 436,
   "ten_co_so": "Quầy thuốc Trang Hoàng",
   "address": "I20 ấp Tân Lập, xã Phước Tỉnh, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4118663,
   "Latitude": 107.1927658
 },
 {
   "STT": 437,
   "ten_co_so": "Quầy thuốc Trúc Linh",
   "address": "Ấp Nhân Nghĩa, xã Xuyên Mộc, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5564986,
   "Latitude": 107.4262813
 },
 {
   "STT": 438,
   "ten_co_so": "Quầy thuốc Trường An",
   "address": "Tổ 70, đường 41, thôn Trung Nghĩa, xã Nghĩa Thành, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5718057,
   "Latitude": 107.1898113
 },
 {
   "STT": 439,
   "ten_co_so": "Quầy thuốc Uyên Loan",
   "address": "ấp 4, xã Hòa Bình, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6094444,
   "Latitude": 107.3966667
 },
 {
   "STT": 440,
   "ten_co_so": "TTTYT Xã Sông Xoài",
   "address": "Tổ 7, ấp Sông Xoài II, xã Sông Xoài, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.7053432,
   "Latitude": 107.1479108
 },
 {
   "STT": 441,
   "ten_co_so": "TTTYT Xã Xà Bang",
   "address": "Ấp Liên Lộc, xã Xà Bang, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.7261907,
   "Latitude": 107.2408177
 },
 {
   "STT": 442,
   "ten_co_so": "Quầy thuốc Đức Thiện",
   "address": "Tổ 9, thôn 4, xã Long Sơn, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.446867,
   "Latitude": 107.087897
 },
 {
   "STT": 444,
   "ten_co_so": "Quầy thuốc Số 184",
   "address": "Khu phố 2, phường Hắc Dịch, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6466114,
   "Latitude": 107.113007
 },
 {
   "STT": 445,
   "ten_co_so": "Nhà thuốc Tai Mũi Họng",
   "address": "Số 106 -108 Phạm Ngọc Thạch, phường Phước Nguyên, Bà Rịa, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5048599,
   "Latitude": 107.1787498
 },
 {
   "STT": 446,
   "ten_co_so": "Quầy thuốc Tấn Lộc",
   "address": "118/4 ấp Trang Hoàng, xã Bông Trang, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5661288,
   "Latitude": 107.4429863
 },
 {
   "STT": 447,
   "ten_co_so": "Nhà thuốc Trần Phú",
   "address": "606 Trần Phú, phường 5, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3749056,
   "Latitude": 107.071235
 },
 {
   "STT": 448,
   "ten_co_so": "Nhà thuốc Hiền Hòa",
   "address": "53 đường Nguyễn thiện Thuật, phường Thắng Nhất, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3880233,
   "Latitude": 107.1011269
 },
 {
   "STT": 449,
   "ten_co_so": "Nhà thuốc Minh Khang",
   "address": "116 Đô Lương, phường 11, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4039942,
   "Latitude": 107.143786
 },
 {
   "STT": 450,
   "ten_co_so": "Quầy thuốc Số 173",
   "address": "Quầy A35, Chợ TT Phước Hải,, Đất Đỏ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4183205,
   "Latitude": 107.2867768
 },
 {
   "STT": 451,
   "ten_co_so": "Quầy thuốc Đức Long",
   "address": "Ấp Bình Đức, xã Bình Ba, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6183333,
   "Latitude": 107.2348877
 },
 {
   "STT": 452,
   "ten_co_so": "Quầy thuốc Hồng Phúc",
   "address": "Số 32W6, tổ 12, khu phố Long Phượng, thị trấn Long Điền, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5056039,
   "Latitude": 107.1883273
 },
 {
   "STT": 453,
   "ten_co_so": "Quầy thuốc Hồng Phúc",
   "address": "Tổ 9, ấp Phước Lập, xã Mỹ Xuân, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6456132,
   "Latitude": 107.0495635
 },
 {
   "STT": 454,
   "ten_co_so": "Nhà thuốc Như Phong",
   "address": "41 Nguyễn An Ninh, phường Thắng Nhì, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3701756,
   "Latitude": 107.0776468
 },
 {
   "STT": 455,
   "ten_co_so": "Nhà thuốc Phòng khám ĐK Quốc tế thế giới mới",
   "address": "22 Trần Hưng Đạo, phường 1, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.343671,
   "Latitude": 107.0772117
 },
 {
   "STT": 456,
   "ten_co_so": "Quầy thuốc Số 176",
   "address": "72 khu phố Phước Thới, thị trấn Đất Đỏ, Đất Đỏ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4902936,
   "Latitude": 107.2711051
 },
 {
   "STT": 457,
   "ten_co_so": "Nhà thuốc Duy Phát",
   "address": "225 Lê Quang Định, phường Thắng Nhất, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3776493,
   "Latitude": 107.1011102
 },
 {
   "STT": 458,
   "ten_co_so": "Nhà thuốc Tâm Phát",
   "address": "754 Bình Giã, phường 10, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3873991,
   "Latitude": 107.1183572
 },
 {
   "STT": 459,
   "ten_co_so": "Nhà thuốc Kim Hòa",
   "address": "274 đường 30/4, phường Rạch Dừa, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.408268,
   "Latitude": 107.1286931
 },
 {
   "STT": 460,
   "ten_co_so": "Quầy thuốc Thiên An",
   "address": "Tổ 2, khu phố Vạn Hạnh, thị trấn Phú Mỹ, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5872811,
   "Latitude": 107.0622145
 },
 {
   "STT": 461,
   "ten_co_so": "TTTYT Xã Bưng Riềng",
   "address": "Ấp 1, xã Bưng Riềng, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.538601,
   "Latitude": 107.4913489
 },
 {
   "STT": 462,
   "ten_co_so": "Quầy thuốc Hiền Lương",
   "address": "Tổ 4, ấp Mỹ Thạnh, xã Mỹ Xuân, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6180165,
   "Latitude": 107.0642634
 },
 {
   "STT": 463,
   "ten_co_so": "Nhà thuốc Cát Tường",
   "address": "505 đường Trần Phú, phường Thắng Nhì, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3760836,
   "Latitude": 107.0706112
 },
 {
   "STT": 464,
   "ten_co_so": "Quầy thuốc Lưu Luyến",
   "address": "Ấp Hồ Tràm, xã Phước Thuận, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4932137,
   "Latitude": 107.3982236
 },
 {
   "STT": 466,
   "ten_co_so": "Quầy thuốc Hoàng Quân",
   "address": "Tổ 5, ấp Phú Hòa, xã Hòa Hiệp, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6874344,
   "Latitude": 107.4963406
 },
 {
   "STT": 467,
   "ten_co_so": "Quầy thuốc Minh Ý",
   "address": "Tổ 9, ấp Thị Vải, phường Mỹ Xuân, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.620929,
   "Latitude": 107.055887
 },
 {
   "STT": 468,
   "ten_co_so": "Quầy thuốc Phúc An",
   "address": "Số 10, tổ 10, ấp Phước Lâm, xã Phước Hưng, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4155606,
   "Latitude": 107.2102525
 },
 {
   "STT": 469,
   "ten_co_so": "Quầy thuốc Hà Linh",
   "address": "Tổ 1, ấp 1, xã Tóc Tiên, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6070711,
   "Latitude": 107.1173423
 },
 {
   "STT": 470,
   "ten_co_so": "Nhà thuốc Khánh Hậu",
   "address": "126 A15, khu Á Châu, Hoàng Hoa Thám, phường 2, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.338598,
   "Latitude": 107.084622
 },
 {
   "STT": 471,
   "ten_co_so": "Quầy thuốc Minh Lý",
   "address": "Tổ 8, thôn Hữu Phước, xã Suối Nghệ, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5837983,
   "Latitude": 107.204783
 },
 {
   "STT": 472,
   "ten_co_so": "Quầy thuốc Mỹ Xuân",
   "address": "Ấp Bến Đình, xã Mỹ Xuân, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6170354,
   "Latitude": 107.0706189
 },
 {
   "STT": 473,
   "ten_co_so": "Quầy thuốc Ngọc Diệp",
   "address": "Tổ 4, ấp Phước Hưng, xã Mỹ Xuân, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6180165,
   "Latitude": 107.0642634
 },
 {
   "STT": 474,
   "ten_co_so": "Quầy thuốc Phước Nhân",
   "address": "Lô C52, ấp Song Vĩnh, xã Tân Phước, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.565034,
   "Latitude": 107.05809
 },
 {
   "STT": 475,
   "ten_co_so": "Quầy thuốc Quỳnh Anh",
   "address": "Tổ 1, ấp Bình Hải, xã Bình Châu, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.560017,
   "Latitude": 107.5474337
 },
 {
   "STT": 476,
   "ten_co_so": "Quầy thuốc Số 232",
   "address": "Tổ 10, ấp Phú Bình, xã Hòa Hiệp, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6947322,
   "Latitude": 107.4973509
 },
 {
   "STT": 477,
   "ten_co_so": "Nhà thuốc Công Định",
   "address": "486B Trương Công Định, phường 8, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3624606,
   "Latitude": 107.0887532
 },
 {
   "STT": 478,
   "ten_co_so": "Nhà thuốc Hà Anh",
   "address": "162 Xô Viết Nghệ Tĩnh, phường Thắng Tam, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3492139,
   "Latitude": 107.0872138
 },
 {
   "STT": 479,
   "ten_co_so": "Quầy thuốc Hoàng Mai",
   "address": "297/18 tổ 18, ấp Thạnh Sơn 2B, xã Phước Tân, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 15.9128998,
   "Latitude": 79.7399875
 },
 {
   "STT": 480,
   "ten_co_so": "Quầy thuốc Khánh Mai",
   "address": "Kios số 01, vị trí 3, chợ Hòa Long, xã Hòa Long, Bà Rịa, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5211045,
   "Latitude": 107.2055403
 },
 {
   "STT": 481,
   "ten_co_so": "Quầy thuốc Kim Ngân",
   "address": "Tô 09, thôn Lạc Long, xã Kim Long, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.7050987,
   "Latitude": 107.2311774
 },
 {
   "STT": 482,
   "ten_co_so": "Quầy thuốc Ngọc Hiền",
   "address": "Số 1A/1 ấp Phước Lâm, xã Phước Hưng, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4124996,
   "Latitude": 107.2058125
 },
 {
   "STT": 483,
   "ten_co_so": "Quầy thuốc Ngọc Mai",
   "address": "250 Võ Thị Sáu, khu phố Long Tân, thị trấn Long Điền, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4848035,
   "Latitude": 107.2137452
 },
 {
   "STT": 484,
   "ten_co_so": "Quầy thuốc Ngọc Mai",
   "address": "Tổ 26, thôn Hiệp Long, xã Kim Long, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.7050987,
   "Latitude": 107.2311774
 },
 {
   "STT": 485,
   "ten_co_so": "Quầy thuốc Quỳnh Mai",
   "address": "Tổ 1, ấp Lộc Hòa, xã Bình Giã, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6437421,
   "Latitude": 107.2607289
 },
 {
   "STT": 486,
   "ten_co_so": "Quầy thuốc số 157",
   "address": "201 Võ Thị Sáu, khu phố Long Nguyên, thị trấn Long Điền, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4834982,
   "Latitude": 107.2065182
 },
 {
   "STT": 487,
   "ten_co_so": "Quầy thuốc Số 189",
   "address": "Khu phố Ông Trịnh, phường Tân Phước, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5593409,
   "Latitude": 107.0530044
 },
 {
   "STT": 488,
   "ten_co_so": "Quầy thuốc Số 196",
   "address": "Ấp Liên Lộc, xã Xà Bang, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.7261907,
   "Latitude": 107.2408177
 },
 {
   "STT": 489,
   "ten_co_so": "Quầy thuốc Tuyết Mai",
   "address": "889 khu phố Phước Sơn, thị trấn Đất Đỏ, Đất Đỏ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4900131,
   "Latitude": 107.2712946
 },
 {
   "STT": 490,
   "ten_co_so": "Quầy thuốc Tuyết Mai",
   "address": "Tổ 4, thôn Vĩnh Bình, xã Bình Giã, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6437421,
   "Latitude": 107.2607289
 },
 {
   "STT": 491,
   "ten_co_so": "Quầy thuốc Vĩnh Phát",
   "address": "Ấp Bình Tiến, xã Bình Châu, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5582593,
   "Latitude": 107.5393641
 },
 {
   "STT": 492,
   "ten_co_so": "Quầy thuốc Xuân Mai",
   "address": "F12 tổ 1, ấp Phước Lợi, xã Phước Tỉnh, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4118663,
   "Latitude": 107.1927658
 },
 {
   "STT": 493,
   "ten_co_so": "Quầy thuốc Nhân Nghĩa",
   "address": "Tồ 32/6, Ô 3,  khu phố Hải Hòa, thị trấn Long Hải, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.395902,
   "Latitude": 107.2370874
 },
 {
   "STT": 494,
   "ten_co_so": "Nhà thuốc Hà Nguyễn",
   "address": "588 Trương Công Định, phường Nguyễn An Ninh, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3657521,
   "Latitude": 107.0902779
 },
 {
   "STT": 495,
   "ten_co_so": "Quầy thuốc 2156",
   "address": "Tổ 14, ấp Phước Hưng, xã Mỹ Xuân, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6166667,
   "Latitude": 107.0666667
 },
 {
   "STT": 496,
   "ten_co_so": "Quầy thuốc Bảo Khánh",
   "address": "Số 03, tổ 17, ấp Phước Lâm, xã Phước Hưng, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4146365,
   "Latitude": 107.2046113
 },
 {
   "STT": 497,
   "ten_co_so": "Quầy thuốc Bình Minh",
   "address": "Số 04, tổ 23, ấp Phước Lâm, xã Phước Hưng, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4146365,
   "Latitude": 107.2046113
 },
 {
   "STT": 498,
   "ten_co_so": "YHCT Đại lý thành phẩm YHCT",
   "address": "848 khu phố Phước Thới, thị trấn Đất Đỏ, Đất Đỏ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4900131,
   "Latitude": 107.2712946
 },
 {
   "STT": 499,
   "ten_co_so": "Nhà thuốc Minh Châu",
   "address": "2400 Lê Duẩn, phường Phước Nguyên, Bà Rịa, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4931446,
   "Latitude": 107.1770627
 },
 {
   "STT": 500,
   "ten_co_so": "Nhà thuốc Minh Châu",
   "address": "34-36 Huỳnh Minh Thạnh, thị trấn Phước Bửu, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5341688,
   "Latitude": 107.3994655
 },
 {
   "STT": 501,
   "ten_co_so": "Quầy thuốc Nguyên Thảo 2",
   "address": "số 14, Tổ 18, ấp Phước Thọ, xã Phước Hưng, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4124996,
   "Latitude": 107.2058125
 },
 {
   "STT": 502,
   "ten_co_so": "Quầy thuốc Như Phúc",
   "address": "Khu phố Mỹ Thạnh, phường Mỹ Xuân, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6231602,
   "Latitude": 107.0545273
 },
 {
   "STT": 503,
   "ten_co_so": "Quầy thuốc Số 03",
   "address": "61 Lê Quý Đôn, phường Phước Trung, Bà Rịa, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4923016,
   "Latitude": 107.1714499
 },
 {
   "STT": 504,
   "ten_co_so": "Quầy thuốc Số 109",
   "address": "344 đường 30/4, phường Rạch Dừa, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.408268,
   "Latitude": 107.1286931
 },
 {
   "STT": 505,
   "ten_co_so": "Quầy thuốc số 226",
   "address": "Chợ thị trấn Phước Bửu, thị trấn Phước Bửu, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5353285,
   "Latitude": 107.4055814
 },
 {
   "STT": 506,
   "ten_co_so": "Nhà thuốc Văn Hiếu",
   "address": "05 Hoàng Văn Thụ, phường 7, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3648532,
   "Latitude": 107.0886833
 },
 {
   "STT": 507,
   "ten_co_so": "Nhà thuốc Mai Hương",
   "address": "156A, Hoàng Hoa Thám, phường 2, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3350019,
   "Latitude": 107.0870009
 },
 {
   "STT": 508,
   "ten_co_so": "Quầy thuốc Thái Bình",
   "address": "Ấp Phú Thạnh, Xã Mỹ Xuân, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6166667,
   "Latitude": 107.0666667
 },
 {
   "STT": 509,
   "ten_co_so": "Quầy thuốc Đương Ngân",
   "address": "tổ 1, ấp Hải Lâm, xã Phước Hưng, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4233104,
   "Latitude": 107.2213893
 },
 {
   "STT": 510,
   "ten_co_so": "Nhà thuốc Kim Hằng",
   "address": "478 Trần Phú, phường 5, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3804345,
   "Latitude": 107.0665121
 },
 {
   "STT": 511,
   "ten_co_so": "Quầy thuốc Số 164",
   "address": "Kios 32A, chợ Long Điền, thị trấn Long Điền, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4940076,
   "Latitude": 107.2145613
 },
 {
   "STT": 512,
   "ten_co_so": "Quầy thuốc Hoàng Phát",
   "address": "494 tổ 20, Thôn Trung Sơn, xã Suối Nghệ, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6043384,
   "Latitude": 107.1898113
 },
 {
   "STT": 513,
   "ten_co_so": "Quầy thuốc Ngọc Trâm",
   "address": "Tổ 11, ấp Bình Đức, xã Bình Ba, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6182884,
   "Latitude": 107.2311774
 },
 {
   "STT": 514,
   "ten_co_so": "Nhà thuốc Lê Lai",
   "address": "112 Lê Lai, phường 3, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3491014,
   "Latitude": 107.0785473
 },
 {
   "STT": 515,
   "ten_co_so": "Quầy thuốc Mỹ Hường",
   "address": "Tổ 02, ấp An Thạnh , xã An Ngãi, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4696887,
   "Latitude": 107.2140964
 },
 {
   "STT": 516,
   "ten_co_so": "Quầy thuốc Ngọc Mỹ",
   "address": "A6 nhà lồng số 1, chợ Bình Châu, xã Bình Châu, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5553344,
   "Latitude": 107.5483173
 },
 {
   "STT": 517,
   "ten_co_so": "thuốc từ DL Bá An Đường",
   "address": "69 Lê Quý Đôn, khu phố 2, phường Phước Trung, Bà Rịa, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4914922,
   "Latitude": 107.1761933
 },
 {
   "STT": 518,
   "ten_co_so": "Quầy thuốc Hoàng Ân",
   "address": "133 Hùng Vương, TT. Ngãi Giao, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6437428,
   "Latitude": 107.244934
 },
 {
   "STT": 519,
   "ten_co_so": "Nhà thuốc Long Tâm",
   "address": "254B Bình Giã, phường 8, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3591226,
   "Latitude": 107.0939691
 },
 {
   "STT": 520,
   "ten_co_so": "TTTYT TYT Hòa Hội",
   "address": "Ấp 2, xã Hòa Hội, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.622042,
   "Latitude": 107.4332026
 },
 {
   "STT": 521,
   "ten_co_so": "Quầy thuốc Vân Nam",
   "address": "1/22 khu phố Hải Hà 2, thị trấn Long Hải, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.395902,
   "Latitude": 107.2370874
 },
 {
   "STT": 522,
   "ten_co_so": "Nhà thuốc Minh Hạt",
   "address": "24C Lê Phụng Hiểu, phường 8, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3580162,
   "Latitude": 107.0916464
 },
 {
   "STT": 523,
   "ten_co_so": "Nhà thuốc Minh Ngân 165B",
   "address": "165B đường Đô Lương, phường 12, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.410713,
   "Latitude": 107.139232
 },
 {
   "STT": 524,
   "ten_co_so": "Quầy thuốc Ngân Nga",
   "address": "Thôn 6, xã Long Sơn, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4637689,
   "Latitude": 107.0842094
 },
 {
   "STT": 525,
   "ten_co_so": "Quầy thuốc Nguyệt Nga",
   "address": "Tổ 2, thôn Bầu Điển, xã Đá Bạc, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5939448,
   "Latitude": 107.2784615
 },
 {
   "STT": 526,
   "ten_co_so": "Quầy thuốc Thành Công",
   "address": "Tổ 13, ấp Phước Lộc, xã Phước Hưng, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4163017,
   "Latitude": 107.2104935
 },
 {
   "STT": 527,
   "ten_co_so": "Quầy thuốc Thanh Nga",
   "address": "Tổ 4, khu 3, ấp Ông Trịnh, xã Tân Phước, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5598524,
   "Latitude": 107.0542708
 },
 {
   "STT": 528,
   "ten_co_so": "Quầy thuốc Thanh Tâm",
   "address": "Tổ 1, ấp Mỹ Thạnh, xã Mỹ Xuân, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6327258,
   "Latitude": 107.0548264
 },
 {
   "STT": 529,
   "ten_co_so": "TTTYT Thị trấn Phước Bửu",
   "address": "Khu phố Xóm Rẫy, thị trấn Phước Bửu, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5229889,
   "Latitude": 107.4020047
 },
 {
   "STT": 530,
   "ten_co_so": "Quầy thuốc Thu Nga",
   "address": "Tổ 10/27, khu phố Hải Vân, thị trấn Long Hải, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.395902,
   "Latitude": 107.2370874
 },
 {
   "STT": 531,
   "ten_co_so": "Quầy thuốc Bảo Ngân",
   "address": "Tổ 34, thôn 5, xã Bình Trung, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6424077,
   "Latitude": 107.2843726
 },
 {
   "STT": 532,
   "ten_co_so": "Quầy thuốc Duy Ngân",
   "address": "Số 147/6, ấp Phú Thiện, xã Hòa Hiệp, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6787414,
   "Latitude": 107.5031811
 },
 {
   "STT": 533,
   "ten_co_so": "Nhà thuốc Kim Hằng",
   "address": "43 đường Ngô Đức Kế, phường 7, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3646051,
   "Latitude": 107.0809561
 },
 {
   "STT": 534,
   "ten_co_so": "Quầy thuốc Kim Ngân",
   "address": "Ấp 3, xã Bàu Lâm, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6804293,
   "Latitude": 107.3838553
 },
 {
   "STT": 535,
   "ten_co_so": "Quầy thuốc Ngọc Thu",
   "address": "Số 13A2, Lê Lợi, thị trấn Ngãi Giao, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6513589,
   "Latitude": 107.2407474
 },
 {
   "STT": 536,
   "ten_co_so": "Quầy thuốc Số 195",
   "address": "Ấp Thị Vải, xã Mỹ Xuân, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.620929,
   "Latitude": 107.055887
 },
 {
   "STT": 537,
   "ten_co_so": "Quầy thuốc Thanh Tâm",
   "address": "Tổ 14, ấp Phước Thắng, xã Phước Tỉnh, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4118663,
   "Latitude": 107.1927658
 },
 {
   "STT": 538,
   "ten_co_so": "Nhà thuốc Phương Mai",
   "address": "05 Nguyễn Hữu Cảnh, phường Thắng Nhất, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3916015,
   "Latitude": 107.1009877
 },
 {
   "STT": 539,
   "ten_co_so": "Nhà thuốc Bệnh viện Bà Rịa",
   "address": "17 Phạm Ngọc Thạch, phường Phước Hưng, Bà Rịa, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5083495,
   "Latitude": 107.1758612
 },
 {
   "STT": 540,
   "ten_co_so": "Quầy thuốc Hữu Nghĩa",
   "address": "Ấp Tiến Thành, xã Quảng Thành, , Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.694576,
   "Latitude": 107.2782896
 },
 {
   "STT": 541,
   "ten_co_so": "TTTYT Xã Hắc Dịch",
   "address": "Ấp Trảng Lớn, xã hắc Dịch, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.65251,
   "Latitude": 107.112179
 },
 {
   "STT": 542,
   "ten_co_so": "Quầy thuốc Hồng Đức",
   "address": "Tổ 01, ấp Phước Thọ, xã Phước Hưng, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4158576,
   "Latitude": 107.2153233
 },
 {
   "STT": 543,
   "ten_co_so": "Nhà thuốc A+Pharmacy",
   "address": "01 Võ Văn Tần, phường Thắng Nhất, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3855784,
   "Latitude": 107.1001094
 },
 {
   "STT": 544,
   "ten_co_so": "TTTYT Xã Tóc Tiên",
   "address": "Ấp 2, xã Tóc Tiên, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5903171,
   "Latitude": 107.1074504
 },
 {
   "STT": 545,
   "ten_co_so": "Quầy thuốc Bảo Quyên",
   "address": "Số 14/1A, tổ 1B, ấp An Ngãi, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 15.9128998,
   "Latitude": 79.7399875
 },
 {
   "STT": 546,
   "ten_co_so": "Quầy thuốc Bích Ngọc",
   "address": "Ấp Phước Lập, xã Mỹ Xuân, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6456132,
   "Latitude": 107.0495635
 },
 {
   "STT": 547,
   "ten_co_so": "Quầy thuốc Châu Ngọc",
   "address": "Ấp Gia Hòa Yên, xã Bình Giã, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6437421,
   "Latitude": 107.2607289
 },
 {
   "STT": 548,
   "ten_co_so": "Quầy thuốc Hoàng Yến",
   "address": "Chung cư lô E, số 10, thôn Vạn Hạnh, thị trấn Phú mỹ, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5892425,
   "Latitude": 107.0710693
 },
 {
   "STT": 549,
   "ten_co_so": "Quầy thuốc Kim Ngọc",
   "address": "Tổ 29, thôn Sơn Lập, xã Sơn Bình , Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.649907,
   "Latitude": 107.3434916
 },
 {
   "STT": 550,
   "ten_co_so": "Quầy thuốc Lộc Ngân",
   "address": "Số 191, tổ 10, ấp Phước Sơn, xã Phước Long Thọ, Đất Đỏ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5059422,
   "Latitude": 107.3021069
 },
 {
   "STT": 551,
   "ten_co_so": "Quầy thuốc Ngọc Tuyền",
   "address": "Tổ 1, ấp Bình Đức, xã Bình Ba, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6174118,
   "Latitude": 107.234279
 },
 {
   "STT": 552,
   "ten_co_so": "Quầy thuốc Nguyên Ngọc",
   "address": "Tổ 5, thôn Phước Hiệp, xã Tân Hòa, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4983607,
   "Latitude": 107.1706079
 },
 {
   "STT": 553,
   "ten_co_so": "Nhà thuốc Nhi Sài Gòn",
   "address": "518 CMT8, phường Phước Trung, Bà Rịa, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4874905,
   "Latitude": 107.1918614
 },
 {
   "STT": 554,
   "ten_co_so": "Quầy thuốc Như Ngọc",
   "address": "Trung tâm thương mại huyện Châu Đức, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6479007,
   "Latitude": 107.238735
 },
 {
   "STT": 555,
   "ten_co_so": "Nhà thuốc Ninh An",
   "address": "137A Hoàng Hoa Thám, phường Thắng Tam, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.340298,
   "Latitude": 107.0831013
 },
 {
   "STT": 556,
   "ten_co_so": "Quầy thuốc Phúc Hưng Thịnh",
   "address": "Tổ 1, ấp Phước Lập, xã Mỹ Xuân, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6456132,
   "Latitude": 107.0495635
 },
 {
   "STT": 558,
   "ten_co_so": "Quầy thuốc An Bình",
   "address": "295 Hùng Vương, thị trấn Ngãi Giao, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6495,
   "Latitude": 107.2463509
 },
 {
   "STT": 559,
   "ten_co_so": "Quầy thuốc Thiên Ân 1",
   "address": "Tổ 10, ấp Phước Lập, xã Mỹ Xuân, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6456132,
   "Latitude": 107.0495635
 },
 {
   "STT": 560,
   "ten_co_so": "Nhà thuốc Hồng Ánh",
   "address": "Tầng trệt, tòa nhà Lapen Center, 33A đường 30/4,  Phường 9 , Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3699116,
   "Latitude": 107.0849744
 },
 {
   "STT": 561,
   "ten_co_so": "Quầy thuốc Tâm Thiện",
   "address": "568 tổ 1, khu phố Tường Thành, thị trấn Đất Đỏ, Đất Đỏ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4913475,
   "Latitude": 107.2725505
 },
 {
   "STT": 562,
   "ten_co_so": "Nhà thuốc Tú Anh",
   "address": "Số 02, đường Hùng Vương, phường Phước Nguyên, Bà Rịa, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5011303,
   "Latitude": 107.1740188
 },
 {
   "STT": 563,
   "ten_co_so": "Quầy thuốc Ánh Nguyệt",
   "address": "Tổ 36, thôn 5, xã Bình Trung, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6462751,
   "Latitude": 107.2815315
 },
 {
   "STT": 564,
   "ten_co_so": "Quầy thuốc Bảo Châu 1",
   "address": "Lô A12, ấp Tân Lập, xã Phước Tỉnh, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4118663,
   "Latitude": 107.1927658
 },
 {
   "STT": 565,
   "ten_co_so": "thuốc từ DL Cở sở Đông Y Minh Nguyệt Đường",
   "address": "40 Nam Kỳ Khởi Nghĩa, phường Thắng Tam, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3446473,
   "Latitude": 107.0833915
 },
 {
   "STT": 566,
   "ten_co_so": "Quầy thuốc Minh Nguyệt",
   "address": "315 Khu phố Trường Thành, Đất Đỏ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4900131,
   "Latitude": 107.2712946
 },
 {
   "STT": 567,
   "ten_co_so": "Quầy thuốc Như Nguyệt",
   "address": "Tổ 7, khu phố Tân Hạnh, phường Phú Mỹ, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5905345,
   "Latitude": 107.0480378
 },
 {
   "STT": 568,
   "ten_co_so": "Nhà thuốc Thành Hưng",
   "address": "888/11 đường 30/4, phường 11, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4100558,
   "Latitude": 107.1276599
 },
 {
   "STT": 569,
   "ten_co_so": "Quầy thuốc Thu Nguyệt",
   "address": "Tổ 3, ấp Hoàng Giao, thị trấn Ngãi Giao, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6405556,
   "Latitude": 107.2477778
 },
 {
   "STT": 570,
   "ten_co_so": "Quầy thuốc Thiên Ân",
   "address": "Tổ 12, ấp Tân Lộc, xã Phước Hòa, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5124259,
   "Latitude": 107.0903698
 },
 {
   "STT": 572,
   "ten_co_so": "Quầy thuốc Duyên Ngọc",
   "address": "Tổ 13, ấp Song Vĩnh, xã Tân Phước, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.565034,
   "Latitude": 107.05809
 },
 {
   "STT": 573,
   "ten_co_so": "Quầy thuốc Thanh Nhàn",
   "address": "Ấp 4, xã Hòa Hội, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6279317,
   "Latitude": 107.4440254
 },
 {
   "STT": 574,
   "ten_co_so": "Nhà thuốc Diễm My",
   "address": "1063 đường 30/4, phường 11, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4100484,
   "Latitude": 107.1275379
 },
 {
   "STT": 575,
   "ten_co_so": "Nhà thuốc Sao Kim",
   "address": "941 Bình Giã, phường 10, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3929803,
   "Latitude": 107.1181147
 },
 {
   "STT": 576,
   "ten_co_so": "Quầy thuốc Phước Thuận",
   "address": "Tổ 1/362, khu phố Hải Bình, thị trấn Long Hải, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3842286,
   "Latitude": 107.2366329
 },
 {
   "STT": 577,
   "ten_co_so": "Quầy thuốc Thanh Duy",
   "address": "Tổ 17, thôn Trung Sơn, xã Suối Nghệ, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6043384,
   "Latitude": 107.1898113
 },
 {
   "STT": 578,
   "ten_co_so": "Quầy thuốc Nhật Hoàng",
   "address": "Số 43/18, ấp 4, xã Hòa Hiệp, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6787414,
   "Latitude": 107.5031811
 },
 {
   "STT": 579,
   "ten_co_so": "Quầy thuốc Thanh Thế",
   "address": "Chợ Sơn Bình, xã Sơn Bình, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6462999,
   "Latitude": 107.3297846
 },
 {
   "STT": 580,
   "ten_co_so": "Quầy thuốc Hoài Nhi",
   "address": "Ấ p1, xã Bàu Lâm, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.693165,
   "Latitude": 107.373486
 },
 {
   "STT": 581,
   "ten_co_so": "Nhà thuốc Thanh Hoa",
   "address": "72 Nam Kỳ Khởi Nghĩa, phường Thắng Tam, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3456835,
   "Latitude": 107.084095
 },
 {
   "STT": 582,
   "ten_co_so": "Quầy thuốc Số 112",
   "address": "264 Nguyễn Hữu Cảnh, phường Thắng Nhất, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3836181,
   "Latitude": 107.1088867
 },
 {
   "STT": 583,
   "ten_co_so": "Quầy thuốc Trưng Nhị",
   "address": "Tổ 31/4 Ô 4, khu phố Hải Hà 1, thị trấn Long Hải, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3851624,
   "Latitude": 107.2401989
 },
 {
   "STT": 584,
   "ten_co_so": "Quầy thuốc Thanh Tâm",
   "address": "Ấp Phước Hưng, xã Tam Phước, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.444291,
   "Latitude": 107.2311774
 },
 {
   "STT": 585,
   "ten_co_so": "Quầy thuốc Láng Dài",
   "address": "Tổ 20, ấp Cây Cám, xã Láng Dài, Đất Đỏ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5195949,
   "Latitude": 107.3551587
 },
 {
   "STT": 586,
   "ten_co_so": "Nhà thuốc Nhật Hoàng",
   "address": "785 Bình Giã, phường 10, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3841447,
   "Latitude": 107.1155754
 },
 {
   "STT": 587,
   "ten_co_so": "Nhà thuốc Tiến Đạt",
   "address": "15 Trần Anh Tông, phường Thắng Nhất, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3799476,
   "Latitude": 107.1031398
 },
 {
   "STT": 590,
   "ten_co_so": "Quầy thuốc 258",
   "address": "Ấp Phước Hiệp, xã Tân Hòa, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4033645,
   "Latitude": 107.1813173
 },
 {
   "STT": 591,
   "ten_co_so": "Nhà thuốc Bảo Châu",
   "address": "38A Nguyễn Văn Cừ, KP1, phường Long Toàn, Bà Rịa, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4890239,
   "Latitude": 107.1932888
 },
 {
   "STT": 592,
   "ten_co_so": "Quầy thuốc Dược Đức",
   "address": "Ô 2, trung tâm thương mại Châu Đức, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6479007,
   "Latitude": 107.238735
 },
 {
   "STT": 593,
   "ten_co_so": "Quầy thuốc Hồng Nhung",
   "address": "Tổ 1, ấp Gia Hòa Yên, xã Bình Giã, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6463909,
   "Latitude": 107.2707516
 },
 {
   "STT": 594,
   "ten_co_so": "Quầy thuốc Hùng Nhung",
   "address": "Ấp 4B, xã Tân Lâm, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.7377268,
   "Latitude": 107.4203669
 },
 {
   "STT": 595,
   "ten_co_so": "Quầy thuốc Minh Anh",
   "address": "Tổ 1, ấp Mỹ Thạnh, xã Mỹ Xuân, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6327258,
   "Latitude": 107.0548264
 },
 {
   "STT": 596,
   "ten_co_so": "Quầy thuốc Minh Thùy",
   "address": "12 Phan Bội Châu, khu phố Tân Phú, thị trấn Phú Mỹ, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6010346,
   "Latitude": 107.0539168
 },
 {
   "STT": 597,
   "ten_co_so": "Quầy thuốc số 145",
   "address": "614/B 16, CMT8, phường Phước Trung, Bà Rịa, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4875674,
   "Latitude": 107.1917859
 },
 {
   "STT": 598,
   "ten_co_so": "Quầy thuốc Số 258",
   "address": "Ấp Phước Hiệp, xã Tân Hoà, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4033645,
   "Latitude": 107.1813173
 },
 {
   "STT": 599,
   "ten_co_so": "Quầy thuốc Song Hy",
   "address": "Tổ 4, thôn Quảng Phú, thị trấn Phú Mỹ, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5905345,
   "Latitude": 107.0480378
 },
 {
   "STT": 600,
   "ten_co_so": "Quầy thuốc Thiên Phúc",
   "address": "Tổ 54, thôn Quảng Tây, xã Nghĩa Thành, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5803813,
   "Latitude": 107.1873057
 },
 {
   "STT": 601,
   "ten_co_so": "Nhà thuốc Tuờng Vy",
   "address": "1019 đường 30/4, phường 12, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4100484,
   "Latitude": 107.1275379
 },
 {
   "STT": 602,
   "ten_co_so": "Quầy thuốc Ái Như",
   "address": "Tổ 14, ấp Mỹ Thạnh, xã Mỹ Xuân, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6115125,
   "Latitude": 107.0707415
 },
 {
   "STT": 603,
   "ten_co_so": "Quầy thuốc Thiên Phước",
   "address": "6/1 (căn B) ấp Tân Hòa, xã Long Tân, Đất Đỏ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5505926,
   "Latitude": 107.2784615
 },
 {
   "STT": 604,
   "ten_co_so": "Quầy thuốc Hữu Nghị",
   "address": "Chợ Tân Lâm, xã Tân Lâm, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.7377268,
   "Latitude": 107.4203669
 },
 {
   "STT": 605,
   "ten_co_so": "Quầy thuốc Thụy Niên",
   "address": "Đường 7, thôn Gio An, xã Suối Nghệ, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5895466,
   "Latitude": 107.2292749
 },
 {
   "STT": 606,
   "ten_co_so": "Quầy thuốc Thắng Ninh",
   "address": "ấp Phước Tân 2, xã Tân Hưng, Bà Rịa, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.531944,
   "Latitude": 107.175
 },
 {
   "STT": 607,
   "ten_co_so": "Nhà thuốc Bảo Minh Châu",
   "address": "1501 đường 30/4, phường 12, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4100484,
   "Latitude": 107.1275379
 },
 {
   "STT": 608,
   "ten_co_so": "Quầy thuốc Vương Khang",
   "address": "số 26, KP .Hải Bình, TT. Long Hải, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4041001,
   "Latitude": 107.2301996
 },
 {
   "STT": 609,
   "ten_co_so": "Quầy thuốc Bảo Trâm",
   "address": "Ấp Mỹ Thạnh, xã Mỹ Xuân, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6168779,
   "Latitude": 107.0668172
 },
 {
   "STT": 610,
   "ten_co_so": "Quầy thuốc Số 185",
   "address": "Ấp Phước Hiệp, xã Tân Hòa, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4033645,
   "Latitude": 107.1813173
 },
 {
   "STT": 611,
   "ten_co_so": "Quầy thuốc An Nuy",
   "address": "Thôn Viêt Cường, xã Cù Bị, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.719309,
   "Latitude": 107.154427
 },
 {
   "STT": 612,
   "ten_co_so": "Quầy thuốc Đức Mạnh",
   "address": "17T3 khu phố Long Sơn, thị trấn Long Điền, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4824202,
   "Latitude": 107.2207892
 },
 {
   "STT": 613,
   "ten_co_so": "Quầy thuốc Khánh Hà",
   "address": "Tổ 1, ấp Phước Tân 4, xã Tân Hưng, Bà Rịa, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.531944,
   "Latitude": 107.175
 },
 {
   "STT": 614,
   "ten_co_so": "Quầy thuốc Kim Oanh",
   "address": "Tổ 30, thôn Sơn Lập, xã Sơn Bình, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.649907,
   "Latitude": 107.3434916
 },
 {
   "STT": 615,
   "ten_co_so": "Quầy thuốc Ngọc Châu",
   "address": "131/12 khu phố Xóm Rẫy, thị trấn Phước Bữu, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5353285,
   "Latitude": 107.4055814
 },
 {
   "STT": 616,
   "ten_co_so": "Quầy thuốc Ngọc Đức",
   "address": "A 8, Trung tâm thương mại Kim Long, xã Kim Long, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.7043005,
   "Latitude": 107.2445889
 },
 {
   "STT": 617,
   "ten_co_so": "Quầy thuốc Nguyễn Bình",
   "address": "Ấp 3, xã Tóc Tiên, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.604068,
   "Latitude": 107.1127778
 },
 {
   "STT": 618,
   "ten_co_so": "TTTYT Phòng khám khu vực Long Điền",
   "address": "Số 108, Võ Thị Sáu, phòng khám khu vực Long Điền, thị trấn Long Điền, thị trấn Long Điền, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4831424,
   "Latitude": 107.2080612
 },
 {
   "STT": 619,
   "ten_co_so": "Quầy thuốc Số 178",
   "address": "Tổ 1, khu phố Phước Hưng, phường Mỹ Xuân, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.501265,
   "Latitude": 107.1793596
 },
 {
   "STT": 620,
   "ten_co_so": "Quầy thuốc Số 242",
   "address": "Kios số 2, chợ Long Hải, thị trấn Long Hải, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4085143,
   "Latitude": 107.2213235
 },
 {
   "STT": 621,
   "ten_co_so": "Quầy thuốc Thiên Phước",
   "address": "D37, tổ 01, ấp Phước Thái, xã Phước Tỉnh, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.412204,
   "Latitude": 107.1915213
 },
 {
   "STT": 622,
   "ten_co_so": "Nhà thuốc Trang Phát",
   "address": "743 đường 30/4, phường Rạch Dừa, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4100484,
   "Latitude": 107.1275379
 },
 {
   "STT": 623,
   "ten_co_so": "Quầy thuốc Trung Hiếu",
   "address": "Tổ 3, ấp Trung Hùng, xã Bông Trang, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5355319,
   "Latitude": 107.4499404
 },
 {
   "STT": 624,
   "ten_co_so": "Quầy thuốc Nguyễn Phấn",
   "address": "Tổ 1/45, Ô 1, khu phố Hải Bình, thị trấn Long Hải, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.395902,
   "Latitude": 107.2370874
 },
 {
   "STT": 625,
   "ten_co_so": "Nhà thuốc Vương Lộc",
   "address": "150 Cô Giang, Phường 4, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3564974,
   "Latitude": 107.0749248
 },
 {
   "STT": 626,
   "ten_co_so": "Nhà thuốc Quỳnh Nhi",
   "address": "169A Lưu Chí Hiếu, phường 10, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3867844,
   "Latitude": 107.1123299
 },
 {
   "STT": 627,
   "ten_co_so": "Nhà thuốc Hùng Phong",
   "address": "Ấp 2 Đông, xã Bàu Lâm, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6970348,
   "Latitude": 107.3730563
 },
 {
   "STT": 628,
   "ten_co_so": "Nhà thuốc Ngọc Châu 2",
   "address": "196 Cách Mạng Tháng Tám, P. Phước Hiệp, Bà Rịa, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4966679,
   "Latitude": 107.1689714
 },
 {
   "STT": 629,
   "ten_co_so": "Quầy thuốc Số 175",
   "address": "2B, khu phố Phước Thới, thị trấn Đất Đỏ, Đất Đỏ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4898731,
   "Latitude": 107.2711385
 },
 {
   "STT": 630,
   "ten_co_so": "Nhà thuốc BV Lê Lợi",
   "address": "22 Lê Lợi, P.1, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3533269,
   "Latitude": 107.0745242
 },
 {
   "STT": 631,
   "ten_co_so": "Quầy thuốc Hạnh Phúc",
   "address": "2Ô3/4, tổ 9, khu phố Hải Phúc, thị trấn Phước Hải, Đất Đỏ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4054168,
   "Latitude": 107.2607289
 },
 {
   "STT": 632,
   "ten_co_so": "Quầy thuốc Khả Mi",
   "address": "Tổ 6, ấp Mỹ Thạnh, xã Mỹ Xuân, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6293731,
   "Latitude": 107.0492295
 },
 {
   "STT": 633,
   "ten_co_so": "Quầy thuốc Lộc Ngân",
   "address": "Ấp Tây, xã Long Phước, Bà Rịa, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5275353,
   "Latitude": 107.2282686
 },
 {
   "STT": 634,
   "ten_co_so": "Quầy thuốc Số 180",
   "address": "Tổ 3, khu 1, thôn Phước Hiệp, xã Tân Hòa, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4983607,
   "Latitude": 107.1706079
 },
 {
   "STT": 635,
   "ten_co_so": "Quầy thuốc Vĩnh Phúc",
   "address": "Tổ 7, ấp 3, xã Hòa Hội, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.615904,
   "Latitude": 107.440219
 },
 {
   "STT": 636,
   "ten_co_so": "Quầy thuốc Linh Phụng",
   "address": "1 Ô 2/21, khu phố Lộc An, thị trấn Phước Hải, Đất Đỏ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.465744,
   "Latitude": 107.3434916
 },
 {
   "STT": 637,
   "ten_co_so": "Quầy thuốc Số 101",
   "address": "1228 đường 30/4, phường 12, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4100558,
   "Latitude": 107.1276599
 },
 {
   "STT": 638,
   "ten_co_so": "Quầy thuốc Số 187",
   "address": "2467 Độc Lập, phường Phú Mỹ, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5905345,
   "Latitude": 107.0480378
 },
 {
   "STT": 639,
   "ten_co_so": "Quầy thuốc Thiên Phước",
   "address": "Tổ 4, ấp Tây, xã Long Phước, Bà Rịa, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.52333,
   "Latitude": 107.2220913
 },
 {
   "STT": 640,
   "ten_co_so": "Nhà thuốc Bảo Linh",
   "address": "26 đường Nguyễn Hữu Cảnh, phường Thắng Nhất , Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.384098,
   "Latitude": 107.1085021
 },
 {
   "STT": 641,
   "ten_co_so": "Quầy thuốc Hoàng Hiếu 2",
   "address": "Tổ 9, khu TĐC Phước lập, xã Mỹ Xuân, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6456132,
   "Latitude": 107.0495635
 },
 {
   "STT": 642,
   "ten_co_so": "Nhà thuốc Hùng Lâm",
   "address": "119 Đô Lương, P.  11, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4099856,
   "Latitude": 107.1396248
 },
 {
   "STT": 643,
   "ten_co_so": "Quầy thuốc Mai Chi",
   "address": "Ấp Phước Tân 4, xã Tân Hưng, Bà Rịa, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.531944,
   "Latitude": 107.175
 },
 {
   "STT": 644,
   "ten_co_so": "Quầy thuốc Mai Phương",
   "address": "Chợ Sơn Bình, xã Sơn Bình, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6462999,
   "Latitude": 107.3297846
 },
 {
   "STT": 645,
   "ten_co_so": "Quầy thuốc Minh Huy",
   "address": "Tổ 17, ấp Mỹ Thạnh, xã Mỹ Xuân, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6327258,
   "Latitude": 107.0548264
 },
 {
   "STT": 646,
   "ten_co_so": "Quầy thuốc Nam Việt",
   "address": "Tổ 2, ấp Phước Hưng, xã Mỹ Xuân, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6166667,
   "Latitude": 107.0666667
 },
 {
   "STT": 647,
   "ten_co_so": "Quầy thuốc Phương Huỳnh",
   "address": "Ô 2, ấp Bắc 2,  xã Hòa Long, Bà Rịa, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5466302,
   "Latitude": 107.2056869
 },
 {
   "STT": 648,
   "ten_co_so": "Quầy thuốc Số 171",
   "address": "1 Ô 8/9, ấp An Hòa, xã Lộc An, Đất Đỏ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4547513,
   "Latitude": 107.3325279
 },
 {
   "STT": 650,
   "ten_co_so": "Nhà thuốc Thảo Hồng",
   "address": "92B đường Đô Lương, phường 11, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4016764,
   "Latitude": 107.1442814
 },
 {
   "STT": 651,
   "ten_co_so": "TTTYT Thị trấn Phú Mỹ",
   "address": "Khu phố Ngọc Hà, thị trấn Phú Mỹ, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6063271,
   "Latitude": 107.0542758
 },
 {
   "STT": 652,
   "ten_co_so": "Nhà thuốc Y Đức",
   "address": "514 Trần Phú, phường 5, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3792553,
   "Latitude": 107.0668761
 },
 {
   "STT": 653,
   "ten_co_so": "Quầy thuốc Minh Châu",
   "address": "Đường Mỹ Xuân - Ngãi Giao, ấp trảng Lớn, xã Hắc Dịch, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6437252,
   "Latitude": 107.105096
 },
 {
   "STT": 654,
   "ten_co_so": "Quầy thuốc Hoa Hạ",
   "address": "Tổ 71, ấp Liên Hiệp 1, xã Xà Bang , Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.7240106,
   "Latitude": 107.2172995
 },
 {
   "STT": 655,
   "ten_co_so": "Quầy thuốc Kim Phượng",
   "address": "Ấp 3, xã Bưng Riềng, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.538601,
   "Latitude": 107.4913489
 },
 {
   "STT": 656,
   "ten_co_so": "Quầy thuốc Kim Phượng",
   "address": "Thôn 1, xã Long Sơn, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4488577,
   "Latitude": 107.1216613
 },
 {
   "STT": 657,
   "ten_co_so": "Quầy thuốc Kim Phượng",
   "address": "Tổ 8, thôn Tân Bình, xã Sơn Bình, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6446655,
   "Latitude": 107.3622179
 },
 {
   "STT": 658,
   "ten_co_so": "Nhà thuốc Minh Đức",
   "address": "179 Bạch Đằng, phường 5, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3805217,
   "Latitude": 107.0669389
 },
 {
   "STT": 659,
   "ten_co_so": "Quầy thuốc Phượng",
   "address": "Tổ 10, khu phố Trảng Cát, phường Hắc Dịch, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6466114,
   "Latitude": 107.113007
 },
 {
   "STT": 660,
   "ten_co_so": "Quầy thuốc Quốc Huy",
   "address": "Tổ 02, 7/2B, ấp An Phước, xã An Ngãi, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4287762,
   "Latitude": 107.2373088
 },
 {
   "STT": 661,
   "ten_co_so": "Quầy thuốc Số 146",
   "address": "Số 15/4 Nguyễn Văn Cừ, phường Long Tâm, Bà Rịa, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4957925,
   "Latitude": 107.1967846
 },
 {
   "STT": 662,
   "ten_co_so": "Quầy thuốc Số 244",
   "address": "Chợ Hòa Hội, xã Hòa Hội, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6129943,
   "Latitude": 107.4391275
 },
 {
   "STT": 663,
   "ten_co_so": "Quầy thuốc Đức Vinh",
   "address": "Tổ 7, khu phố Phước Lập, phường Mỹ Xuân, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6396214,
   "Latitude": 107.0521974
 },
 {
   "STT": 664,
   "ten_co_so": "Quầy thuốc Nhân Đức",
   "address": "Tổ 23, thôn Sông Cầu, xã Nghĩa Thành, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5673977,
   "Latitude": 107.2113457
 },
 {
   "STT": 665,
   "ten_co_so": "Nhà thuốc Thùy Trang",
   "address": "86 Nơ Trang Long, phường Rạch Dừa, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3936161,
   "Latitude": 107.1120053
 },
 {
   "STT": 666,
   "ten_co_so": "Quầy thuốc BVĐK Tân Thành",
   "address": "Khuôn viên BVĐK Tân Thành, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6107096,
   "Latitude": 107.0562664
 },
 {
   "STT": 667,
   "ten_co_so": "Quầy thuốc Như Nguyệt",
   "address": "Chợ Quảng Thành, xã Quảng Thành, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6978999,
   "Latitude": 107.2769287
 },
 {
   "STT": 668,
   "ten_co_so": "Quầy thuốc Số 240",
   "address": "Số 114, ấp Phước Tấn, xã Tân Hòa, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5330334,
   "Latitude": 107.1071
 },
 {
   "STT": 669,
   "ten_co_so": "Nhà thuốc Bệnh viện Mắt",
   "address": "21 Phạm Ngọc Thạch, phường Phước Hưng, Bà Rịa, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5081969,
   "Latitude": 107.1759813
 },
 {
   "STT": 670,
   "ten_co_so": "Quầy thuốc Ngọc Quý",
   "address": "2 Ô1/3, khu phố Hải Lạc, thị trấn Phước Hải, Đất Đỏ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.414526,
   "Latitude": 107.278711
 },
 {
   "STT": 671,
   "ten_co_so": "Quầy thuốc Tường Vy",
   "address": "Tổ 10, thôn Xuân Tân, xã Xuân Sơn, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6522573,
   "Latitude": 107.3265123
 },
 {
   "STT": 672,
   "ten_co_so": "Nhà thuốc Công ty TNHH bệnh viện Asia Phú Mỹ",
   "address": "Tổ 1, khu phố Mỹ Tân, phường Mỹ Xuân, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.610617,
   "Latitude": 107.0561272
 },
 {
   "STT": 673,
   "ten_co_so": "Nhà thuốc Hoàng Tuyết",
   "address": "72 Lưu Chí Hiếu, phường Thắng Nhất, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3918221,
   "Latitude": 107.1075314
 },
 {
   "STT": 674,
   "ten_co_so": "Quầy thuốc Hồng Quyên",
   "address": "TC 052, khu Phố Hải Bình, thị trấn Long Hải, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3819595,
   "Latitude": 107.2497115
 },
 {
   "STT": 675,
   "ten_co_so": "Quầy thuốc Khải Quyên",
   "address": "Tổ 8, ấp Ông Trịnh, xã Tân Phước, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5573341,
   "Latitude": 107.0598491
 },
 {
   "STT": 676,
   "ten_co_so": "Quầy thuốc Kim Chi",
   "address": "Tổ 3, thôn Tân Hà, xã Châu Pha, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5737817,
   "Latitude": 107.1543601
 },
 {
   "STT": 677,
   "ten_co_so": "Nhà thuốc Lê Uyên",
   "address": "43 Nguyễn Bỉnh Khiêm, phường 3, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3421746,
   "Latitude": 107.0821117
 },
 {
   "STT": 678,
   "ten_co_so": "Nhà thuốc Rạng Đông",
   "address": "62 Lê Quang Định, phường 9, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3784148,
   "Latitude": 107.1009487
 },
 {
   "STT": 679,
   "ten_co_so": "Quầy thuốc Anh Tuấn",
   "address": "Tổ 35/10 Ô4, Khu phố Hải Hà 1, thị Trấn Long Hải, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.395902,
   "Latitude": 107.2370874
 },
 {
   "STT": 680,
   "ten_co_so": "Nhà thuốc Gia Tường",
   "address": "17 đường Đồ Chiểu, phường 1, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3497864,
   "Latitude": 107.078761
 },
 {
   "STT": 681,
   "ten_co_so": "Quầy thuốc Quỳnh",
   "address": "Tổ 33, thôn Thạch Long, xã Kim Long, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6948133,
   "Latitude": 107.2770285
 },
 {
   "STT": 682,
   "ten_co_so": "Quầy thuốc Kim Ngọc",
   "address": "Tổ 7, ấp Phước Lập, xã Mỹ Xuân, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6456132,
   "Latitude": 107.0495635
 },
 {
   "STT": 683,
   "ten_co_so": "Quầy thuốc Đức Châu",
   "address": "Tổ 1, ấp Tân Ro. Xã Châu Pha, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5997876,
   "Latitude": 107.1499291
 },
 {
   "STT": 684,
   "ten_co_so": "Quầy thuốc Số 216",
   "address": "Số 311/12, tổ 12, ấp Phú Thiện, xã Hòa Hiệp, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6787414,
   "Latitude": 107.5031811
 },
 {
   "STT": 685,
   "ten_co_so": "Quầy thuốc Nguyên Thảo",
   "address": "Kios A1/01, chợ Phước Tỉnh, xã Phước Tỉnh, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4057146,
   "Latitude": 107.1871018
 },
 {
   "STT": 686,
   "ten_co_so": "Quầy thuốc Quốc Trọng",
   "address": " Ô2, ấp Bắc, xã Hòa Long, Bà Rịa, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5199356,
   "Latitude": 107.1973274
 },
 {
   "STT": 687,
   "ten_co_so": "Quầy thuốc Số 255",
   "address": "Ấp Tân Phú, xã Châu Pha, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5737587,
   "Latitude": 107.153931
 },
 {
   "STT": 688,
   "ten_co_so": "Quầy thuốc Kim Sinh",
   "address": "ấp Phú Vinh, xã Hòa Hiệp, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.685912,
   "Latitude": 107.504997
 },
 {
   "STT": 689,
   "ten_co_so": "Quầy thuốc Hoài Phúc",
   "address": "Chợ Mỹ Thạnh, Ấp Mỹ Thạnh, xã Mỹ Xuân, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6085884,
   "Latitude": 107.0574538
 },
 {
   "STT": 690,
   "ten_co_so": "Quầy thuốc Hồng Son",
   "address": "thôn Tân Long, xã Kim Long, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.705,
   "Latitude": 107.2311111
 },
 {
   "STT": 691,
   "ten_co_so": "Nhà thuốc Hồng Sơn",
   "address": "26 Lê Lợi, phường 4, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3538079,
   "Latitude": 107.0746156
 },
 {
   "STT": 692,
   "ten_co_so": "Nhà thuốc Huy Phương",
   "address": "94 Bình Gĩa, phường 8, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3542454,
   "Latitude": 107.0908818
 },
 {
   "STT": 693,
   "ten_co_so": "Nhà thuốc Khang Huê",
   "address": "491 đường Trần Phú, phường Thắng Nhì, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3760836,
   "Latitude": 107.0706112
 },
 {
   "STT": 694,
   "ten_co_so": "Quầy thuốc Số 134",
   "address": "107 Thùy Vân, phường 2, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3338077,
   "Latitude": 107.089743
 },
 {
   "STT": 695,
   "ten_co_so": "Nhà thuốc Thu Thảo",
   "address": "756C Bình Giã, phường 10, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.38795,
   "Latitude": 107.1186087
 },
 {
   "STT": 696,
   "ten_co_so": "Quầy thuốc Số 13",
   "address": "28 Nguyễn Đình Chiểu, phường Phước Hiệp, Bà Rịa, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4939759,
   "Latitude": 107.1683809
 },
 {
   "STT": 697,
   "ten_co_so": "Quầy thuốc Thảo Nguyên",
   "address": "Tổ 1, ấp Vĩnh An, xã Bình Giã, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6437421,
   "Latitude": 107.2607289
 },
 {
   "STT": 698,
   "ten_co_so": "Quầy thuốc Thu Sương",
   "address": "Tổ 10, ấp Đức Trung, xã Bình Ba, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6182884,
   "Latitude": 107.2311774
 },
 {
   "STT": 699,
   "ten_co_so": "Quầy thuốc Văn Sỹ",
   "address": "225 ấp Vĩnh An, xã Bình Giã, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6437421,
   "Latitude": 107.2607289
 },
 {
   "STT": 700,
   "ten_co_so": "Quầy thuốc Lộc Ngân 5",
   "address": "1 Ô 2/5, khu phố Hải Phúc, thị trấn Phước Hải, Đất Đỏ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4902864,
   "Latitude": 107.2711131
 },
 {
   "STT": 701,
   "ten_co_so": "Quầy thuốc Số 222",
   "address": "Chợ xã Bình Châu, xã Bình Châu, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5553344,
   "Latitude": 107.5483173
 },
 {
   "STT": 702,
   "ten_co_so": "Nhà thuốc Bảo Ngọc ",
   "address": "258 lê Lợi, phường 4, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3610984,
   "Latitude": 107.0755204
 },
 {
   "STT": 703,
   "ten_co_so": "Nhà thuốc Đông Trang",
   "address": "40 đường Lê Văn Lộc, phường 9, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3721756,
   "Latitude": 107.0827861
 },
 {
   "STT": 704,
   "ten_co_so": "Quầy thuốc Hoài Tâm",
   "address": "Tổ 2, khu phố Phước Lập, phường Mỹ Xuân, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6396214,
   "Latitude": 107.0521974
 },
 {
   "STT": 705,
   "ten_co_so": "Quầy thuốc Hoàng Xuân",
   "address": "Số 385 Hùng Vương, trị trấn Ngãi Giao, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6525261,
   "Latitude": 107.2467327
 },
 {
   "STT": 706,
   "ten_co_so": "Quầy thuốc Minh Tâm",
   "address": "80B3, Võ Thị Sáu, thị trấn Long Điền, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4844231,
   "Latitude": 107.201169
 },
 {
   "STT": 707,
   "ten_co_so": "Nhà thuốc Quốc Anh",
   "address": "52 Trương Công Định, phường 3, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3450354,
   "Latitude": 107.080073
 },
 {
   "STT": 708,
   "ten_co_so": "Quầy thuốc Số 188",
   "address": "4/188, ấp Hải Sơn, xã Phước Hòa, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5367551,
   "Latitude": 107.0765948
 },
 {
   "STT": 709,
   "ten_co_so": "Quầy thuốc Số 218",
   "address": "ấp Thanh Bình 3, xã Bình Châu, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5563889,
   "Latitude": 107.5422222
 },
 {
   "STT": 710,
   "ten_co_so": "Quầy thuốc Số 243",
   "address": "Khu phố Phước Lộc, thị trấn Phước Bửu, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5354501,
   "Latitude": 107.403416
 },
 {
   "STT": 711,
   "ten_co_so": "Quầy thuốc Tâm Đức",
   "address": "Tổ 5, ấp 3, xã Sông Xoài, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6605776,
   "Latitude": 107.1543601
 },
 {
   "STT": 712,
   "ten_co_so": "Quầy thuốc Thanh Tâm",
   "address": "Ấp Phước Hưng, xã Mỹ Xuân, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6159918,
   "Latitude": 107.057912
 },
 {
   "STT": 713,
   "ten_co_so": "TTTYT Xã Phước Tân",
   "address": "Ấp Thạnh Sơn 2A, xã Phước Tân, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.576922,
   "Latitude": 107.373161
 },
 {
   "STT": 714,
   "ten_co_so": "Quầy thuốc Xuân Tâm",
   "address": "Chợ Bưng Riềng, xã Bưng Riềng, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5522225,
   "Latitude": 107.4839021
 },
 {
   "STT": 715,
   "ten_co_so": "Quầy thuốc Phương An",
   "address": "50 Căn B, khu phố Phước Trung, thị trấn Đất Đỏ, Đất Đỏ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4900131,
   "Latitude": 107.2712946
 },
 {
   "STT": 716,
   "ten_co_so": "Quầy thuốc 205",
   "address": "71 Lê Hồng Phong, khu phố 6, thị trấn Ngãi Giao, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6475599,
   "Latitude": 107.2447157
 },
 {
   "STT": 717,
   "ten_co_so": "Quầy thuốc Phòng khám đa khoa Vạn Tâm -Sài Gòn",
   "address": "Ấp Tân Phú, xã Châu Pha, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5737587,
   "Latitude": 107.153931
 },
 {
   "STT": 718,
   "ten_co_so": "TTTYT Xã Xuyên Mộc",
   "address": "Ấp Nhân Nghĩa, xã Xuyên Mộc, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5564986,
   "Latitude": 107.4262813
 },
 {
   "STT": 719,
   "ten_co_so": "Nhà thuốc Thảo Trang",
   "address": "178 Nguyễn Văn Trỗi, phường 4, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3588804,
   "Latitude": 107.0790351
 },
 {
   "STT": 720,
   "ten_co_so": "Nhà thuốc Đức Lộc",
   "address": "33 Phước Thắng, phường 12, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4135174,
   "Latitude": 107.16122849999999
 },
 {
   "STT": 721,
   "ten_co_so": "Quầy thuốc Thanh Thà",
   "address": "Tổ 20, thôn Sông Cầu, xã Nghĩa Thành, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5673977,
   "Latitude": 107.2113457
 },
 {
   "STT": 722,
   "ten_co_so": "Quầy thuốc Số 206",
   "address": "Chợ Xà Bang, xã Xà Bang, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.7263261,
   "Latitude": 107.238664
 },
 {
   "STT": 723,
   "ten_co_so": "Nhà thuốc Đăng Khôi",
   "address": "114 Nguyễn Thiện Thuật, phường Thắng Nhất, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3886585,
   "Latitude": 107.1015666
 },
 {
   "STT": 724,
   "ten_co_so": "Quầy thuốc Đức Lộc",
   "address": "Tổ 6, ấp Trảng Cát, xã Hắc Dịch, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6436941,
   "Latitude": 107.1030964
 },
 {
   "STT": 725,
   "ten_co_so": "Quầy thuốc Khánh Hà",
   "address": "Tổ 71, thôn Chòi Đồng, xã Cù Bị, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.7294922,
   "Latitude": 107.1839024
 },
 {
   "STT": 726,
   "ten_co_so": "Nhà thuốc Thiên Bảo",
   "address": "A7-7/3, Trung tâm đô thị Chí Linh, phường Nguyễn An Ninh, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3787996,
   "Latitude": 107.1118176
 },
 {
   "STT": 727,
   "ten_co_so": "Quầy thuốc Chiến Thắng",
   "address": "Ấp 4, xã Hòa Bình, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6094444,
   "Latitude": 107.3966667
 },
 {
   "STT": 728,
   "ten_co_so": "Quầy thuốc Minh Thắng",
   "address": "tổ 70, thôn Chòi Đồng, xã Cù Bị, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.7294922,
   "Latitude": 107.1839024
 },
 {
   "STT": 729,
   "ten_co_so": "Quầy thuốc số 143",
   "address": "91 Cách Mạng Tháng 8, phường Long Hương, Bà Rịa, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4985544,
   "Latitude": 107.1634864
 },
 {
   "STT": 730,
   "ten_co_so": "Nhà thuốc Thái Hằng",
   "address": "559 Bình Giã, phường Thắng Nhất, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3772801,
   "Latitude": 107.1043892
 },
 {
   "STT": 731,
   "ten_co_so": "Quầy thuốc Anh Khoa",
   "address": "Tổ 9, thôn Xuân Tân, xã Xuân Sơn, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6522573,
   "Latitude": 107.3265123
 },
 {
   "STT": 732,
   "ten_co_so": "Nhà thuốc Bảo An",
   "address": "576A, đường 30/4, phường Rạch Dừa, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4100558,
   "Latitude": 107.1276599
 },
 {
   "STT": 733,
   "ten_co_so": "Quầy thuốc Bảo Ngọc",
   "address": "Tổ 11, ấp Ông Trịnh, xã Tân Phước, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5567116,
   "Latitude": 107.065233
 },
 {
   "STT": 734,
   "ten_co_so": "Quầy thuốc Gia Hảo",
   "address": "Tổ 4, ấp Phước Lợi, xã Phước Hội, Đất Đỏ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4506553,
   "Latitude": 107.2824195
 },
 {
   "STT": 735,
   "ten_co_so": "Quầy thuốc Kiều Thanh",
   "address": "thôn Quảng Tây, xã Nghĩa Thành, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5803813,
   "Latitude": 107.1873057
 },
 {
   "STT": 736,
   "ten_co_so": "Quầy thuốc Ngọc Hân",
   "address": "432/12 ấp Tân An, xã Phước Tân, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5669449,
   "Latitude": 107.3730563
 },
 {
   "STT": 737,
   "ten_co_so": "Quầy thuốc Phương Thanh",
   "address": "Khu 2, thôn Ngọc Hà, thị trấn Phú Mỹ, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6063271,
   "Latitude": 107.0542758
 },
 {
   "STT": 738,
   "ten_co_so": "Quầy thuốc Số 223",
   "address": "Chợ xã Bình Châu, xã Bình Châu, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5553344,
   "Latitude": 107.5483173
 },
 {
   "STT": 739,
   "ten_co_so": "Nhà thuốc Thanh Chi",
   "address": "79 Lê Quý Đôn, phường Phước Trung, Bà Rịa, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4921519,
   "Latitude": 107.171794
 },
 {
   "STT": 740,
   "ten_co_so": "Quầy thuốc Thanh Hà",
   "address": "Tổ 1, khu phố Bến Đình, Phường Mỹ Xuân, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3736826,
   "Latitude": 107.0808893
 },
 {
   "STT": 741,
   "ten_co_so": "Quầy thuốc Thanh Thanh",
   "address": "37/5 Hoàng Diệu, khu phố Tân Ngọc, thị trân Phú Mỹ, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.606072,
   "Latitude": 107.0542973
 },
 {
   "STT": 742,
   "ten_co_so": "Nhà thuốc Vĩnh Thọ",
   "address": "552 Cách Mạng Tháng Tám, phường Phước Trung, Bà Rịa, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4921634,
   "Latitude": 107.1834509
 },
 {
   "STT": 743,
   "ten_co_so": "Quầy thuốc Quang Đạt",
   "address": "Đường 25, Tổ 45, thôn Vinh Sơn, xã Nghĩa Thành, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5828685,
   "Latitude": 107.2061775
 },
 {
   "STT": 744,
   "ten_co_so": "Quầy thuốc Thành Phát",
   "address": "Chợ Ngọc Hà, thị trấn Phú Mỹ, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6063271,
   "Latitude": 107.0542758
 },
 {
   "STT": 745,
   "ten_co_so": "Nhà thuốc Thảo Trang 163",
   "address": "163 Xô Viết Nghệ Tỉnh, phường Thắng Tam, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3475075,
   "Latitude": 107.0863741
 },
 {
   "STT": 746,
   "ten_co_so": "Quầy thuốc Gia Nghi",
   "address": "Ấp Nhân Nghĩa, xã Xuyên Mộc, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5564986,
   "Latitude": 107.4262813
 },
 {
   "STT": 747,
   "ten_co_so": "Nhà thuốc Hữu Cảnh",
   "address": "74 Nguyễn Hữu Cảnh, phường Thắng Nhất, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3906057,
   "Latitude": 107.10278
 },
 {
   "STT": 748,
   "ten_co_so": "Quầy thuốc Khương Khương",
   "address": "142 khu phố Phước Thạnh, phường Mỹ Xuân, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6288504,
   "Latitude": 107.0421324
 },
 {
   "STT": 749,
   "ten_co_so": "Quầy thuốc Lê Ái",
   "address": "4 Ô 1/5, khu phố Lộc An, thị trấn Phước Hải, Đất Đỏ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4054168,
   "Latitude": 107.2607289
 },
 {
   "STT": 750,
   "ten_co_so": "Quầy thuốc Minh Anh",
   "address": "Tổ 8, ấp 3, xã Tân Hưng, Bà Rịa, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5319776,
   "Latitude": 107.1750393
 },
 {
   "STT": 751,
   "ten_co_so": "Nhà thuốc Minh Khôi",
   "address": "56 Lê Quang Định, phường 9, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3785822,
   "Latitude": 107.1009876
 },
 {
   "STT": 752,
   "ten_co_so": "Quầy thuốc Nhân Tâm",
   "address": "118 Phan Bội Châu, khu phố Tân Phú, thị trấn Phú Mỹ, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6009163,
   "Latitude": 107.050611
 },
 {
   "STT": 753,
   "ten_co_so": "Quầy thuốc Như Ý",
   "address": "Thôn 5, xã Bình Trung, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6425,
   "Latitude": 107.284444
 },
 {
   "STT": 754,
   "ten_co_so": "Quầy thuốc Phương Thảo",
   "address": "Tổ 4, khu phố Ông Trịnh, phường Tân Phước, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5567116,
   "Latitude": 107.065233
 },
 {
   "STT": 755,
   "ten_co_so": "Quầy thuốc Phương Thảo",
   "address": "Tổ 71, thôn Chòi Đồng, xã Cù Bị, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.7294922,
   "Latitude": 107.1839024
 },
 {
   "STT": 756,
   "ten_co_so": "Quầy thuốc Thảo Dương",
   "address": "Ấp 2, xã Tóc Tiên, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5903171,
   "Latitude": 107.1074504
 },
 {
   "STT": 757,
   "ten_co_so": "Quầy thuốc Thảo Ngân",
   "address": "Khu phố Tân Hạnh, thị trấn Phú Mỹ, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5872811,
   "Latitude": 107.0622145
 },
 {
   "STT": 758,
   "ten_co_so": "Quầy thuốc Thảo Uyên",
   "address": "Tổ 50, ấp Hiệp Thành, xã Quảng Thành, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6974711,
   "Latitude": 107.2773613
 },
 {
   "STT": 759,
   "ten_co_so": "Quầy thuốc Trần Thị Thanh Thảo",
   "address": "Khu chợ xã Hòa Hiệp, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.690055,
   "Latitude": 107.5025833
 },
 {
   "STT": 760,
   "ten_co_so": "TTTYT TTYT Xuyên Mộc",
   "address": "QL 55, TTYT Xuyên Mộc, TT. Phước Bửu, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5390052,
   "Latitude": 107.4129995
 },
 {
   "STT": 761,
   "ten_co_so": "Nhà thuốc Việt Anh",
   "address": "58 Xô Viết Ngệ Tĩnh, phường Thắng Tam, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3432774,
   "Latitude": 107.0849954
 },
 {
   "STT": 762,
   "ten_co_so": "Nhà thuốc Việt Anh",
   "address": "58 Xô Viết Nghệ Tĩnh, phường Thắng Tam, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3432774,
   "Latitude": 107.0849954
 },
 {
   "STT": 763,
   "ten_co_so": "TTTYT Xã Cù Bị",
   "address": "Thôn Đồng Tiến, xã Cù Bị, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.731347,
   "Latitude": 107.183258
 },
 {
   "STT": 764,
   "ten_co_so": "Quầy thuốc Đăng Quang",
   "address": "Ấp 3, xã Sông Xoài, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.666111,
   "Latitude": 107.15
 },
 {
   "STT": 765,
   "ten_co_so": "Quầy thuốc Số 201",
   "address": "Thôn 6, xã Bình Trung, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6379169,
   "Latitude": 107.2861962
 },
 {
   "STT": 766,
   "ten_co_so": "Quầy thuốc Tường Vy",
   "address": "ấp Nhân Nghĩa, xã Xuyên Mộc, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5564986,
   "Latitude": 107.4262813
 },
 {
   "STT": 767,
   "ten_co_so": "Quầy thuốc Gia Mỹ",
   "address": "Tổ 12, khu phố Hải Bình, thị trấn Long Hải, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3842286,
   "Latitude": 107.2366329
 },
 {
   "STT": 768,
   "ten_co_so": "Nhà thuốc Long Hằng",
   "address": "37 Phước Thắng, phường 12, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4134939,
   "Latitude": 107.1611866
 },
 {
   "STT": 769,
   "ten_co_so": "TTTYT Xã Láng Lớn",
   "address": "Thôn Sông Xoài 3, xã Láng Lớn, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6591667,
   "Latitude": 107.1777778
 },
 {
   "STT": 770,
   "ten_co_so": "Nhà thuốc Hoàng Vũ",
   "address": "1007/33 đường 30/4, phường 11, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4100484,
   "Latitude": 107.1275379
 },
 {
   "STT": 771,
   "ten_co_so": "Quầy thuốc Ngọc Thịnh",
   "address": "Số 257/8, ấp 2, xã Hòa Bình, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6089467,
   "Latitude": 107.3967105
 },
 {
   "STT": 772,
   "ten_co_so": "Quầy thuốc Số 234",
   "address": "Ấp Nhân Tâm, xã Xuyên Mộc, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5723613,
   "Latitude": 107.4228481
 },
 {
   "STT": 773,
   "ten_co_so": "Quầy thuốc Thiện Ân",
   "address": "Ấp Bình Hòa, xã Bình Châu, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.559203,
   "Latitude": 107.545337
 },
 {
   "STT": 774,
   "ten_co_so": "Quầy thuốc Bảo Kim",
   "address": "Tổ 7, ấp Láng Cát, xã Tân Hải, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.504039,
   "Latitude": 107.0923331
 },
 {
   "STT": 775,
   "ten_co_so": "Nhà thuốc Nguyễn Thảo",
   "address": "Số 913 đường 30/4, Phường 11, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4100484,
   "Latitude": 107.1275379
 },
 {
   "STT": 776,
   "ten_co_so": "Quầy thuốc Nhi Sài Gòn",
   "address": "Tổ 3, ấp Mỹ Tân, xã Mỹ Xuân , Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6327258,
   "Latitude": 107.0548264
 },
 {
   "STT": 777,
   "ten_co_so": "Nhà thuốc Duy Anh",
   "address": "57 Nơ Trang Long, phường Rạch Dừa, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3949932,
   "Latitude": 107.1120697
 },
 {
   "STT": 778,
   "ten_co_so": "Nhà thuốc Seaview",
   "address": "D1-3/6, trung tâm Đô thị Chí Linh, P. Nguyễn An Ninh, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3787996,
   "Latitude": 107.1118176
 },
 {
   "STT": 779,
   "ten_co_so": "Quầy thuốc Xuân Vinh",
   "address": "D22/2, ấp Phước Thái, xã Phước Tỉnh, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4141066,
   "Latitude": 107.2013665
 },
 {
   "STT": 780,
   "ten_co_so": "Quầy thuốc Thoại Khanh",
   "address": "Tổ 28, thôn Quảng Thành 1, xã Nghĩa Thành, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5677597,
   "Latitude": 107.211463
 },
 {
   "STT": 781,
   "ten_co_so": "Quầy thuốc Anh Việt",
   "address": "Ấp Mỹ Thạnh, xã Mỹ Xuân, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6168779,
   "Latitude": 107.0668172
 },
 {
   "STT": 782,
   "ten_co_so": "Nhà thuốc Hiển Vinh",
   "address": "79 Ba Cu, phường 4, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3509586,
   "Latitude": 107.0783253
 },
 {
   "STT": 783,
   "ten_co_so": "Quầy thuốc Minh Quân",
   "address": "Số 12, tổ 3, ấp Lò Voi, xã Phước Hưng, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4138775,
   "Latitude": 107.2009961
 },
 {
   "STT": 784,
   "ten_co_so": "Quầy thuốc Thiên Ân",
   "address": "Tổ 6, ấp Mỹ Hòa, xã Long Mỹ, Đất Đỏ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4343,
   "Latitude": 107.268429
 },
 {
   "STT": 785,
   "ten_co_so": "Quầy thuốc Lan Anh",
   "address": "7 Ô 1/14, khu phố Hải Lạc, thị trấn Phước Hải, Đất Đỏ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.414526,
   "Latitude": 107.278711
 },
 {
   "STT": 786,
   "ten_co_so": "Quầy thuốc Số 238",
   "address": "Tổ 10, Ô 2, khu phố Hải Điền 2, thị trấn Long Hải, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3976551,
   "Latitude": 107.2374214
 },
 {
   "STT": 787,
   "ten_co_so": "Nhà thuốc Anh Đức",
   "address": "151 Xô Viết Nghệ Tĩnh, P. Thắng Tam, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3466609,
   "Latitude": 107.0860672
 },
 {
   "STT": 788,
   "ten_co_so": "Quầy thuốc Đất Đỏ",
   "address": "168A/Ô3, khu phố Thanh Tân, thị trấn Đất Đỏ, Đất Đỏ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4913475,
   "Latitude": 107.2725505
 },
 {
   "STT": 789,
   "ten_co_so": "Nhà thuốc Hiền Lê",
   "address": "127 Nguyễn An Ninh, phường Thắng Nhì, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3701588,
   "Latitude": 107.0797161
 },
 {
   "STT": 790,
   "ten_co_so": "Quầy thuốc Ngọc Hiền",
   "address": "Tổ 3, khu phố Hải Phong 2, thị trấn Long Hải, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.385358,
   "Latitude": 107.237932
 },
 {
   "STT": 791,
   "ten_co_so": "Nhà thuốc Nhi đồng Sài Gòn- VT",
   "address": "Số 210 Bình Giã, Phường 8, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3565594,
   "Latitude": 107.0940574
 },
 {
   "STT": 792,
   "ten_co_so": "Quầy thuốc Quỳnh Anh III",
   "address": "Tổ 4 khu phố Vạn Hạnh, thị trấn  Phú Mỹ, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5892425,
   "Latitude": 107.0710693
 },
 {
   "STT": 793,
   "ten_co_so": "Nhà thuốc Thảo Tiên",
   "address": "749 Bình Giã, phường 10, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3831057,
   "Latitude": 107.1143635
 },
 {
   "STT": 794,
   "ten_co_so": "Quầy thuốc Thiên An",
   "address": "Chợ xã Bông Trang,  xã Bông Trang, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.55278,
   "Latitude": 107.4548399
 },
 {
   "STT": 795,
   "ten_co_so": "Quầy thuốc Thu Hà",
   "address": "Tổ 3, ấp Mỹ Tân, xã Mỹ Xuân , Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6327258,
   "Latitude": 107.0548264
 },
 {
   "STT": 796,
   "ten_co_so": "Quầy thuốc Châu Thư",
   "address": "Tổ 5, ấp Suối Nhum, xã Hắc Dịch, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6271597,
   "Latitude": 107.1005
 },
 {
   "STT": 797,
   "ten_co_so": "Quầy thuốc Hồng Phúc",
   "address": "189 tổ 12, khu phố Phước Lộc, thị trấn Phước Bửu, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5353285,
   "Latitude": 107.4055814
 },
 {
   "STT": 798,
   "ten_co_so": "Quầy thuốc Minh Thu",
   "address": "Số 101 E2, khu phố Long Liên, thị trấn Long Điền, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5056039,
   "Latitude": 107.1883273
 },
 {
   "STT": 799,
   "ten_co_so": "Quầy thuốc Minh Thư",
   "address": "107/5, ấp Nhân Tâm, xã Xuyên Mộc, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5585237,
   "Latitude": 107.4262813
 },
 {
   "STT": 800,
   "ten_co_so": "Quầy thuốc Mỹ Thư",
   "address": "Tổ 20, ấp Mỹ Thuận, xã Long Mỹ, Đất Đỏ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4372227,
   "Latitude": 107.2725505
 },
 {
   "STT": 801,
   "ten_co_so": "Quầy thuốc Tân Thư",
   "address": "Thôn Trung Sơn, xã Suối Nghệ, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5865463,
   "Latitude": 107.2050624
 },
 {
   "STT": 802,
   "ten_co_so": "Quầy thuốc Thư Kỳ",
   "address": "Tổ 22, ấp Hải Sơn, xã Phước Hòa, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5367551,
   "Latitude": 107.0765948
 },
 {
   "STT": 803,
   "ten_co_so": "Nhà thuốc Tường Vân",
   "address": "Số 372, Hùng Vương, khu phố 1, phường Long Tâm, Bà Rịa, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.512407,
   "Latitude": 107.186255
 },
 {
   "STT": 804,
   "ten_co_so": "Quầy thuốc Uyên Thư",
   "address": "Đường số 01, tổ 3/162, khu phố Hải Bình, thị trấn Long Hải, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.395902,
   "Latitude": 107.2370874
 },
 {
   "STT": 805,
   "ten_co_so": "TTTYT Xã Suối Nghệ",
   "address": "Đường số 1, thôn Trung Sơn, xã Suối Nghệ, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5923591,
   "Latitude": 107.2164298
 },
 {
   "STT": 806,
   "ten_co_so": "Quầy thuốc Thúy Hiền",
   "address": "Thôn 6, xã Long Sơn, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4637689,
   "Latitude": 107.0842094
 },
 {
   "STT": 807,
   "ten_co_so": "Quầy thuốc Yến Nhung",
   "address": "Tổ10, ấp Lam Sơn, xã Phước Hoà, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5298175,
   "Latitude": 107.0802641
 },
 {
   "STT": 808,
   "ten_co_so": "Nhà thuốc Đức Hiển",
   "address": "Số 121 Nguyễn Văn Cừ, khu phố 2, phường Long Toàn, Bà Rịa, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4951288,
   "Latitude": 107.1963332
 },
 {
   "STT": 809,
   "ten_co_so": "Quầy thuốc Kim Ngân",
   "address": "Ấp Phước Tân 1, xã Tân Hưng, Bà Rịa, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.531944,
   "Latitude": 107.175
 },
 {
   "STT": 810,
   "ten_co_so": "TTTYT Xã Phước Tỉnh",
   "address": "Ấp Phước Lợi, xã Phước Tỉnh, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4121196,
   "Latitude": 107.1933666
 },
 {
   "STT": 811,
   "ten_co_so": "TTTYT Xã Tân Hòa",
   "address": "Ấp Phước Tấn, xã Tân Hòa, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5193791,
   "Latitude": 107.0947444
 },
 {
   "STT": 812,
   "ten_co_so": "Quầy thuốc Formosa",
   "address": "Toà nhà TT.CSKH khu Công nghiệp Mỹ Xuân A2, ấp Phú hà, xã Mỹ Xuân, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6335408,
   "Latitude": 107.0456348
 },
 {
   "STT": 813,
   "ten_co_so": "Quầy thuốc Hồng Diễm",
   "address": "Ấp 4B, xã Tân Lâm, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.7377268,
   "Latitude": 107.4203669
 },
 {
   "STT": 814,
   "ten_co_so": "Nhà thuốc Kim Như",
   "address": "402 Bình Giã, phường Nguyễn An Ninh, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3725893,
   "Latitude": 107.0999221
 },
 {
   "STT": 815,
   "ten_co_so": "Quầy thuốc Kim Thương",
   "address": "G61 tổ 3, ấp Tân Phước, xã Phước Tỉnh, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.402897,
   "Latitude": 107.186093
 },
 {
   "STT": 816,
   "ten_co_so": "Quầy thuốc Linh Đan 3",
   "address": "Thôn 6, xã Long Sơn, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4637689,
   "Latitude": 107.0842094
 },
 {
   "STT": 817,
   "ten_co_so": "Nhà thuốc Tâm Đức",
   "address": "21 Nguyễn Bỉnh Khiêm, phường 3, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3411558,
   "Latitude": 107.0817279
 },
 {
   "STT": 818,
   "ten_co_so": "Quầy thuốc Xuân Thương",
   "address": "ấp Tân Phú, xã Châu Pha, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5737587,
   "Latitude": 107.153931
 },
 {
   "STT": 819,
   "ten_co_so": "Quầy thuốc Gia Hân",
   "address": "E11, tổ 02, ấp Phước Thiện, xã Phước Tỉnh, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.412436,
   "Latitude": 107.200427
 },
 {
   "STT": 820,
   "ten_co_so": "Quầy thuốc Quỳnh Anh",
   "address": "số 39L1, KP. Long Hiệp, TT. Long Điền, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4801367,
   "Latitude": 107.2121441
 },
 {
   "STT": 821,
   "ten_co_so": "Quầy thuốc Số 212",
   "address": "Tổ 25, ấp An Hải, xã Lộc An, Đất đỏ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4662722,
   "Latitude": 107.3374762
 },
 {
   "STT": 822,
   "ten_co_so": "Quầy thuốc Hương Sơn",
   "address": "Tổ 4, thôn Láng Cát, xã Tân Hải, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.504039,
   "Latitude": 107.0923331
 },
 {
   "STT": 823,
   "ten_co_so": "Nhà thuốc Á Châu",
   "address": "111 Lê Lai, phường 1, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3495989,
   "Latitude": 107.0782784
 },
 {
   "STT": 824,
   "ten_co_so": "Nhà thuốc Bảo Hân ",
   "address": "179 Lê Hồng Phong, phường 8, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3530302,
   "Latitude": 107.0874364
 },
 {
   "STT": 825,
   "ten_co_so": "Quầy thuốc Kim Thúy",
   "address": "Trung tâm thương mại Phú Mỹ, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.590556,
   "Latitude": 107.048056
 },
 {
   "STT": 826,
   "ten_co_so": "Quầy thuốc Kim Thúy 2",
   "address": "Khu phố Tân Ngọc, thị trấn Phú Mỹ, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6052066,
   "Latitude": 107.0504989
 },
 {
   "STT": 827,
   "ten_co_so": "Quầy thuốc Ngọc Ánh",
   "address": "28 tổ 8, ấp Phú Thọ, xã Hòa Hiệp, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6874344,
   "Latitude": 107.4963406
 },
 {
   "STT": 828,
   "ten_co_so": "Quầy thuốc Ngọc Thuý",
   "address": "Thôn Phước Tấn, xã Tân Hoà, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5319353,
   "Latitude": 107.1043967
 },
 {
   "STT": 829,
   "ten_co_so": "Quầy thuốc Phương Thúy",
   "address": "Tổ 10, thôn Quảng Phú, xã Đá Bạc, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5840475,
   "Latitude": 107.2513178
 },
 {
   "STT": 830,
   "ten_co_so": "Quầy thuốc Tâm Thúy",
   "address": "Ấp Tân An, xã Phước Tân, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5670715,
   "Latitude": 107.3730563
 },
 {
   "STT": 831,
   "ten_co_so": "Quầy thuốc Thanh Thanh",
   "address": "124 Hùng Vương, thị trấn Ngãi Giao, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.643258,
   "Latitude": 107.2449091
 },
 {
   "STT": 832,
   "ten_co_so": "Quầy thuốc Thu Hà",
   "address": "Tổ 19, Thôn Hưng Long, xã Kim Long, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.7155986,
   "Latitude": 107.2378916
 },
 {
   "STT": 833,
   "ten_co_so": "Quầy thuốc Thúy Đạt",
   "address": "Tổ 3, khu phố Phú Thạnh, phường Mỹ Xuân, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5905556,
   "Latitude": 107.0480556
 },
 {
   "STT": 834,
   "ten_co_so": "Quầy thuốc Thúy Hà",
   "address": "53 đường số 9, thôn Gio An, xã Suối Nghệ, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.58925,
   "Latitude": 107.1706398
 },
 {
   "STT": 835,
   "ten_co_so": "Nhà thuốc Trần Thanh Tâm",
   "address": "456/36 Bình Giã, phường Nguyễn An Ninh, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.353438,
   "Latitude": 107.089392
 },
 {
   "STT": 836,
   "ten_co_so": "Nhà thuốc Việt Hưng",
   "address": "223 Phạm Hồng Thái, phường 7, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3684895,
   "Latitude": 107.0828516
 },
 {
   "STT": 837,
   "ten_co_so": "TTTYT Xã Kim Long",
   "address": "Thôn Lạc Long, xã Kim Long, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.705,
   "Latitude": 107.231111
 },
 {
   "STT": 838,
   "ten_co_so": "Quầy thuốc Gia Huy",
   "address": "19/1, ấp Thanh Bình 3, xã Bình Châu, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5551551,
   "Latitude": 107.5477832
 },
 {
   "STT": 839,
   "ten_co_so": "Nhà thuốc Lộc Tiên",
   "address": "246 Nguyễn An Ninh, Phường 7, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3688688,
   "Latitude": 107.0828497
 },
 {
   "STT": 840,
   "ten_co_so": "Quầy thuốc Thụy Linh",
   "address": "354/13 ấp Bình Trung, xã Bình Châu, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.57921,
   "Latitude": 107.5386809
 },
 {
   "STT": 841,
   "ten_co_so": "Quầy thuốc 150",
   "address": "220 Độc Lập, khu phố Quảng Phú, phường Phú Mỹ, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5905556,
   "Latitude": 107.0480556
 },
 {
   "STT": 842,
   "ten_co_so": "Quầy thuốc Diễm Thủy",
   "address": "Tổ 3, thôn Sơn Thuận, xã Xuân Sơn, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6404024,
   "Latitude": 107.3198424
 },
 {
   "STT": 843,
   "ten_co_so": "Quầy thuốc Đức Minh",
   "address": "Tổ 09/5, khu phố Hải Điền, thị trấn Long Hải, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3851624,
   "Latitude": 107.2401989
 },
 {
   "STT": 844,
   "ten_co_so": "Nhà thuốc Gia Phước",
   "address": "Số 145 Nguyễn Tất Thành,  phường Phước Nguyyên, Bà Rịa, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4972403,
   "Latitude": 107.1803158
 },
 {
   "STT": 845,
   "ten_co_so": "Quầy thuốc Ngọc Điểm",
   "address": "Tổ 6, thôn Đức Mỹ, xã Suối Nghệ, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5909619,
   "Latitude": 107.2306159
 },
 {
   "STT": 846,
   "ten_co_so": "Quầy thuốc Ngọc Thủy",
   "address": "Lô 5, khu C, chợ Chu Hải, xã Tân Hải, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4999125,
   "Latitude": 107.1195602
 },
 {
   "STT": 847,
   "ten_co_so": "Nhà thuốc Nhân Đức",
   "address": "935F Bình Giã, phường 10, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.392565,
   "Latitude": 107.1182247
 },
 {
   "STT": 848,
   "ten_co_so": "Nhà thuốc Nhật Minh",
   "address": "1774 đường 30/4, phường 12, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4100558,
   "Latitude": 107.1276599
 },
 {
   "STT": 849,
   "ten_co_so": "Quầy thuốc Phước Nguyên",
   "address": "Tổ 12, ấp Ông Trịnh, xã Tân Phước, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5567116,
   "Latitude": 107.065233
 },
 {
   "STT": 850,
   "ten_co_so": "Quầy thuốc Số 150",
   "address": "Thôn Quảng Phú, thị trấn Phú Mỹ, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5994608,
   "Latitude": 107.0552406
 },
 {
   "STT": 851,
   "ten_co_so": "Quầy thuốc Thanh Thủy 09",
   "address": "Số 17, Ngô Quyền, xã Tân Ngọc, thị trấn Phú Mỹ, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.607426,
   "Latitude": 107.05298
 },
 {
   "STT": 852,
   "ten_co_so": "Quầy thuốc Tịnh Tâm",
   "address": "Tổ 15, Ấp Phước Tấn, xã Tân Hòa, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.519433,
   "Latitude": 107.087105
 },
 {
   "STT": 853,
   "ten_co_so": "Quầy thuốc Trần Thanh",
   "address": "Số 153/6, TL 328, xã Phước Tân, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5537883,
   "Latitude": 107.3875831
 },
 {
   "STT": 854,
   "ten_co_so": "Quầy thuốc Vĩnh Đức 2",
   "address": "Tổ 7, ấp Tân Lộc, xã Phước Hòa, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5124259,
   "Latitude": 107.0903698
 },
 {
   "STT": 855,
   "ten_co_so": "Nhà thuốc Thiên Lộc",
   "address": "49 Đội Cấn, phường 8, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3629904,
   "Latitude": 107.0927149
 },
 {
   "STT": 856,
   "ten_co_so": "Quầy thuốc An Tâm",
   "address": "Tổ 4, khu phố Ông Trịnh, phường Tân Phước, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5567116,
   "Latitude": 107.065233
 },
 {
   "STT": 857,
   "ten_co_so": "Nhà thuốc Dương Thùy",
   "address": "20 Hoàng Hoa Thám, phường 2, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3407104,
   "Latitude": 107.0790893
 },
 {
   "STT": 858,
   "ten_co_so": "Quầy thuốc Hoàng Lam",
   "address": "B22, tổ  4, ấp Phước Bình, xã Phước Tỉnh, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4117133,
   "Latitude": 107.1987423
 },
 {
   "STT": 859,
   "ten_co_so": "thuốc từ DL Kim Tiên",
   "address": "Ấp Mỹ Thạnh, xã Mỹ Xuân, , Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6168779,
   "Latitude": 107.0668172
 },
 {
   "STT": 860,
   "ten_co_so": "Quầy thuốc Mai Tiên",
   "address": "Tổ 11, khu phố Tân Ngọc, thị trấn Phú Mỹ, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5573341,
   "Latitude": 107.0598491
 },
 {
   "STT": 861,
   "ten_co_so": "Quầy thuốc Thủy Tiên",
   "address": "Tổ 1, ấp Đông Hải, xã Tân Hải, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5005153,
   "Latitude": 107.1071
 },
 {
   "STT": 862,
   "ten_co_so": "Quầy thuốc Thảo Nhi",
   "address": "Tổ 02, ấp Phước Lộc, xã Phước Hưng, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4077885,
   "Latitude": 107.2207964
 },
 {
   "STT": 863,
   "ten_co_so": "Nhà thuốc Tuệ Minh",
   "address": "Số 06 đường Võ Văn Kiệt, phường Long Tâm, Bà Rịa, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.514704,
   "Latitude": 107.2040408
 },
 {
   "STT": 864,
   "ten_co_so": "Quầy thuốc Tuấn Hà",
   "address": "Tổ 43, thôn Thạch Long, xã Kim Long, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.7050987,
   "Latitude": 107.2311774
 },
 {
   "STT": 865,
   "ten_co_so": "Quầy thuốc Linh Đan",
   "address": "Thôn 7, tổ 3, xã Long Sơn, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.446867,
   "Latitude": 107.087897
 },
 {
   "STT": 866,
   "ten_co_so": "Quầy thuốc Tình Vinh",
   "address": "Tổ 28, thôn Quảng Thành 1, xã Nghĩa Thành, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5677597,
   "Latitude": 107.211463
 },
 {
   "STT": 867,
   "ten_co_so": "Quầy thuốc An Hân",
   "address": "Tổ 6, ấp Phước Thuận, xã Phước Tỉnh, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.412436,
   "Latitude": 107.200427
 },
 {
   "STT": 868,
   "ten_co_so": "Quầy thuốc Ngọc Ngân",
   "address": "Tổ 9, ấp Trảng Cát, xã Hắc Dịch, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6401209,
   "Latitude": 107.0889812
 },
 {
   "STT": 869,
   "ten_co_so": "Quầy thuốc An Bình",
   "address": "Đường Phan Chu Trinh, tổ 7, khu phố Ngọc Hà, thị trấn Phú Mỹ, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5905345,
   "Latitude": 107.0480378
 },
 {
   "STT": 870,
   "ten_co_so": "Quầy thuốc Việt Anh",
   "address": "Tổ 27, thôn Quảng Thành 1, xã Nghĩa Thành, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5718057,
   "Latitude": 107.1898113
 },
 {
   "STT": 871,
   "ten_co_so": "TTTYT Bình Châu",
   "address": "Quốc lộ 55, ấp Láng Găng, xã Bình Châu, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5578171,
   "Latitude": 107.5366654
 },
 {
   "STT": 872,
   "ten_co_so": "Quầy thuốc Hoàng Phú",
   "address": "4Ô3/23, khu phố Phước Trung, thị trấn Phước Hải, Đất Đỏ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4902864,
   "Latitude": 107.2711131
 },
 {
   "STT": 873,
   "ten_co_so": "Quầy thuốc Ngọc Trân",
   "address": "Tổ 4, khu phố Ngọc Hà, thị trấn Phú Mỹ, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6052902,
   "Latitude": 107.0513143
 },
 {
   "STT": 874,
   "ten_co_so": "Quầy thuốc Phú Thịnh",
   "address": "Số 22, ấp Hải Sơn, xã Phước Hưng, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3918408,
   "Latitude": 107.2325626
 },
 {
   "STT": 875,
   "ten_co_so": "Quầy thuốc Thanh Thúy",
   "address": "Tổ 16, ấp Mỹ Tân, xã Mỹ Xuân , Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6327258,
   "Latitude": 107.0548264
 },
 {
   "STT": 876,
   "ten_co_so": "Quầy thuốc Thanh Y",
   "address": "Kiosque số 12, chợ Hòa Long, xã Hòa Long, Bà Rịa, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5211045,
   "Latitude": 107.2055403
 },
 {
   "STT": 877,
   "ten_co_so": "Quầy thuốc Thu Trâm",
   "address": "Tổ 2, thôn Quảng Giao, xã Xuân Sơn, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6404024,
   "Latitude": 107.3198424
 },
 {
   "STT": 878,
   "ten_co_so": "TTTYT Xã Bàu Chinh",
   "address": "Thôn Tân Hiệp, xã Bàu Chinh, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.678333,
   "Latitude": 107.225
 },
 {
   "STT": 879,
   "ten_co_so": "Quầy thuốc Bảo Trân",
   "address": "1Ô1/1, KP. Hải Trung, thị trấn Phước Hải, Đất Đỏ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4054168,
   "Latitude": 107.2607289
 },
 {
   "STT": 880,
   "ten_co_so": "Quầy thuốc Bảo Châu",
   "address": "102/4, ấp Nhân Nghĩa, xã Xuyên Mộc, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5585237,
   "Latitude": 107.4262813
 },
 {
   "STT": 881,
   "ten_co_so": "Quầy thuốc Gia Phú",
   "address": "Tổ 1, ấp Gò Cà, xã Phước Thuận, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4790005,
   "Latitude": 107.3967105
 },
 {
   "STT": 882,
   "ten_co_so": "Quầy thuốc Hà Lan",
   "address": "E48, tổ 01, ấp Phước Thiện, xã Phước Tỉnh, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.412436,
   "Latitude": 107.200427
 },
 {
   "STT": 883,
   "ten_co_so": "Nhà thuốc Hà Vy",
   "address": "223 Lê Lợi, phường 6, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3701231,
   "Latitude": 107.0765804
 },
 {
   "STT": 884,
   "ten_co_so": "Quầy thuốc Hoàng Lực 1",
   "address": "Tổ 31, ấp Tiến Thành, xã Quảng Thành, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.694576,
   "Latitude": 107.2782896
 },
 {
   "STT": 885,
   "ten_co_so": "Quầy thuốc Hoàng Trang",
   "address": "1223 chợ Phước Tỉnh, xã Phước Tỉnh, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4057146,
   "Latitude": 107.1871018
 },
 {
   "STT": 886,
   "ten_co_so": "Nhà thuốc Khánh Quân",
   "address": "551 đường Nguyễn An Ninh, phường Nguyễn An Ninh, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3619666,
   "Latitude": 107.0958048
 },
 {
   "STT": 887,
   "ten_co_so": "Quầy thuốc Kim Trang",
   "address": "Thôn 10, xã Long Sơn, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4529906,
   "Latitude": 107.0896654
 },
 {
   "STT": 888,
   "ten_co_so": "Quầy thuốc Phúc An",
   "address": "Số 235, Lê Hồng Phong, thị trấn Ngãi Giao, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6483193,
   "Latitude": 107.2410428
 },
 {
   "STT": 889,
   "ten_co_so": "Quầy thuốc Sài Gòn 09",
   "address": "Tổ 1/63, Ô1, khu phố Hải Bình, thị trấn Long Hải, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.395902,
   "Latitude": 107.2370874
 },
 {
   "STT": 890,
   "ten_co_so": "Quầy thuốc Số 186",
   "address": "Khu phố Bến Đình, phường Mỹ Xuân, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3736826,
   "Latitude": 107.0808893
 },
 {
   "STT": 891,
   "ten_co_so": "Quầy thuốc Số 192",
   "address": "Tổ  2, ấp Phước Lộc, xã Tân Phước, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 15.9128998,
   "Latitude": 79.7399875
 },
 {
   "STT": 892,
   "ten_co_so": "Nhà thuốc Tâm Đức",
   "address": "446 Lê Lợi, phường Thắng Nhì, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3711059,
   "Latitude": 107.076811
 },
 {
   "STT": 893,
   "ten_co_so": "Quầy thuốc Tâm Như",
   "address": "167 Phan Bội Châu, thị trấn Phú Mỹ, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6007307,
   "Latitude": 107.0502036
 },
 {
   "STT": 894,
   "ten_co_so": "Quầy thuốc Tân Tiến",
   "address": "Số 11/1 ấp Phú Bình, xã Hoà Hiệp, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6787414,
   "Latitude": 107.5031811
 },
 {
   "STT": 895,
   "ten_co_so": "Quầy thuốc Thiên Trang",
   "address": "Tổ 73, ấp Liên Hiệp, xã Xà Bang, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.7240106,
   "Latitude": 107.2172995
 },
 {
   "STT": 896,
   "ten_co_so": "Quầy thuốc Thu Trang",
   "address": "Tổ 5, ấp  Tân Tiến, xã Châu Pha, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5737817,
   "Latitude": 107.1543601
 },
 {
   "STT": 897,
   "ten_co_so": "Quầy thuốc Thu Trang",
   "address": "Tổ 8, khu phố Quảng Phú, thị trấn Phú Mỹ, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5969113,
   "Latitude": 107.0521311
 },
 {
   "STT": 898,
   "ten_co_so": "Quầy thuốc Thu Trang",
   "address": "Tổ 83, thôn Tân Long, xã Kim Long, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.705,
   "Latitude": 107.2311111
 },
 {
   "STT": 899,
   "ten_co_so": "Quầy thuốc Thùy Trang",
   "address": "56/Ô 3, căn B, khu phố Thanh Long, thị trấn Đất Đỏ, Đất Đỏ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4913475,
   "Latitude": 107.2725505
 },
 {
   "STT": 900,
   "ten_co_so": "Quầy thuốc Trường An",
   "address": "Tổ 6, ấp Phước Hữu, xã Long Phước, Bà Rịa, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5138192,
   "Latitude": 107.2253817
 },
 {
   "STT": 901,
   "ten_co_so": "Nhà thuốc Trường Chinh",
   "address": "203 Nguyễn Hữu Cảnh, phường Thắng Nhất, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3864536,
   "Latitude": 107.1077556
 },
 {
   "STT": 902,
   "ten_co_so": "Nhà thuốc Yến Trang",
   "address": "Thôn 4, xã Long Sơn, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4525,
   "Latitude": 107.089167
 },
 {
   "STT": 903,
   "ten_co_so": "Nhà thuốc Thường Kiệt",
   "address": "62 Lý Thường Kiệt, phường 1, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3492061,
   "Latitude": 107.0768851
 },
 {
   "STT": 904,
   "ten_co_so": "Quầy thuốc Khánh Linh",
   "address": "C28, tổ 3, ấp Phước Hòa, xã Phước Tỉnh, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4118663,
   "Latitude": 107.1927658
 },
 {
   "STT": 905,
   "ten_co_so": "Quầy thuốc Minh Trí",
   "address": "Ấp Bà Rịa, xã Phước Tân, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5670715,
   "Latitude": 107.3730563
 },
 {
   "STT": 906,
   "ten_co_so": "Quầy thuốc Ngọc Hợi",
   "address": "Thôn Sông Cầu, xã Nghĩa Thành, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5673977,
   "Latitude": 107.2113457
 },
 {
   "STT": 907,
   "ten_co_so": "Quầy thuốc Số 167",
   "address": "64 Mạc Thanh Đạm, thị trấn Long Điền, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4861474,
   "Latitude": 107.2121473
 },
 {
   "STT": 908,
   "ten_co_so": "Quầy thuốc Bảo Ngọc",
   "address": "Tổ 15/8, KP. Hải Vân, TT. Long Hải, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4067208,
   "Latitude": 107.2370874
 },
 {
   "STT": 909,
   "ten_co_so": "Quầy thuốc Bảo Bình",
   "address": "ấp Láng Găng, xã Bình Châu, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5582593,
   "Latitude": 107.5393641
 },
 {
   "STT": 910,
   "ten_co_so": "Quầy thuốc Bảo Quân",
   "address": "Kiốt số 12, chợ Long Phước, ấp Tây, xã Long Phước, Bà Rịa, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.52333,
   "Latitude": 107.2220913
 },
 {
   "STT": 911,
   "ten_co_so": "Quầy thuốc Gia Khang",
   "address": "Tổ 2, ấp Phước Lập, xã Mỹ Xuân, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6456132,
   "Latitude": 107.0495635
 },
 {
   "STT": 912,
   "ten_co_so": "Quầy thuốc Hồng Tâm",
   "address": "Tổ 4, ấp Gia Hòa Yên, xã Bình Giã, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6463909,
   "Latitude": 107.2707516
 },
 {
   "STT": 913,
   "ten_co_so": "Quầy thuốc Kiều Trinh",
   "address": "Ấp 3, chợ Hòa Hội, xã Hòa Hội, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6279317,
   "Latitude": 107.4440254
 },
 {
   "STT": 914,
   "ten_co_so": "Quầy thuốc Mỹ Trinh",
   "address": "Tổ 3, xã Tân Hà, xã Châu Pha, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5737817,
   "Latitude": 107.1543601
 },
 {
   "STT": 915,
   "ten_co_so": "Quầy thuốc Ngọc Trinh",
   "address": "Tổ 5, ấp Hải Sơn, xã Phước Hưng, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.428333,
   "Latitude": 107.236944
 },
 {
   "STT": 916,
   "ten_co_so": "Quầy thuốc Thuận An",
   "address": "Tổ 8, thôn Trung Sơn, xã Suối Nghệ, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6043384,
   "Latitude": 107.1898113
 },
 {
   "STT": 917,
   "ten_co_so": "Quầy thuốc Hồng Hạnh",
   "address": "Tổ 13, ấp An Lộc, xã An Nhứt, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4696887,
   "Latitude": 107.2140964
 },
 {
   "STT": 918,
   "ten_co_so": "Quầy thuốc Trọng",
   "address": "Tổ 3, ấp Bến Đình, xã Mỹ Xuân, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3736826,
   "Latitude": 107.0808893
 },
 {
   "STT": 919,
   "ten_co_so": "Quầy thuốc Bệnh viện y học  cổ truyền ",
   "address": "Ấp Tây, xã Long Phước, Bà Rịa, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5275353,
   "Latitude": 107.2282686
 },
 {
   "STT": 920,
   "ten_co_so": "Nhà thuốc Mai Hà",
   "address": "76 Nguyễn Văn Trỗi, phường 4, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3550506,
   "Latitude": 107.0780323
 },
 {
   "STT": 921,
   "ten_co_so": "Quầy thuốc Medic Sài Gòn",
   "address": "Số 41/4, ấp Phú Bình, xã Hòa Hiệp, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6787414,
   "Latitude": 107.5031811
 },
 {
   "STT": 922,
   "ten_co_so": "Quầy thuốc Số 211",
   "address": "5 Ô 2/22, khu phố Hải Trung, thị trấn Phước Hải, Đất Đỏ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4054168,
   "Latitude": 107.2607289
 },
 {
   "STT": 923,
   "ten_co_so": "TTTYT Xã An Ngãi",
   "address": "Ấp An Nhứt, xã An Ngãi, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4277356,
   "Latitude": 107.2212852
 },
 {
   "STT": 924,
   "ten_co_so": "Nhà thuốc Phương Hoa",
   "address": "387A Nguyễn Hữu Cảnh, phường 10, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3742688,
   "Latitude": 107.1182125
 },
 {
   "STT": 925,
   "ten_co_so": "Nhà thuốc Minh",
   "address": "78 Lý Thường Kiệt, phường 1, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3494416,
   "Latitude": 107.0768153
 },
 {
   "STT": 926,
   "ten_co_so": "Nhà thuốc Phước Thắng",
   "address": "27 Phước Thắng, phường 12, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4135677,
   "Latitude": 107.1613183
 },
 {
   "STT": 927,
   "ten_co_so": "Quầy thuốc Sài Gòn ",
   "address": "59 QL 55, khu phố Phước Hòa, thị trấn Phước Bửu, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.534124,
   "Latitude": 107.3956206
 },
 {
   "STT": 928,
   "ten_co_so": "Quầy thuốc Thanh Trung",
   "address": "Đường số 1, tổ 18, thôn Suối Nghệ, xã Suối Nghệ, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5923591,
   "Latitude": 107.2164298
 },
 {
   "STT": 929,
   "ten_co_so": "Quầy thuốc Thiên Phú",
   "address": "Tổ 5/3, Ô3, khu phố Hải Sơn, thị trấn Long Hải, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.418488,
   "Latitude": 107.2833871
 },
 {
   "STT": 930,
   "ten_co_so": "TTTYT Xã Mỹ Xuân",
   "address": "Ấp Thị Vải, xã Mỹ Xuân, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.620929,
   "Latitude": 107.055887
 },
 {
   "STT": 931,
   "ten_co_so": "TTTYT Xã Phước Thuận",
   "address": "Ấp Xóm Rẫy, xã Phước Thuận, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4932137,
   "Latitude": 107.3982236
 },
 {
   "STT": 932,
   "ten_co_so": "Nhà thuốc Đình Dũng",
   "address": "120 Hạ Long, P. 2, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3385725,
   "Latitude": 107.0718051
 },
 {
   "STT": 933,
   "ten_co_so": "Nhà thuốc Phương Thảo",
   "address": "16 Phan Văn Trị, phường Thắng Tam, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3375292,
   "Latitude": 107.0903154
 },
 {
   "STT": 934,
   "ten_co_so": "Quầy thuốc An Phú",
   "address": "Đường 81, khu phố Tân Phú, thị trấn Phú Mỹ, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5922866,
   "Latitude": 107.0546302
 },
 {
   "STT": 935,
   "ten_co_so": "Quầy thuốc Cẩm Tú",
   "address": "ấp 2 Đông, xã Bàu Lâm, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6970348,
   "Latitude": 107.3730563
 },
 {
   "STT": 936,
   "ten_co_so": "Quầy thuốc Nhã Uyên",
   "address": "Số 22, đường Độc Lập, ấp Tân Ngọc, thị trấn Phú Mỹ, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.605519,
   "Latitude": 107.0545872
 },
 {
   "STT": 937,
   "ten_co_so": "Quầy thuốc Số 219",
   "address": "Ấp Nhân Trí, xã Xuyên Mộc, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5585237,
   "Latitude": 107.4262813
 },
 {
   "STT": 938,
   "ten_co_so": "Nhà thuốc Hùng Thảo",
   "address": "Số 6, Hàn Thuyên, phường Thắng Nhất, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3893052,
   "Latitude": 107.1091783
 },
 {
   "STT": 939,
   "ten_co_so": "Quầy thuốc Số 191",
   "address": "Chợ mới, phường Hắc Dịch, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4099646,
   "Latitude": 107.1338118
 },
 {
   "STT": 940,
   "ten_co_so": "Quầy thuốc Số 202",
   "address": "Ấp Vĩnh An, xã Bình Giã, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6436111,
   "Latitude": 107.2605556
 },
 {
   "STT": 941,
   "ten_co_so": "Nhà thuốc Tuấn Nguyễn",
   "address": "269 Bình Giã, phường 8, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3587611,
   "Latitude": 107.0939677
 },
 {
   "STT": 942,
   "ten_co_so": "Quầy thuốc Gia Huy",
   "address": "Ấp Phước Lập, xã Mỹ Xuân, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6456132,
   "Latitude": 107.0495635
 },
 {
   "STT": 943,
   "ten_co_so": "TTTYT Hòa Hiệp",
   "address": "Ấp Phú Sơn, xã Hòa Hiệp, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6887378,
   "Latitude": 107.498116
 },
 {
   "STT": 944,
   "ten_co_so": "Nhà thuốc Phước Nguyên",
   "address": "Tổ 4, Trần Hưng Đạo, phường Phước Nguyên, Bà Rịa, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5005182,
   "Latitude": 107.1748586
 },
 {
   "STT": 945,
   "ten_co_so": "Nhà thuốc Lê Lợi",
   "address": "02 Nguyễn An Ninh, phường 7, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3701462,
   "Latitude": 107.0768985
 },
 {
   "STT": 946,
   "ten_co_so": "Quầy thuốc Thanh Nga",
   "address": "Tổ 4, khu phố Long Liên, thị trấn Long Điền, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4933173,
   "Latitude": 107.2168114
 },
 {
   "STT": 947,
   "ten_co_so": "Quầy thuốc Ngọc Ngân 2",
   "address": "Tổ 9, ấp Trảng Cát, xã Hắc Dịch, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6401209,
   "Latitude": 107.0889812
 },
 {
   "STT": 948,
   "ten_co_so": "Quầy thuốc Trung Kiên",
   "address": "Ấp Bình Minh, xã Bình Châu, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5436122,
   "Latitude": 107.5409258
 },
 {
   "STT": 949,
   "ten_co_so": "Nhà thuốc Bệnh viện Tâm thần",
   "address": "Ấp Bình Mỹ, xã Bình Ba, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6190082,
   "Latitude": 107.2317977
 },
 {
   "STT": 950,
   "ten_co_so": "Quầy thuốc Hoàng Tuyến",
   "address": "12/5 ấp Phước Lâm, xã Phước Hưng, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4124996,
   "Latitude": 107.2058125
 },
 {
   "STT": 951,
   "ten_co_so": "Quầy thuốc Quầy thuốc 90",
   "address": "Số 65J, khu phố Long An, thị trấn Long Điền, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4811517,
   "Latitude": 107.2115938
 },
 {
   "STT": 952,
   "ten_co_so": "Quầy thuốc Bích Tuyền",
   "address": "Tổ 1 ô 3 khu phố Hải Phong 1, thị trấn Long Hải, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.385358,
   "Latitude": 107.237932
 },
 {
   "STT": 954,
   "ten_co_so": "Quầy thuốc Ly Na",
   "address": "Tổ 16, khu phố Hải Bình, thị trấn Long Hải, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3842286,
   "Latitude": 107.2366329
 },
 {
   "STT": 955,
   "ten_co_so": "Quầy thuốc Phát Lộc",
   "address": "Tổ 6, thôn Tân Tiến , xã Châu Pha, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5737817,
   "Latitude": 107.1543601
 },
 {
   "STT": 956,
   "ten_co_so": "Quầy thuốc Số 259",
   "address": "Tổ 2, khu phố Song Vĩnh, phường Tân Phước, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.565034,
   "Latitude": 107.05809
 },
 {
   "STT": 957,
   "ten_co_so": "Quầy thuốc Sơn Tuyền",
   "address": "khu B, tổ 23, ấp An Điền, xã Lộc An, Đất Đỏ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4765664,
   "Latitude": 107.3434916
 },
 {
   "STT": 958,
   "ten_co_so": "Quầy thuốc Thanh Tuyền",
   "address": "3 Ô 1/10, khu phố Hải Trung, thị trấn Phước Hải, Đất Đỏ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.427116,
   "Latitude": 107.294412
 },
 {
   "STT": 959,
   "ten_co_so": "Quầy thuốc Thanh Tuyền",
   "address": "Tổ 5 thôn Phú Sơn, xã Đá Bạc, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5879937,
   "Latitude": 107.2693773
 },
 {
   "STT": 960,
   "ten_co_so": "Quầy thuốc Tuyết Lan",
   "address": "2066 tổ 8, thôn 2, xã Bình Trung, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6424077,
   "Latitude": 107.2843726
 },
 {
   "STT": 962,
   "ten_co_so": "Nhà thuốc Bạch Đằng",
   "address": "4B Trần Hưng Đạo, phường 3, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3424569,
   "Latitude": 107.0779829
 },
 {
   "STT": 963,
   "ten_co_so": "Quầy thuốc Bạch Tuyết",
   "address": "Tổ 3 ấp 2, xã Tóc Tiên, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6032018,
   "Latitude": 107.113007
 },
 {
   "STT": 965,
   "ten_co_so": "Nhà thuốc Hiếu Thảo",
   "address": "59 Lưu Chí Hiếu, phường Rạch Dừa, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3927796,
   "Latitude": 107.1067846
 },
 {
   "STT": 966,
   "ten_co_so": "Quầy thuốc Linh Tuyết",
   "address": "Tổ 31/9, khu phố Hải Hà 1, thị trấn Long Hải, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.395902,
   "Latitude": 107.2370874
 },
 {
   "STT": 967,
   "ten_co_so": "Quầy thuốc Như Tuyết",
   "address": "Tổ 03/86, khu phố Hải An, thị trấn Long Hải, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.395902,
   "Latitude": 107.2370874
 },
 {
   "STT": 968,
   "ten_co_so": "Quầy thuốc Số 163",
   "address": "Chợ cũ Long Điền, thị trấn Long Điền, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4933173,
   "Latitude": 107.2168114
 },
 {
   "STT": 969,
   "ten_co_so": "Quầy thuốc Anh Tài",
   "address": "tổ 13, thôn Đức Mỹ, xã Suối Nghệ, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5909619,
   "Latitude": 107.2306159
 },
 {
   "STT": 970,
   "ten_co_so": "Quầy thuốc Hiển Vinh",
   "address": "Tổ 4, ấp Phước Tân 3, Bà Rịa, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5308689,
   "Latitude": 107.1706079
 },
 {
   "STT": 971,
   "ten_co_so": "Quầy thuốc Hoài Uyên",
   "address": "Tổ 11, khu phố Thanh Long, thị trấn Đất Đỏ, Đất Đỏ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4913029,
   "Latitude": 107.2654597
 },
 {
   "STT": 972,
   "ten_co_so": "Quầy thuốc Khánh Minh",
   "address": "Tổ 13, ấp Bến Đình, xã Mỹ Xuân, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.624853,
   "Latitude": 107.053561
 },
 {
   "STT": 973,
   "ten_co_so": "Quầy thuốc Thu Thủy",
   "address": "Số 32, tổ 12, đường Độc Lập, khu phố Ngọc Hà, thị trấn Phú Mỹ, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6058116,
   "Latitude": 107.0545431
 },
 {
   "STT": 974,
   "ten_co_so": "Nhà thuốc Trường Khoa",
   "address": "966 Đường 30/4, phường 11, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4100558,
   "Latitude": 107.1276599
 },
 {
   "STT": 975,
   "ten_co_so": "Quầy thuốc 257",
   "address": "78 đường Độc Lập, phường Phú Mỹ, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6037543,
   "Latitude": 107.0543863
 },
 {
   "STT": 976,
   "ten_co_so": "Quầy thuốc An Đức",
   "address": "Chợ Ông Trịnh, xã Tân Phước, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.561854,
   "Latitude": 107.0608711
 },
 {
   "STT": 977,
   "ten_co_so": "Nhà thuốc Hoàng Châu",
   "address": "157A1, Đô Lương, phường 12, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4048757,
   "Latitude": 107.1437342
 },
 {
   "STT": 978,
   "ten_co_so": "Quầy thuốc Huyền Trâm",
   "address": "Chợ Sơn Bình, xã Sơn Bình, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6462999,
   "Latitude": 107.3297846
 },
 {
   "STT": 979,
   "ten_co_so": "Nhà thuốc Minh Châu",
   "address": "312 đường Lê Hồng Phong, phường 3, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3544848,
   "Latitude": 107.0851159
 },
 {
   "STT": 980,
   "ten_co_so": "Quầy thuốc Mỹ Hiệp",
   "address": "Tổ 1, ấp Nhân Tiến, xã Xuyên Mộc, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5657342,
   "Latitude": 107.4231952
 },
 {
   "STT": 981,
   "ten_co_so": "Nhà thuốc Ngọc Châu",
   "address": "10-12 Lê Quý Đôn, phường Phước Hiệp, Bà Rịa, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.493223,
   "Latitude": 107.1692587
 },
 {
   "STT": 982,
   "ten_co_so": "Quầy thuốc Số 225",
   "address": "Ấp Thạnh Sơn 2A, xã Phước Tân, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.576922,
   "Latitude": 107.373161
 },
 {
   "STT": 983,
   "ten_co_so": "Quầy thuốc Số 257",
   "address": "Số 03/1,  khu phố Ngọc Hà, thị trấn Phú Mỹ, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6052902,
   "Latitude": 107.0513143
 },
 {
   "STT": 984,
   "ten_co_so": "Nhà thuốc Thanh Hằng",
   "address": "42 Đồng Khởi, phường 1, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3511976,
   "Latitude": 107.0775305
 },
 {
   "STT": 985,
   "ten_co_so": "Quầy thuốc Thanh Vân",
   "address": "183 đường 1, thôn Đức Mỹ, xã Suối Nghệ, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.594188,
   "Latitude": 107.1848543
 },
 {
   "STT": 986,
   "ten_co_so": "Nhà thuốc Thảo Linh",
   "address": "661 Bình Giã, phường Thắng Nhất, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3808014,
   "Latitude": 107.1101993
 },
 {
   "STT": 987,
   "ten_co_so": "Quầy thuốc Thu Vân",
   "address": "Số 06/5A, ấp An Phước, xã An Ngãi , Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4141066,
   "Latitude": 107.2013665
 },
 {
   "STT": 988,
   "ten_co_so": "Quầy thuốc Thùy Vân",
   "address": "Tổ 12, ấp Phước Lập, xã Mỹ Xuân, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6456132,
   "Latitude": 107.0495635
 },
 {
   "STT": 989,
   "ten_co_so": "TTTYT Xã Xuân Sơn",
   "address": "Thôn Quảng Giao, xã Xuân Sơn, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.638002,
   "Latitude": 107.322502
 },
 {
   "STT": 990,
   "ten_co_so": "Nhà thuốc Khánh Uyên",
   "address": "345 đường 30/4, phường Rạch Dừa, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4072174,
   "Latitude": 107.1293273
 },
 {
   "STT": 992,
   "ten_co_so": "Quầy thuốc Ly Ly",
   "address": "Tổ 6, ấp Phú Hòa, xã Hòa Hiệp, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6787414,
   "Latitude": 107.5031811
 },
 {
   "STT": 993,
   "ten_co_so": "Nhà thuốc Chí Linh",
   "address": "151 Nguyễn Hữu Cảnh, phường Thắng Nhất, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3877862,
   "Latitude": 107.106584
 },
 {
   "STT": 994,
   "ten_co_so": "Quầy thuốc Minh Khôi",
   "address": "Tổ 12/5A, ấp An Thạnh, xã An Ngãi, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4696887,
   "Latitude": 107.2140964
 },
 {
   "STT": 995,
   "ten_co_so": "Quầy thuốc Ngọc Vi",
   "address": "Trung tâm thương mai Kim Long, xã Kim Long, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.7043005,
   "Latitude": 107.2445889
 },
 {
   "STT": 996,
   "ten_co_so": "Quầy thuốc Số  183",
   "address": "Lô D, khu phố Hải Sơn, thị trấn Phước Hải, Đất Đỏ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.418488,
   "Latitude": 107.2833871
 },
 {
   "STT": 997,
   "ten_co_so": "Quầy thuốc Tân Phát",
   "address": "C21 Trường Chinh, tổ 13, phường Tân Phú, phường Phú Mỹ, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5905345,
   "Latitude": 107.0480378
 },
 {
   "STT": 998,
   "ten_co_so": "Quầy thuốc Anh Thơ",
   "address": "Tổ 2, thôn bàu Điển, xã Đá Bạc, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5939448,
   "Latitude": 107.2784615
 },
 {
   "STT": 999,
   "ten_co_so": "Quầy thuốc Gia Hảo",
   "address": "Tổ 4, khu phố Hải Tân, thị trấn Phước Hải, Đất Đỏ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4054168,
   "Latitude": 107.2607289
 },
 {
   "STT": 1001,
   "ten_co_so": "Quầy thuốc Khánh Vũ",
   "address": "Tổ 29, khu 5B, ấp Hải Sơn, xã Phước Hòa, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5367551,
   "Latitude": 107.0765948
 },
 {
   "STT": 1002,
   "ten_co_so": "Quầy thuốc Minh Khôi",
   "address": "Ấp 2, xã Hắc Dịch, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6466114,
   "Latitude": 107.113007
 },
 {
   "STT": 1003,
   "ten_co_so": "Quầy thuốc Mười Trang",
   "address": "60/3 tổ 3, ấp Phú Vinh, xã Hòa Hiệp, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6787414,
   "Latitude": 107.5031811
 },
 {
   "STT": 1004,
   "ten_co_so": "Quầy thuốc Thanh Tuấn",
   "address": "Đường Huỳnh Minh Thạnh, khu phố Thanh Sơn, thị trấn Phước Bửu, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5353285,
   "Latitude": 107.4055814
 },
 {
   "STT": 1005,
   "ten_co_so": "Quầy thuốc Hoàng Vui",
   "address": "Tổ 8, ấp Bình Hoà, xã Bình Châu, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.559203,
   "Latitude": 107.545337
 },
 {
   "STT": 1006,
   "ten_co_so": "Nhà thuốc Mai Thanh",
   "address": "15 Đường 30/4, phường Thắng Nhì, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.408268,
   "Latitude": 107.1286931
 },
 {
   "STT": 1007,
   "ten_co_so": "Quầy thuốc Khánh My",
   "address": "ấp Nhân Tâm, xã Xuyên Mộc, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5723613,
   "Latitude": 107.4228481
 },
 {
   "STT": 1008,
   "ten_co_so": "Quầy thuốc Kim Phúc",
   "address": "Tổ 7, khu phố Tân Hạnh, thị trấn Phú Mỹ, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5905345,
   "Latitude": 107.0480378
 },
 {
   "STT": 1009,
   "ten_co_so": "Quầy thuốc Ngọc Giang",
   "address": "Tổ 19/41, khu phố Hải Tân, thị trấn Long Hải, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3851624,
   "Latitude": 107.2401989
 },
 {
   "STT": 1011,
   "ten_co_so": "Quầy thuốc Số 229",
   "address": "Chợ Cây Điệp, xã Phước Tân, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.563505,
   "Latitude": 107.3819784
 },
 {
   "STT": 1012,
   "ten_co_so": "Nhà thuốc Bệnh viện đa khoa Vạn Phước",
   "address": "Số 42, Cách Mạng Tháng 8, xã Long Hương, Bà Rịa, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4991221,
   "Latitude": 107.16019
 },
 {
   "STT": 1013,
   "ten_co_so": "Quầy thuốc Bình Dân",
   "address": "Hàng 1, thôn 2, xã Bình Trung, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6462751,
   "Latitude": 107.2815315
 },
 {
   "STT": 1014,
   "ten_co_so": "Nhà thuốc Linh Đan",
   "address": "Số 83 Lê Duẩn, tổ 5, khu phố 1, phường Phước Nguyên, Bà Rịa, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4956352,
   "Latitude": 107.178086
 },
 {
   "STT": 1015,
   "ten_co_so": "Quầy thuốc Thiên Phúc",
   "address": "Khu A, phố Chợ, ấp Thanh bình 2, xã Bình Châu, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5603904,
   "Latitude": 107.5411257
 },
 {
   "STT": 1016,
   "ten_co_so": "Nhà thuốc Thy Anh",
   "address": "110 Nguyễn Thanh Đằng, phường Phước Hiệp, Bà Rịa, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4972273,
   "Latitude": 107.1714645
 },
 {
   "STT": 1017,
   "ten_co_so": "Quầy thuốc Diệu Thiện",
   "address": "Tổ 1, ấp Phước Lập, xã Mỹ Xuân, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6456132,
   "Latitude": 107.0495635
 },
 {
   "STT": 1018,
   "ten_co_so": "Nhà thuốc Hoàng Ân",
   "address": "7A1, đường 30/4, phường 11, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.408268,
   "Latitude": 107.1286931
 },
 {
   "STT": 1019,
   "ten_co_so": "Nhà thuốc Minh Ngân",
   "address": "348 Lê Lợi, phường 7, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3691168,
   "Latitude": 107.0765412
 },
 {
   "STT": 1020,
   "ten_co_so": "Quầy thuốc Nhi",
   "address": "Số 225, Hùng Vương, thị trấn Ngãi Giao, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6440828,
   "Latitude": 107.2448518
 },
 {
   "STT": 1021,
   "ten_co_so": "Nhà thuốc Phương Lan",
   "address": "114 Nguyễn Tri Phương, phường 7, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3626746,
   "Latitude": 107.0849905
 },
 {
   "STT": 1022,
   "ten_co_so": "Quầy thuốc Thành Công 3",
   "address": "Kios số 50, chợ Mới Long Hải, thị trấn Long Hải, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.395902,
   "Latitude": 107.2370874
 },
 {
   "STT": 1023,
   "ten_co_so": "Quầy thuốc Tuấn Ý",
   "address": "Tổ 19, ấp Bình Đức, xã Bình Ba, Châu Đức, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6182884,
   "Latitude": 107.2311774
 },
 {
   "STT": 1024,
   "ten_co_so": "Quầy thuốc Số 179",
   "address": "Ấp Tân Phú, xã Châu Pha, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5737587,
   "Latitude": 107.153931
 },
 {
   "STT": 1025,
   "ten_co_so": "Nhà thuốc Nhất Nhất",
   "address": "90 Nguyễn Thị Định, phường Phước Nguyên, Bà Rịa, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4952643,
   "Latitude": 107.1863339
 },
 {
   "STT": 1026,
   "ten_co_so": "Nhà thuốc Thảo Nguyên",
   "address": "1003 Bình Giã, phường 10, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.398465,
   "Latitude": 107.1153313
 },
 {
   "STT": 1027,
   "ten_co_so": "Nhà thuốc Hải Đăng",
   "address": "Số 6, Nguyễn Trường Tộ, phường 2, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3400284,
   "Latitude": 107.0787082
 },
 {
   "STT": 1028,
   "ten_co_so": "Quầy thuốc Hải Yến",
   "address": "Ấp Nhân Trung, xã Xuyên Mộc, Xuyên Mộc, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5585237,
   "Latitude": 107.4262813
 },
 {
   "STT": 1029,
   "ten_co_so": "Nhà thuốc Hằng",
   "address": "40A2, Phước Thắng, phường 12, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4135794,
   "Latitude": 107.1609413
 },
 {
   "STT": 1030,
   "ten_co_so": "Nhà thuốc Lê Huỳnh",
   "address": "244 đường Trương Công Định, phường 3, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3533599,
   "Latitude": 107.0841855
 },
 {
   "STT": 1031,
   "ten_co_so": "Nhà thuốc Minh Chánh",
   "address": "72 Nguyễn Bỉnh Khiêm, phường Thắng Tam, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3434575,
   "Latitude": 107.0826142
 },
 {
   "STT": 1032,
   "ten_co_so": "Quầy thuốc Minh Cường",
   "address": "Số 62H2 khu phố Long Nguyên, thị trấn Long Điền, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5056039,
   "Latitude": 107.1883273
 },
 {
   "STT": 1033,
   "ten_co_so": "Quầy thuốc Minh Dũng",
   "address": "Số 47, ấp An Trung, xã An Nhứt, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4901197,
   "Latitude": 107.2459527
 },
 {
   "STT": 1034,
   "ten_co_so": "Quầy thuốc Nhất Nhất",
   "address": "Tổ 13, ấp 2, xã Hắc Dịch, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6466114,
   "Latitude": 107.113007
 },
 {
   "STT": 1035,
   "ten_co_so": "Quầy thuốc Phúc An Khang",
   "address": "83 Phạm Hữu Chí, khu phố Tân Phú, phường Phú Mỹ, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6017466,
   "Latitude": 107.0505033
 },
 {
   "STT": 1037,
   "ten_co_so": "Quầy thuốc Quang Vinh",
   "address": "3Ô2/1, khu phố Hải Phúc, thị trấn Phước Hải, Đất Đỏ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4054168,
   "Latitude": 107.2607289
 },
 {
   "STT": 1038,
   "ten_co_so": "Quầy thuốc Quang Yến",
   "address": "Tổ 10, khu phố Hải Hà, thị trấn Long Hải, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.385358,
   "Latitude": 107.237932
 },
 {
   "STT": 1039,
   "ten_co_so": "Quầy thuốc Số 113",
   "address": "Số 135, đường 30/4, phường Thắng Nhất, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3916629,
   "Latitude": 107.0995726
 },
 {
   "STT": 1041,
   "ten_co_so": "Quầy thuốc Số 193",
   "address": "Khu phố 5, phường Hắc Dịch, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6466114,
   "Latitude": 107.113007
 },
 {
   "STT": 1042,
   "ten_co_so": "Quầy thuốc Tân Trí",
   "address": "Tổ 15, ấp Lò Vôi, xã Phước Hưng, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4299543,
   "Latitude": 107.2370536
 },
 {
   "STT": 1043,
   "ten_co_so": "Quầy thuốc Thiên Ân",
   "address": "Tổ 13, ấp Phước Thọ, xã Phước Hưng, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4283624,
   "Latitude": 107.2370874
 },
 {
   "STT": 1044,
   "ten_co_so": "Quầy thuốc Thu Yến",
   "address": "Tổ 1/9 ô1 khu phố Hải Vân, thị trấn Long Hải, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.395902,
   "Latitude": 107.2370874
 },
 {
   "STT": 1045,
   "ten_co_so": "Quầy thuốc Trí An 2",
   "address": "Ấp Phước Lập, xã Mỹ Xuân, Phú Mỹ, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.6456132,
   "Latitude": 107.0495635
 },
 {
   "STT": 1046,
   "ten_co_so": "Chi nhánh Công ty cổ phần Dược Hậu Giang tại Vũng Tàu",
   "address": "60 Nguyễn Mạnh Hùng, phường Long Toàn, thành phố Bà Rịa, Bà Rịa, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4903525,
   "Latitude": 107.1948824
 },
 {
   "STT": 1047,
   "ten_co_so": "Chi nhánh Công ty cổ phần Traphaco tại Vũng Tàu",
   "address": "Số G25, Trần Cao Vân, phường 9, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.374502,
   "Latitude": 107.0873152
 },
 {
   "STT": 1048,
   "ten_co_so": "Công ty cổ phần Dược - Mỹ phẩm và thương mại KHÁNH HOÀ",
   "address": "9 Nguyễn Kim, phường 4, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3553924,
   "Latitude": 107.0816002
 },
 {
   "STT": 1049,
   "ten_co_so": "Công ty cổ phần Dược phẩm Imexpharm - Chi nhánh Bà Rịa -Vũng Tàu",
   "address": "41-43 Nguyễn Khánh Toàn, phường Phước Nguyên, Bà Rịa, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5055464,
   "Latitude": 107.1831447
 },
 {
   "STT": 1050,
   "ten_co_so": "Công ty cổ phần Dược phẩm OPC - Cửa hàng giới thiệu và kinh doanh dược phẩm",
   "address": "38 Trương Văn Bang, phường 7, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3648162,
   "Latitude": 107.0849579
 },
 {
   "STT": 1051,
   "ten_co_so": "Công ty TNHH Dược phẩm và TTBYT Quang Phát",
   "address": "46 Lê Quý Đôn, phường Phước Trung, Bà Rịa, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4928024,
   "Latitude": 107.1700858
 },
 {
   "STT": 1052,
   "ten_co_so": "Công ty TNHH Đầu tư Gia Hòa Phát",
   "address": "689 Trương Công Định, phường 7, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3648219,
   "Latitude": 107.0896556
 },
 {
   "STT": 1053,
   "ten_co_so": "Công ty TNHH Dược phẩm Song Hà",
   "address": "19D 10 Tôn Thất Thuyết, phường 9, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3712804,
   "Latitude": 107.0839853
 },
 {
   "STT": 1054,
   "ten_co_so": "Công ty TNHH Dược phẩm Thanh Tâm",
   "address": "150 đường 27/4, phường Phước Hiệp, Bà Rịa, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4979921,
   "Latitude": 107.1679938
 },
 {
   "STT": 1055,
   "ten_co_so": "Công ty TNHH Dược phẩm và trang thiết bị y tế Vĩnh Khang",
   "address": "Số 38 Nguyễn Trung Trực phường 9, Vũng Tàu, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.3727348,
   "Latitude": 107.0840938
 },
 {
   "STT": 1056,
   "ten_co_so": "Công ty TNHH AFP Pharma",
   "address": "Đường số 14, khu phố Long liên, thị trấn Long Điền, Long Điền, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.4840911,
   "Latitude": 107.2079756
 },
 {
   "STT": 1057,
   "ten_co_so": "Công ty Dược phẩm TNHH Leung Kai Fook -VN ",
   "address": "Khu Công nghiệp Mỹ Xuân - A2, Tân Thành, TỈNH KHÁNH HOÀ",
   "Longtitude": 10.5905556,
   "Latitude": 107.0480556
 }
];