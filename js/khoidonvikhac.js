var datakhoidonvikhac = [
 {
   "STT": 1,
   "Name": "Trung tâm Truyền thông giáo dục sức khỏe",
   "area": "Tỉnh",
   "icon": "maps/map-images/ttsk.png",
   "address": "31 Lê Lợi, Phường 4, Thành Phố Vũng Tàu, Tỉnh Khánh Hoà, Việt Nam",
   "Longtitude": 10.3555534,
   "Latitude": 107.0724201
 },
 {
   "STT": 2,
   "Name": "Trung tâm Kiểm nghiệm Dược phẩm - Mỹ phẩm",
   "area": "Tỉnh",
   "icon": "maps/map-images/ttkn.png",
   "address": "47 Lê Văn Lộc, Phường 9, Thành phố Vũng Tầu, KHÁNH HOÀ, Việt Nam",
   "Longtitude": 10.3706418,
   "Latitude": 107.074787
 },
 {
   "STT": 3,
   "Name": "Trung tâm Y tế dự phòng",
   "area": "Tỉnh",
   "icon": "maps/map-images/ttytdp.png",
   "address": "Phước Hưng, Bà Rịa, KHÁNH HOÀ, Việt Nam",
   "Longtitude": 10.5074322,
   "Latitude": 107.1747167
 },
 {
   "STT": 4,
   "Name": "Trung tâm Phòng, chống HIV/AIDS",
   "area": "Tỉnh",
   "icon": "maps/map-images/hiv.png",
   "address": "3 Phạm Văn Đồng, Phước Trung, Tp. Bà Rịa, KHÁNH HOÀ, Việt Nam",
   "Longtitude": 10.488459,
   "Latitude": 107.1803491
 },
 {
   "STT": 5,
   "Name": "Trung tâm Chăm sóc sức khỏe sinh sản",
   "area": "Tỉnh",
   "icon": "maps/map-images/skss.png",
   "address": "Hoà Long, Bà Rịa, KHÁNH HOÀ, Việt Nam",
   "Longtitude": 10.4884585,
   "Latitude": 107.1650282
 },
 {
   "STT": 6,
   "Name": "Trung tâm giám định y khoa",
   "area": "Tỉnh",
   "icon": "maps/map-images/gdyk.png",
   "address": "31/4 Lê Lợi, Phường 4, Thành phố Vũng Tầu, KHÁNH HOÀ, Việt Nam",
   "Longtitude": 10.3558963,
   "Latitude": 107.0722869
 },
 {
   "STT": 7,
   "Name": "Chi cục Dân số - Kế hoạch hóa gia đình",
   "icon": "maps/map-images/cckhhgd.png",
   "area": "Tỉnh",
   "address": "74 Ba Cu, Phường 1, Thành phố Vũng Tầu, KHÁNH HOÀ, Việt Nam",
   "Longtitude": 10.3501247,
   "Latitude": 107.075258
 },
 {
   "STT": 8,
   "Name": "Chi cục An toàn vệ sinh thực phẩm",
   "area": "Tỉnh",
   "icon": "maps/map-images/attp.png",
   "address": "Phạm Văn Đồng, Phước Hưng, Bà Rịa, KHÁNH HOÀ, Việt Nam",
   "Longtitude": 10.4916178,
   "Latitude": 107.1813914
 },
 {
   "STT": 9,
   "Name": "Trường trung cấp y tế",
   "area": "Tỉnh",
   "icon": "maps/map-images/tcyt.png",
   "address": "1165 Võ Văn Kiệt, ấp Tây, xã Hoà Long, Bà Rịa, KHÁNH HOÀ, Việt Nam",
   "Longtitude": 10.5120336,
   "Latitude": 107.1944006
 }
];