﻿$(function () {



    var chart = new Highcharts.chart('container', {
        chart: {
            type: 'column',
            options3d: {
                enabled: true,
                alpha: 0,
                beta: 0,
                viewDistance: 25,
                depth: 40
            }
        },

        title: {
            text: 'Biểu đồ thống kê số người chết và bị thương',
            style: {
                color: '#000000',
                fontWeight: 'bold'
            }
        },

        xAxis: {
            categories: ['Tháng 1', 'Tháng 2', 'Tháng 3', 'Tháng 4', 'Tháng 5'],
            labels: {
                skew3d: true,
                style: {
                    fontSize: '16px'
                }
            }
        },
        credits: {
            enabled: false
        },
        exporting: { enabled: false },
        yAxis: {
            allowDecimals: false,
            min: 0,
            title: {
                text: 'Số vụ',
                skew3d: true
            }
        },

        tooltip: {
            headerFormat: '<b>{point.key}</b><br>',
            pointFormat: '<span style="color:{series.color}">●</span> {point.y} / {point.stackTotal}'
        },

        plotOptions: {
            column: {
                stacking: 'normal',
                depth: 40
            }
        },

        series: [{
            name: 'Số người bị thương',
            data: [5, 3, 4, 7, 8],
            stack: 'male'
        }, {
            name: 'Số người chết',
            data: [3, 4, 4, 2, 2],
            stack: 'male'
        }]
    });


});